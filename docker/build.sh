#!/usr/bin/env bash
set -e
set -x

if [ ! -f ./oats.cabal ]; then
  set +x
  echo "You are probably running the script from a wrong directory."
  echo "It's meant to be run from project root, but oats.cabal is missing."
  echo "Current path is: <$PWD>"
  exit 15
fi
mkdir -p docker/cache/user_home_stack
mkdir -p docker/cache/project_stack_work

show_help() {
  echo "Usage: ./build.sh [OPTIONS]"
  echo "Options:"
  echo "  --no-docker-cache    Build the Docker image without using cache"
  echo "  --help               Show this help message"
}

while [[ $# -gt 0 ]]; do
  case "$1" in
    --no-docker-cache) docker_cache="--no-cache"; shift ;;
    --help) show_help; exit 0 ;;
    *) echo "Unknown option: $1"; show_help; exit 1 ;;
  esac
done

echo Building build image
{ time docker build $docker_cache -t oats-build -f Dockerfile-build --progress=plain .; } 2>&1 | tee docker/build.log
[ ${PIPESTATUS[0]} -eq 0 ] || false

echo Copying cache from build container
time docker run --rm -it -v "$(pwd)/docker/cache":/cache oats-build sh -c "cp -r /root/.stack/* /cache/user_home_stack && cp -r /app/.stack-work/* /cache/project_stack_work && chown -R $(id -u):$(id -g) /cache"
du -hs docker/cache/*

echo Creating runner image
{ time docker build $docker_cache -t mon7/oats .; } 2>&1 | tee -a docker/build.log
[ ${PIPESTATUS[0]} -eq 0 ] || false
