# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
### Changed
- Updated stack snapshot to lts-23.3.

## [1.6.3]  - 2024-09-29
### Added
### Changed
- TypeScript test project: updates of dependencies and minor fixes.
- Improved error message for required referencing non-existing properties.
- Fixed swapped operation ID and URL in error context in params of endpoint.

## [1.6.2]  - 2024-06-05
### Added
- Added very basic support for cookie type parameter in endpoints.

## [1.6.1]  - 2023-09-04
### Changed
- Added git to docker run image (fixes missing API info).

## [1.6.0]
### Added
- Implemented afterFileWriteCommand.
- Implemented better cache for docker image building.
- Implemented support for proper prefix of custom OA fields (x-oats-overrideNonRequiredRepresentation, x-oats-meta). Added deprecation warning to old names (x-overrideNonRequiredRepresentation, x-meta).
- Implemented API info.
- Implemented recalculated min/max constants.
- Implemented custom trimmed min length validator.
- Implemented recalculated combined min/max constants.

### Changed
- Possibly fixed issue with wrong path separator when generating from custom schemas on Windows
- Reimplemented how decimal places are calculated, added support for "decimals" with zero decimal places.
- Added support for endpoint description field (previously was only summary accessible in template).
- Transitioned from lens to optics library. Split data modules to multiple files.

## [1.5.0]
### Added
- Implemented support for UUID string format.

### Changed
- Fixed invalid tags in endpoints not reporting error.
- Fixed crashing build when user has allow-newer set to false.

## [1.4.1]
### Changed
- Fixed crash when output directory is missing and cleaning is enabled.

## [1.4.0]
### Added
- Implemented cleaning of output directory.

### Changed
- Unused validations (bugs in API) are now correctly reported as errors.
- Updated compiler and dependencies.
- Optimized docker runner image.

## [1.3.0]
### Added
- Implemented support for top-level date and date-time (aliases).

### Changed
- Moved oats config files to its directory in e2e tests.

## [1.2.0]
### Added
- Implemented support for field comments.

### Changed
- Fixed missing error on validations in places where no code could be generated from them.
- Split e2e tests to files.
- Removed debug output from PrimitiveTypeInRequestOrResponseBody error message.

## [1.1.0]
### Added
- Implemented recursive schemas support. 
- Added changelog file.
- Implemented experimental support for endpoint permissions.

### Changed
- Improved error and warning formatting.
- Refactoring of color mode handling.
- Fixed messages in warnings and errors being always stripped of color.
- Fixed forgotten symbol in MultipleTitleSources warning formatting.
