FROM mhart/alpine-node:slim-16
WORKDIR /app

RUN apk add --no-cache libc6-compat gmp git

COPY --from=oats-build /root/.local/bin/oats /app
RUN /app/oats --version

ENV PATH=$PATH:/app

ENTRYPOINT ["/app/oats"]
