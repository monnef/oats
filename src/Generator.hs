{-# LANGUAGE DataKinds #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE PartialTypeSignatures #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE ViewPatterns #-}

module Generator
  ( generateTypeScript,
    generateEndpointsTypeScript,
    extractSuccessResponse,
    rawTagToTagId,
    genMultilineSingleLineComment,
    fieldToIoTs,
    FieldLocation (..),
    isSimpleFieldName,
    fieldToTs,
    genTsInterface,
    genNumberRegex,
    calcLimitVal,
    MinOrMax (..),
    RangeLimit (..),
  )
where

import BuildInfo
import Control.Applicative ((<|>))
import Control.Applicative.HT (lift6)
import Control.Monad (unless)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Except (throwError)
import Data
import Data.Aeson (toJSON)
import Data.Char (isAlphaNum)
import Data.Default (def)
import Data.Either.Combinators (mapBoth)
import Data.Either.Extra (mapLeft, maybeToEither)
import Data.Function (on)
import qualified Data.HashMap.Lazy as M
import Data.List (delete, find, groupBy, nub, sort, sortBy, sortOn)
import Data.Maybe (catMaybes, fromMaybe, isJust, maybeToList)
import Data.Ord (comparing)
import qualified Data.Text as T
import Data.Tuple.Extra (uncurry3)
import DataProcessor (depFromTOA, mimeAppJson)
import qualified Git
import Optics hiding (re)
import RangeUtils
import Safe (fromJustNote, headMay, lastMay)
import System.FilePath (dropExtension, takeDirectory)
import Text.Casing (fromWords, toPascal)
import Text.RE.PCRE.Text (allMatches, compileRegex, compileSearchReplace, matched, matchedText, re, (*=~), (*=~/), (?=~))
import Utils

data GenImportType = GenericImportType | StringPatternImportType deriving (Eq)

ioTsImport :: Text
ioTsImport = "import * as t from 'io-ts';"

isSchemaRecursive :: OAMeta -> Bool
isSchemaRecursive = view #isRecursive >>> (== Just True)

genSchemaImports :: WorkContext -> GenImportType -> OAMeta -> AppM [Text]
genSchemaImports ctx typ meta = do
  let deps = meta ^. #dependencies & bool id (sort >>> nub >>> delete (meta ^. #name)) (isSchemaRecursive meta)
  depImports <- genDepImports ctx (Just meta) deps <&> (sort >>> nub)
  return $
    concat
      [ [ioTsImport],
        getIfPresent (ctx ^. #config % #useExcess) #excessImport, --TODO: should only be inserted if used
        getIfPresent (typ == StringPatternImportType) #stringPatternImport
      ]
      <> depImports
  where
    getIfPresent :: Bool -> Optic A_Lens is Config Config (Maybe a) (Maybe a) -> [a]
    getIfPresent cond cfgLens = if cond then fromMaybe [] (ctx ^. #config % cfgLens <&> (: [])) else []

shouldGenerateDepImports :: AppOperationMode -> Bool
shouldGenerateDepImports m
  | m == SimpleSchemaMode = False
  | m `elem` [BatchMode, SingleFileMode] = True
  | otherwise = error [qq|invalid mode {m} passed to shouldGenerateDepImports|]

genDepImports :: WorkContext -> Maybe OAMeta -> [Text] -> AppM [Text]
genDepImports ctx@(view #mode >>> shouldGenerateDepImports -> True) meta xs = mapM (genDepImport ctx meta) xs <&> (newLine <>)
genDepImports _ _ _ = return []

genDepImport :: WorkContext -> Maybe OAMeta -> Text -> AppM Text
genDepImport ctx meta x = do
  let depMeta = find (\y -> y ^. #name == x) (ctx ^. #metas)
  let appMode = ctx ^. #mode
  let depPath = if appMode == SingleFileMode then Nothing else depMeta <&> (^. #path) & join
  let depName = if appMode == SingleFileMode then depMeta <&> (^. #name) else Nothing
  depPathFromMeta :: Text <- depPath <|> depName & maybeToEither (GenerationError (mkErrCtxFromMetaOrDef meta) [qq|Not found dependency "{x}"|]) & liftEitherToAppM
  let impPath :: Text = depPathFromMeta & liftToText dropExtension & backSlashToSlash
  let moduleName = ctx ^. #config % #apiModule
  return
    [qq|// @ts-ignore: Workaround for "is declared but its value is never read."
import \{ {x}, {typeNameToIoTsSchemaName ctx x} \} from '{moduleName}/{impPath}';|]

data FieldLocation = FieldLocationRequired | FieldLocationOptional deriving (Eq, Show)

oaTypeToIoTsType :: WorkContext -> FieldLocation -> TypeOA -> Either AppError Text
oaTypeToIoTsType ctx loc = \case
  PrimitiveTOA StringPOA -> Right "t.string"
  PrimitiveTOA BooleanPOA -> Right "t.boolean"
  PrimitiveTOA IntegerPOA -> Right "t.Integer"
  PrimitiveTOA NumberPOA -> Right "t.number"
  ArrayTOA t -> do
    itemType <- oaTypeToIoTsType ctx loc t
    return $ "t.array(" <> itemType <> ")"
  SchemaTOA s -> Right $ typeNameToIoTsSchemaName ctx s
  OneValueEnumTOA t -> Right [qq|t.literal('{t}')|]

oaTypeToTsType :: TypeOA -> Text
oaTypeToTsType = \case
  PrimitiveTOA StringPOA -> "string"
  PrimitiveTOA BooleanPOA -> "boolean"
  PrimitiveTOA IntegerPOA -> "number"
  PrimitiveTOA NumberPOA -> "number"
  ArrayTOA t -> let itemType = oaTypeToTsType t in [qq|$itemType[]|]
  SchemaTOA s -> s
  OneValueEnumTOA t -> [qq|'{t}'|]

isSimpleFieldName :: Text -> Bool
isSimpleFieldName = (?=~ [re|^[a-zA-Z_]+[\da-zA-Z_]*$|]) >>> matched

escapeFieldNameIfNecessary :: Text -> Text
escapeFieldNameIfNecessary x = x & bool genJsStringLiteral id (isSimpleFieldName x)

defaultFieldCommentWrapPoint :: Int
defaultFieldCommentWrapPoint = 100

addCommentToFieldTs :: Int -> Maybe Text -> Text -> [Text]
addCommentToFieldTs wrapPoint comment code = case comment of
  Just descr ->
    let longComment = bool [] (descr & genMultilineSingleLineComment) shouldWrapComment
        shortComment = bool "" (" // " <> descr) (not shouldWrapComment)
     in longComment <> [code <> shortComment]
  Nothing -> [code]
  where
    shouldWrapComment =
      let d = fromMaybe "" comment in T.length code + T.length d > wrapPoint || T.isInfixOf "\n" d

fieldToIoTs :: WorkContext -> FieldLocation -> OA -> FieldOA -> Either AppError [Text]
fieldToIoTs ctx loc cls FieldOA {..} =
  if shouldRender (loc, required, repr) then fieldLine else Right []
  where
    repr = nonRequiredReprForClass ctx cls
    typeText = oaTypeToIoTsType ctx loc typ <&> bool id wrapWithUnionNull (repr == NullAsNonRequired && required == OptionalOA)
    processedName = escapeFieldNameIfNecessary name
    fieldLine = fieldLineRaw <&> addCommentToFieldTs defaultFieldCommentWrapPoint description
    fieldLineRaw = typeText <&> \tt -> [qq|$processedName: $tt,|]
    wrapWithUnionNull (x :: Text) = [qq|t.union([$x, t.null])|]
    shouldRender =
      flip
        elem
        [ (FieldLocationOptional, OptionalOA, UndefinedAsNonRequired),
          (FieldLocationRequired, RequiredOA, UndefinedAsNonRequired),
          (FieldLocationRequired, RequiredOA, NullAsNonRequired),
          (FieldLocationRequired, OptionalOA, NullAsNonRequired)
        ]

fieldToTs :: NonRequiredRepresentation -> FieldOA -> [Text]
fieldToTs nonReqRepr FieldOA {..} = fieldLineRaw & addCommentToFieldTs defaultFieldCommentWrapPoint description
  where
    fieldLineRaw = case nonReqRepr of
      NullAsNonRequired -> let reqPart :: Text = bool "" " | null" isOpt in [qq|$processedFieldName: $t$reqPart;|]
      UndefinedAsNonRequired -> let reqPart :: Text = bool "" "?" isOpt in [qq|$processedFieldName$reqPart: $t;|]
    isOpt = required == OptionalOA
    processedFieldName = escapeFieldNameIfNecessary name
    t = oaTypeToTsType typ

genTsInterface :: WorkContext -> Text -> [FieldOA] -> AppM Text
genTsInterface ctx name xs =
  return
    [qq|export interface $name \{
$fields
\}|]
  where
    fields = xs <&> fieldToTs (ctx ^. #config % #nonRequiredRepresentation) & concat <&> ("  " <>) & T.intercalate "\n"

genMultilineSingleLineComment :: Text -> [Text]
genMultilineSingleLineComment = T.lines >>> chompList >>> fmap ("// " <>)
  where
    chompList xs = (bool id init $ lastMay xs == Just "") xs

genDocs :: WorkContext -> OAMeta -> [Text]
genDocs _ m = descr <> commnt <> exmpl
  where
    p x = x <&> genMultilineSingleLineComment & maybeToList & join
    descr = m ^. #description & p
    commnt = m ^. #comment & p
    exmpl = m ^. #example <&> ("Example: " <>) & p

genTypePartNameForClass :: Text -> Maybe Text -> Text
genTypePartNameForClass baseName Nothing = baseName
genTypePartNameForClass baseName (Just prefix) = prefix <> capitalize baseName

objectFieldPrefix :: Text
objectFieldPrefix = "  "

genRequiredPart :: WorkContext -> Maybe Text -> OA -> Either AppError [Text]
genRequiredPart ctx prefix x@OAClass {} = do
  fields' <- x ^?<> #fields <&> fieldToIoTs ctx FieldLocationRequired x & liftEithersToMultiError
  let preparedFields = fields' & concat <&> (objectFieldPrefix <>)
  return $
    [ [[qq|const $partName = t.interface(\{|]],
      preparedFields,
      ["});"]
    ]
      & concat
  where
    partName = genTypePartNameForClass "requiredPart" prefix
genRequiredPart _ _ x = error $ "Invalid OA type:" <> pShow x & toS

nonRequiredReprForClass :: WorkContext -> OA -> NonRequiredRepresentation
nonRequiredReprForClass ctx x = fromMaybe (ctx ^. #config % #nonRequiredRepresentation) (x ^?? #overrideNonRequiredRepresentation)

genOptionalPart :: WorkContext -> Maybe Text -> OA -> Either AppError [Text]
genOptionalPart ctx prefix x@OAClass {} =
  if nonRequiredReprForClass ctx x == NullAsNonRequired then return [] else texts
  where
    partName = genTypePartNameForClass "optionalPart" prefix
    texts = do
      fields' <- x ^?<> #fields <&> fieldToIoTs ctx FieldLocationOptional x & liftEithersToMultiError
      let preparedFields = fields' & concat <&> (objectFieldPrefix <>)
      return $
        [ [[qq|const $partName = t.partial(\{|]],
          preparedFields,
          ["});"]
        ]
          & concat
genOptionalPart _ _ x = error $ "Invalid OA type:" <> pShow x & toS

genTypeExport :: WorkContext -> OA -> OAMeta -> [Text]
genTypeExport ctx x meta = [r]
  where
    r = case x of
      OAClass {} -> [qq|export interface $oaName extends t.TypeOf<typeof $schemaName> \{}|]
      OAString {} -> [qq|export type $oaName = string;|]
      OAAlias {aliasTarget = target} -> [qq|export type $oaName = $target;|]
      _ -> [qq|export type $oaName = t.TypeOf<typeof $schemaName>;|]
    oaName = meta ^. #name
    schemaName = typeNameToIoTsSchemaName ctx oaName

genClassSchemaConstructionRight :: WorkContext -> Maybe Text -> OA -> OAMeta -> [Text]
genClassSchemaConstructionRight ctx partPrefix x _meta = [res]
  where
    res =
      (x ^?? #extends <&> (typeNameToIoTsSchemaName ctx >>> (<> ".type")) & maybeToList)
        <> bool [] [genTypePartNameForClass "optionalPart" partPrefix] (nonRequiredReprForClass ctx x == UndefinedAsNonRequired)
        <> [genTypePartNameForClass "requiredPart" partPrefix]
        & \parts ->
          let partsJoined = T.intercalate ", " parts
              partsText = bool partsJoined [qq|t.intersection([$partsJoined])|] (length parts > 1)
              operation :: Text = if ctx ^. #config % #useExcess then "excess" else "t.exact"
           in [qq|$operation($partsText)|]

genRecursiveClassSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either AppError [Text]
genRecursiveClassSchemaConstruction ctx x meta = do
  let clsName = meta ^. #name
  let returnPart = genClassSchemaConstructionRight ctx Nothing x meta & T.intercalate "\n"
  optPart <- genOptionalPart ctx Nothing x <&> appendNewLineIfNotEmpty
  reqPart <- genRequiredPart ctx Nothing x
  let middlePart :: Text = optPart <> reqPart <&> ("  " <>) & T.intercalate nl
  let res =
        [qq|export const {typeNameToIoTsSchemaName ctx clsName}: t.Type<$clsName> = t.recursion('{clsName}', () => \{
$middlePart$nl
  return $returnPart;
});|]
  return [res]

genClassSchemaConstruction :: WorkContext -> Maybe Text -> OA -> OAMeta -> [Text]
genClassSchemaConstruction ctx partPrefix x meta = [res]
  where
    clsName = meta ^. #name
    rightPart = genClassSchemaConstructionRight ctx partPrefix x meta & T.intercalate "\n"
    res = [qq|export const {typeNameToIoTsSchemaName ctx clsName} = $rightPart;|]

genUnionSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either AppError [Text]
genUnionSchemaConstruction ctx x meta = do
  let unionName = meta ^. #name
  schemas <- x ^?<> #members <&> oaTypeToIoTsType ctx FieldLocationRequired & liftEithersToMultiError <&> T.intercalate ", "
  let tUnion = [qq|t.union([$schemas])|]
  let oneMemberUnionDef = schemas
  let commentPart = x ^?? #tag <&> (\t -> [qq|// tag is field '{t}'|]) & maybeToList
  let defPart = if length (x ^?<> #members) == 1 then oneMemberUnionDef else tUnion
  let exportPart = [[qq|export const {typeNameToIoTsSchemaName ctx unionName} = $defPart;|]]
  return $ commentPart <> exportPart

genEnumSchemaConstruction :: WorkContext -> OA -> OAMeta -> [Text]
genEnumSchemaConstruction ctx x meta = header <> items <> footer
  where
    enumName = meta ^. #name
    header = [[qq|export const {typeNameToIoTsSchemaName ctx enumName} = t.keyof(\{|]]
    items = x ^?<> #enumItems <&> \x -> [qq|  $x: null,|]
    footer = [[qq|});|]] :: [Text]

genRefinement :: Text -> Text -> Text -> Text
genRefinement baseType validation typeName = [qq|t.refinement({baseType}, {validation}, '{typeName}')|]

genArraySchemaConstruction :: WorkContext -> OA -> OAMeta -> Either AppError [Text]
genArraySchemaConstruction ctx x meta = do
  arrType <- x ^? #itemsType & fromJustNote "Missing type of array items" & oaTypeToIoTsType ctx FieldLocationRequired
  let tArray = "t.array(" <> arrType <> ")" :: Text
  return $ case validationJoined of
    Nothing -> [[qq|export const {schemaName} = {tArray};|]]
    Just cond -> [[qq|export const {schemaName} = {genRefinement tArray cond arrName};|]]
  where
    arrName :: Text = meta ^. #name
    uniqueItemsCond = (x ^? #arrayUniqueItems & justFalseToNothing) $> [qq|new Set(xs).size === xs.length|]
    minItemsCond = x ^? #arrayMinItems % _Just <&> \y -> [qq|xs.length >= $y|]
    maxItemsCond = x ^? #arrayMaxItems % _Just <&> \y -> [qq|xs.length <= $y|]
    validationJoined = [uniqueItemsCond, minItemsCond, maxItemsCond] & catMaybes & T.intercalate " && " & textToMaybe <&> ("xs => " <>)
    schemaName = typeNameToIoTsSchemaName ctx arrName

genStringSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either Text [Text]
genStringSchemaConstruction ctx OAString {..} meta =
  case (stringPattern, stringMinLength, stringTrimmedMinLength, stringMaxLength) of
    (Nothing, Nothing, Nothing, Nothing) -> return [[qq|export const $ioTsSchemaName = t.string;|]]
    (Just patty, minLen, trimmedMinLen, maxLen) -> do
      (regexGen, rawRegexSchema, regexSchema) <- processPatty patty
      if isJust minLen || isJust maxLen
        then do
          lenRes <- processMinMax rawRegexSchema minLen trimmedMinLen maxLen
          return $ [regexGen, ""] <> lenRes
        else return [regexGen, "", regexSchema]
    (Nothing, minLen, trimmedMinLen, maxLen) -> processMinMax "t.string" minLen trimmedMinLen maxLen
  where
    processPatty :: Text -> Either Text (Text, Text, Text)
    processPatty patty = do
      psg :: Text <- ctx ^. #config % #stringPatternSchemaGenerator & maybeToEither "Missing stringPatternSchemaGenerator"
      let rawSchema = [qq|{psg}<{schemaName}>({regexGenName}())|]
      return
        ( [qq|export const $regexGenName = (): RegExp => new RegExp({genJsStringLiteral patty});|],
          rawSchema,
          [qq|export const $ioTsSchemaName = {rawSchema};|]
        )
    schemaName = meta ^. #name
    ioTsSchemaName = typeNameToIoTsSchemaName ctx schemaName
    camelSchemaName = deCapitalize schemaName
    regexGenName :: Text = [qq|{camelSchemaName}RegexGen|]
    processMinMax :: Text -> Maybe Int -> Maybe Int -> Maybe Int -> Either Text [Text]
    processMinMax baseSchema minLen trimmedMinLen maxLen = do
      let minLenConst = camelSchemaName <> "MinLength"
      let maxLenConst = camelSchemaName <> "MaxLength"
      let trimmedMinLenConst = camelSchemaName <> "TrimmedMinLength"
      let constPart =
            [ minLen <&> (\x -> [i|export const #{minLenConst} = #{x};|] :: Text),
              trimmedMinLen <&> (\x -> [i|export const #{trimmedMinLenConst} = #{x};|] :: Text),
              maxLen <&> (\x -> [i|export const #{maxLenConst} = #{x};|] :: Text)
            ]
              & catMaybes
      let nLen = "n.length"
      let nTrimLen = "n.trim().length"
      let minCondPart = minLen <&> (\_ -> nLen <> " >= " <> minLenConst) & fromMaybe ""
      let minTrimmedCondPart = trimmedMinLen <&> (\_ -> nTrimLen <> " >= " <> trimmedMinLenConst) & fromMaybe ""
      let maxCondPart = maxLen <&> (\_ -> nLen <> " <= " <> maxLenConst) & fromMaybe ""
      let condPart = [minCondPart, minTrimmedCondPart, maxCondPart] & filter (not . T.null) & T.intercalate " && " & ("n => " <>)
      let schemaPart = [qq|export const $ioTsSchemaName = {genRefinement baseSchema condPart schemaName};|]
      return $ constPart <> ["", schemaPart]
genStringSchemaConstruction _ x _ = Left $ "Invalid OA type passed into genStringSchemaConstruction: " <> toS (pShow x)

genNumberRegex :: Double -> Double -> Bool -> Bool -> Double -> Int -> Text
genNumberRegex min max exclusiveMin exclusiveMax multipleOf decimalPrecision =
  [intPart, decPart] & catMaybes & T.intercalate decSep & surround "^" "$"
  where
    inclusiveMinVal = calcLimitVal Min multipleOf (isExclusiveToRangeLimit exclusiveMin, Inclusive) min
    inclusiveMaxVal = calcLimitVal Max multipleOf (isExclusiveToRangeLimit exclusiveMax, Inclusive) max
    signPart = bool "" "-?" $ inclusiveMinVal < 0.0
    inclusiveIntMinDigits = inclusiveMinVal & incValToIntDigits
    inclusiveIntMaxDigits = inclusiveMaxVal & incValToIntDigits
    maxDigits = Prelude.maximum [inclusiveIntMinDigits, inclusiveIntMaxDigits]
    intDigitsRangeTop :: Text = bool "" [qq|,{maxDigits}|] $ maxDigits > 1
    intPart = Just $ signPart <> [qq|\\d\{1{intDigitsRangeTop}\}|]
    decPart = bool Nothing (Just [qq|\\d\{$decimalPrecision\}|]) $ decimalPrecision > 0
    decSep = "[,.]"
    incValToIntDigits :: Double -> Int
    incValToIntDigits 0.0 = 0
    incValToIntDigits x = x & abs & (+ (multipleOf / 2.0)) & truncate & numberOfDigits

genIntegerOrNumberSchemaConstruction :: WorkContext -> Text -> Text -> Maybe Double -> Maybe Double -> Bool -> Bool -> Maybe Double -> Maybe Int -> Either Text [Text]
genIntegerOrNumberSchemaConstruction ctx@WorkContext {config} baseType schemaName min max exclusiveMin exclusiveMax multipleOf decimalPrecision =
  if any isJust [min, max] || isJust multipleOf
    then processMinMax min max decimalPrecision
    else processSimple
  where
    camelSchemaName = deCapitalize schemaName
    ioTsSchemaName = typeNameToIoTsSchemaName ctx schemaName
    processSimple = Right [[qq|export const {ioTsSchemaName} = {baseType};|]]
    processMinMax :: Maybe Double -> Maybe Double -> Maybe Int -> Either Text [Text]
    processMinMax min max decimalPrecision = do
      let minConst = camelSchemaName <> "Minimum"
      let maxConst = camelSchemaName <> "Maximum"
      let decimalPlacesConst = camelSchemaName <> "DecimalPlaces"
      let regexConst = camelSchemaName <> "RegexGen"
      limitParts :: [Text] <- do
        case (config ^. #numberMinMaxLimitsEnabled, config ^. #numberMinMaxLimitsTemplate, multipleOf) of
          (True, tpl, Just multOf) -> do
            let genPart limitType limit exclusive targetLimitType =
                  traverse (\l -> genLimitPart tpl limitType l exclusive multOf targetLimitType) limit
            let genInInclusiveMode = (config ^. #numberMinMaxLimitsMode) `elem` [InclusiveOnly, InclusiveAndExclusive]
            let genInExclusiveMode = (config ^. #numberMinMaxLimitsMode) `elem` [ExclusiveOnly, InclusiveAndExclusive]
            let actions =
                  bool
                    []
                    [ genPart Min min exclusiveMin Inclusive,
                      genPart Max max exclusiveMax Inclusive
                    ]
                    genInInclusiveMode
                    <> bool
                      []
                      [ genPart Min min exclusiveMin Exclusive,
                        genPart Max max exclusiveMax Exclusive
                      ]
                      genInExclusiveMode
            parts <- sequence actions
            return $ parts & catMaybes
          _ -> return []
      combinedLimitParts :: [Text] <- do
        case (config ^. #numberMinMaxLimitsCombinedEnabled, config ^. #numberMinMaxLimitsCombinedTemplate, multipleOf) of
          (True, tpl, Just multOf) -> do
            let genInInclusiveMode = (config ^. #numberMinMaxLimitsCombinedMode) `elem` [InclusiveOnly, InclusiveAndExclusive]
            let genInExclusiveMode = (config ^. #numberMinMaxLimitsCombinedMode) `elem` [ExclusiveOnly, InclusiveAndExclusive]
            let actions =
                  bool
                    []
                    [genLimitPartCombined tpl (min, exclusiveMin) (max, exclusiveMax) Inclusive multOf]
                    genInInclusiveMode
                    <> bool
                      []
                      [genLimitPartCombined tpl (min, exclusiveMin) (max, exclusiveMax) Exclusive multOf]
                      genInExclusiveMode
            parts <- sequence actions
            return $ parts
          _ -> return []
      let incExclFromIsExclusive isExclMay = isExclMay & bool "inclusive" ("exclusive" :: Text)
      let constPart :: [Text] =
            [ min <&> (\x -> [qq|export const $minConst = {fmtFloat x}; // {incExclFromIsExclusive exclusiveMin}|] :: Text),
              max <&> (\x -> [qq|export const $maxConst = {fmtFloat x}; // {incExclFromIsExclusive exclusiveMax}|] :: Text)
            ]
              <> (limitParts <&> Just)
              <> (combinedLimitParts <&> Just)
              <> [ decimalPrecision <&> (\x -> [qq|export const {decimalPlacesConst} = {x};|] :: Text),
                   lift6 genNumberRegex min max (Just exclusiveMin) (Just exclusiveMax) multipleOf decimalPrecision
                     <&> (\x -> [qq|export const {regexConst} = new RegExp({genJsStringLiteral x});|])
                 ]
              & catMaybes
      let eqSignIfFalse = bool ("=" :: Text) ""
      let minCondPart = min $> [qq|x >{eqSignIfFalse exclusiveMin} |] <> minConst
      let maxCondPart = max $> [qq|x <{eqSignIfFalse exclusiveMax} |] <> maxConst
      let multOfCondPart = multipleOf <&> genMultOfPart
      let condPart = [minCondPart, maxCondPart, multOfCondPart] & catMaybes & T.intercalate " && " & ("x => " <>)
      let schemaPart = [qq|export const $ioTsSchemaName = {genRefinement baseType condPart schemaName};|]
      return $ constPart <> ["", schemaPart]
      where
        genMultOfPart :: Double -> Text
        genMultOfPart x = [qq|Number.isInteger(parseFloat((x / {fmtFloat x}).toFixed(10)))|]
        genLimitPart :: Text -> MinOrMax -> Double -> Bool -> Double -> RangeLimit -> Either Text Text
        genLimitPart template minOrMax val isExcl multOf targetLimitType = do
          let isInclusive = not $ isExclusive targetLimitType
          let limitFromTo = (isExclusiveToRangeLimit isExcl, targetLimitType)
          let limitValue = calcLimitVal minOrMax multOf limitFromTo val & fmtFloat
          let limitName = case minOrMax of Min -> "Minimum"; Max -> "Maximum"
          let rCtx = DocRenderLimitsContext {..}
          renderTemplate template rCtx
        genLimitPartCombined :: Text -> (Maybe Double, Bool) -> (Maybe Double, Bool) -> RangeLimit -> Double -> Either Text Text
        genLimitPartCombined tpl (min, exclusiveMin) (max, exclusiveMax) targetLimitType multOf = do
          let isInclusive = not $ isExclusive targetLimitType
          let orNull = fromMaybe "null"
          let limitMinValue = min <&> calcLimitVal Min multOf (isExclusiveToRangeLimit exclusiveMin, targetLimitType) <&> fmtFloat & orNull
          let limitMaxValue = max <&> calcLimitVal Max multOf (isExclusiveToRangeLimit exclusiveMax, targetLimitType) <&> fmtFloat & orNull
          let rCtx = DocRenderLimitsCombinedContext {..}
          renderTemplate tpl rCtx

genIntegerSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either Text [Text]
genIntegerSchemaConstruction ctx OAInteger {..} meta =
  genIntegerOrNumberSchemaConstruction ctx "t.Integer" (meta ^. #name) (integerMinimum <&> fromIntegral) (integerMaximum <&> fromIntegral) False False Nothing Nothing
genIntegerSchemaConstruction _ x _ = Left $ "Invalid OA type passed into genIntegerSchemaConstruction: " <> toS (pShow x)

genNumberSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either Text [Text]
genNumberSchemaConstruction ctx OANumber {..} meta =
  genIntegerOrNumberSchemaConstruction ctx "t.number" (meta ^. #name) numberMinimum numberMaximum numberExclusiveMinimum numberExclusiveMaximum numberMultipleOf numberDecimalPrecision
genNumberSchemaConstruction _ x _ = Left $ "Invalid OA type passed into genNumberSchemaConstruction: " <> toS (pShow x)

genDecimalSchemaConstruction :: WorkContext -> OA -> OAMeta -> Either Text [Text]
genDecimalSchemaConstruction ctx OADecimal {..} meta = do
  let schemaName = meta ^. #name
  let camelSchemaName = deCapitalize schemaName
  let regexGenName :: Text = [qq|{camelSchemaName}RegexGen|]
  let (totalDigits, decimalDigits) = (decimalPrecision, decimalScale)
  let intDigits = totalDigits - decimalDigits
  let regexGenPart = [qq|export const {regexGenName} = () => /^-?\\d\{1,{intDigits}}(?:\\.\\d\{1,{decimalDigits}})?$/;|]
  psg :: Text <- ctx ^. #config % #stringPatternSchemaGenerator & maybeToEither "Missing stringPatternSchemaGenerator"
  let baseSchema :: Text = [qq|{psg}<{schemaName}>({regexGenName}())|]
  let typePart = [qq|export type {schemaName} = string;|]
  let minConstName :: Text = [qq|{camelSchemaName}Minimum|]
  let maxConstName :: Text = [qq|{camelSchemaName}Maximum|]
  let decimalPlacesConstName :: Text = [qq|{camelSchemaName}DecimalPlaces|]
  let constantsPart =
        [ decimalMin <&> \x -> [qq|export const {minConstName} = {x};|],
          decimalMax <&> \x -> [qq|export const {maxConstName} = {x};|],
          Just [qq|export const {decimalPlacesConstName} = {decimalPrecision};|]
        ]
          & catMaybes
  let validateFnName :: Text = [qq|validate{schemaName}Range|]
  let nNum :: Text = "num"
  let minCondPart = decimalMin $> [qq|{nNum} >= {minConstName}|]
  let maxCondPart = decimalMax $> [qq|{nNum} <= {maxConstName}|]
  let condPart = [minCondPart, maxCondPart] & catMaybes & T.intercalate " && "
  let validatePart =
        [ Just [qq|export const {validateFnName} = (x: string): boolean => \{|],
          Just [qq|  const num = Number(x);|],
          Just [qq|  return {condPart};|],
          Just "};"
        ]
          & catMaybes
  let typeName = [qq|Decimal_{totalDigits}_{decimalDigits}|]
  let ioTsSchemaName = typeNameToIoTsSchemaName ctx schemaName
  let schemaPart = [qq|export const $ioTsSchemaName = {genRefinement baseSchema validateFnName typeName}|]
  return $ [regexGenPart] <> constantsPart <> validatePart <> [schemaPart, typePart]
genDecimalSchemaConstruction _ x _ = Left $ "Invalid OA type passed into genDecimalSchemaConstruction: " <> toS (pShow x)

genAliasConstruction :: WorkContext -> OA -> OAMeta -> Either Text [Text]
genAliasConstruction ctx OAAlias {..} meta =
  return [[qq|export const $schemaName = $targetSchemaName;|]]
  where
    typeName = meta ^. #name
    targetSchemaName = typeNameToIoTsSchemaName ctx aliasTarget
    schemaName = typeNameToIoTsSchemaName ctx typeName
genAliasConstruction _ x _ =
  Left $ "Invalid OA type passed into genAliasConstruction: " <> toS (pShow x)

genDocRenderCtx :: WorkContext -> Text -> Maybe CustomEndpointsMainFile -> [RenderEndpointInfo] -> AppM DocRenderContext
genDocRenderCtx ctx groupName customEndpointsMain xs = do
  let allImportsFromEndpoints = xs <&> (^. #dependencies) & join & sort & nub
  imports <- genDepImports ctx Nothing allImportsFromEndpoints <&> T.intercalate "\n"
  let customEndpoints =
        customEndpointsMain
          <&> (^. #files)
          & fromMaybe (M.fromList [])
          & M.findWithDefault (CustomEndpointsOneFile {code = "", imports = Nothing}) groupName
  let apiDirFromApiFile = ctx ^. #config % #apiFile <&> (toS >>> takeDirectory >>> toS)
  let apiDirFromApiPath = ctx ^. #config % #apiPath
  let apiDir = apiDirFromApiFile <|> apiDirFromApiPath
  apiInfo <- if ctx ^. #config % #apiInfoEnabled then apiDir <&> genApiInfo & sequence else return Nothing
  dateTime <- liftIO currentDateTimeStr
  return $ DocRenderContext {version = versionText, endpoints = xs, ..}
  where
    genApiInfo :: Text -> AppM ApiInfo
    genApiInfo dir = do
      gitHash <- liftEitherInIOToAppM $ Git.getHash dir <&> mapBoth (GenerationError def) Just
      let gitHashShort = gitHash <&> T.take 8
      gitBranch <- liftEitherInIOToAppM $ Git.getBranch dir <&> mapBoth (GenerationError def) Just
      return ApiInfo {..}

genHeader :: WorkContext -> OAMeta -> AppM [Text]
genHeader ctx meta = case ctx ^. #config % #header of
  Nothing -> return []
  Just headerText -> do
    rCtx <- genDocRenderCtx ctx "" Nothing [] <&> toJSON
    let updateError x = GenerationError (mkErrCtxFromMeta meta) ("Failed to generate header: " <> x)
    rendered <- renderTemplate headerText rCtx & mapLeft updateError & liftEitherToAppM
    return $ [rendered] <> newLine

newLine :: [Text]
newLine = [""]

joinParts :: [[Text]] -> AppM Text
joinParts x = x & concat & T.unlines & insertEmptyLineAtEndIfNotPresent & return
  where
    insertEmptyLineAtEndIfNotPresent xs = if T.isSuffixOf "\n\n" xs then xs <> "\n" else xs

appendNewLineIfNotEmpty :: [Text] -> [Text]
appendNewLineIfNotEmpty [] = []
appendNewLineIfNotEmpty xs = xs <> newLine

generateTypeScript :: WorkContext -> OA -> OAMeta -> AppM Text
generateTypeScript ctx x@OAClass {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  header' <- genHeader ctx meta
  let commonBeginning =
        [ header',
          imports,
          newLine,
          genDocs ctx meta & appendNewLineIfNotEmpty
        ]
  optPart <- genOptionalPart ctx Nothing x & liftEitherToAppM
  reqPart <- genRequiredPart ctx Nothing x & liftEitherToAppM
  let nonRecursive =
        [ optPart & appendNewLineIfNotEmpty,
          reqPart,
          newLine,
          genClassSchemaConstruction ctx Nothing x meta,
          newLine,
          genTypeExport ctx x meta
        ]
  tsInterface <- genTsInterface ctx (meta ^. #name) (x ^?<> #fields)
  recPart <- genRecursiveClassSchemaConstruction ctx x meta & liftEitherToAppM
  let recursive =
        [ [tsInterface],
          newLine,
          recPart
        ]
  joinParts $ commonBeginning <> bool nonRecursive recursive (isSchemaRecursive meta)
generateTypeScript ctx x@OAUnion {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  header' <- genHeader ctx meta
  unionPart <- genUnionSchemaConstruction ctx x meta & liftEitherToAppM
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      unionPart,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OAEnum {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  header' <- genHeader ctx meta
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      genEnumSchemaConstruction ctx x meta,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OAArray {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  header' <- genHeader ctx meta
  arrPart <- genArraySchemaConstruction ctx x meta & liftEitherToAppM
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      arrPart,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OAString {} meta = do
  schema <- genStringSchemaConstruction ctx x meta & mapLeft (GenerationError $ mkErrCtxFromMeta meta) & liftEitherToAppM
  imports <- genSchemaImports ctx (if x ^?? #stringPattern & isJust then StringPatternImportType else GenericImportType) meta
  header' <- genHeader ctx meta
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      schema,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OAInteger {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  schema <- genIntegerSchemaConstruction ctx x meta & mapLeft (GenerationError $ mkErrCtxFromMeta meta) & liftEitherToAppM
  header' <- genHeader ctx meta
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      schema,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OANumber {} meta = do
  imports <- genSchemaImports ctx GenericImportType meta
  schema <- genNumberSchemaConstruction ctx x meta & mapLeft (GenerationError $ mkErrCtxFromMeta meta) & liftEitherToAppM
  header' <- genHeader ctx meta
  joinParts
    [ header',
      imports,
      newLine,
      genDocs ctx meta & appendNewLineIfNotEmpty,
      schema,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OADecimal {} meta = do
  imports <- genSchemaImports ctx StringPatternImportType meta
  schema <- genDecimalSchemaConstruction ctx x meta & mapLeft (GenerationError $ mkErrCtxFromMeta meta) & liftEitherToAppM
  header' <- genHeader ctx meta
  joinParts
    [ header',
      imports,
      newLine,
      schema
    ]
generateTypeScript ctx x@OAAlias {} meta = do
  header' <- genHeader ctx meta
  imports <- genSchemaImports ctx GenericImportType meta
  schema <- genAliasConstruction ctx x meta & mapLeft (GenerationError $ mkErrCtxFromMeta meta) & liftEitherToAppM
  joinParts
    [ header',
      imports,
      newLine,
      schema,
      newLine,
      genTypeExport ctx x meta
    ]
generateTypeScript ctx x@OACustom {} meta = do
  joinParts
    [ genDocs ctx meta & appendNewLineIfNotEmpty,
      x ^? #customSchemaCode & maybeToList
    ]

rawTagToTagId :: Text -> Text
rawTagToTagId x = x & T.map (\y -> if isAlphaNum y then y else ' ') & liftToText (fromWords >>> toPascal)

groupEndpointsWork :: EndpointsGenerationMode -> [(URI, RequestType, OAEndpoint)] -> [(FilePath, Text, [(URI, RequestType, OAEndpoint)])]
groupEndpointsWork AllEndpointsInOneFile xs = [("Api.ts", "Api", xs)]
groupEndpointsWork EndpointsWithSameTagInOneFile xs =
  xs
    & groupBy ((==) `on` (^. _3 % #tag))
    <&> conv
  where
    conv :: [(URI, RequestType, OAEndpoint)] -> (FilePath, Text, [(URI, RequestType, OAEndpoint)])
    conv ys =
      let tagId = ys & head & (^. _3 % #tag) & rawTagToTagId & (<> "Api")
       in (tagId <> ".ts" & toS, tagId, ys)

extractSuccessResponse :: M.HashMap Int OARequestOrResponse -> Maybe (Int, OARequestOrResponse)
extractSuccessResponse xs = xs & M.filterWithKey (\x _ -> isHttpSuccessCode x) & M.toList & sortBy (comparing fst) & headMay

typeNameToIoTsSchemaName :: WorkContext -> Text -> Text
typeNameToIoTsSchemaName _ = (<> "Schema")

convEndpointToRenderInfo :: WorkContext -> (URI, RequestType, OAEndpoint) -> AppM RenderEndpointInfo
convEndpointToRenderInfo ctx (url, _, OAEndpoint {..}) = do
  let cfg = ctx ^. #config
  let errInfo = [qq|$url {requestTypeToText requestType} $operationId|]
  let missingRespErr = GenerationError def [qq|Failed to extract success response in endpoint: $errInfo|]
  (expectedStatusCode, resp) <- extractSuccessResponse responses & maybeToEither missingRespErr & liftEitherToAppM
  let maySchemaToTsType e x = x <&> oaTypeToTsType & (<|> Just e)
  let maySchemaToIoTsType e x = x <&> oaTypeToIoTsType def FieldLocationRequired & maybeEitherToEitherMaybe <&> (<|> Just e)
  let reqMime = request ^? _Just % #mime % _Just
  let isRequestFileUpload = isJust reqMime && reqMime /= Just mimeAppJson
  let requestTsType =
        if isRequestFileUpload
          then Just $ cfg ^. #uploadFileRequestType
          else request ^? _Just % #schema <&> maySchemaToTsType "void" & join
  requestSchema <-
    if isRequestFileUpload
      then return $ Just $ cfg ^. #uploadFileRequestSchema
      else request ^? _Just % #schema & join & maySchemaToIoTsType (cfg ^. #emptyRequestSchema) & liftEitherToAppM
  let isResponseFileDownload = isJust (resp ^. #mime) && resp ^. #mime /= Just mimeAppJson
  let responseTsType =
        if isResponseFileDownload
          then Just $ cfg ^. #downloadFileResponseType
          else resp ^. #schema & maySchemaToTsType (cfg ^. #emptyResponseType)
  responseSchema <-
    if isResponseFileDownload
      then return $ Just $ cfg ^. #downloadFileResponseSchema
      else resp ^. #schema & maySchemaToIoTsType (cfg ^. #emptyResponseSchema) & liftEitherToAppM
  (queryParamsTypeName, queryParamsTypeDefinition, queryParamsSchemaName) <-
    mkParamsTypeAndDef QueryParameterInType parameters
  (pathParamsTypeName, pathParamsTypeDefinition, pathParamsSchemaName) <-
    mkParamsTypeAndDef PathParameterInType parameters
  let queryParamsArgumentName = queryParamsTypeName $> queryParamsVariableName
  let pathParamsArgumentName = pathParamsTypeName $> pathParamsVariableName
  let preparedFunctionArguments =
        [ requestTsType <&> ("data: " <>),
          queryParamsTypeName <&> ((queryParamsVariableName <> ": ") <>),
          Just (\tName argName -> (argName <> ": ") <> tName) <*> pathParamsTypeName <*> pathParamsArgumentName
        ]
          <&> maybeToList
          & join
          & T.intercalate ", "
  urlWithParams <- mkUrlWithParams errInfo parameters url
  let dependencies :: [Text] =
        [ request ^? _Just % #schema & join & maybeToList,
          resp ^. #schema & maybeToList,
          parameters <&> (^. #schema)
        ]
          & join
          <&> depFromTOA
          <&> maybeToList
          & join
          & sort
          & nub
  imports <- do
    let meta :: OAMeta = def & #name .~ ("Endpoint<" <> errInfo <> ">") & #path .~ (ctx ^. #config % #apiFile)
    forM dependencies (genDepImport ctx (Just meta)) <&> T.intercalate "\n"
  return $ RenderEndpointInfo {..}
  where
    pathParamsVariableName :: Text = ctx ^. #config % #pathParamsParameterName
    queryParamsVariableName :: Text = ctx ^. #config % #queryParamsParameterName
    mkUrlWithParams :: Text -> [OAParameter] -> Text -> AppM Text
    mkUrlWithParams errInfo params url = do
      let relevantParams = parameters & filter (\x -> x ^. #inn == PathParameterInType)
      let paramNames = relevantParams <&> (^. #name)
      let regexStr = "\\{([a-zA-Z0-9]+)\\}"
      findRegex <- liftIO $ compileRegex regexStr
      replaceRegex <- liftIO $ compileSearchReplace regexStr [qq|$$\{$pathParamsVariableName.$1\}|]
      let paramNamesFromUrl :: [Text] = url *=~ findRegex & allMatches <&> matchedText <&> maybeToList & join
      let paramNamesInUrlFormat = paramNames <&> surround "{" "}"
      forM_ paramNamesFromUrl $ \x ->
        unless (x `elem` paramNamesInUrlFormat) $
          throwError $ GenerationError def [qq|Query parameter "$x" in URL "$url" not found in parameters: $errInfo ; params = $params; paramNames = $paramNames|]
      forM_ paramNamesInUrlFormat $ \x ->
        unless (x `elem` paramNamesFromUrl) $
          throwError $ GenerationError def [qq|Query parameter "$x" not found in URL "$url": $errInfo ; params = $params; paramNames = $paramNames|]
      return $ url *=~/ replaceRegex
    mkParamsTypeAndDef :: OAParameterIn -> [OAParameter] -> AppM (Maybe Text, Maybe Text, Maybe Text)
    mkParamsTypeAndDef parTyp rawParams = do
      let params = rawParams & filter (\x -> x ^. #inn == parTyp)
      if null params
        then return def
        else do
          let paramsName = operationId & capitalize & (<> bool "QueryParams" "PathParams" (parTyp == PathParameterInType))
          paramsDef <- paramsToDef paramsName params
          return (Just paramsName, Just paramsDef, Just $ typeNameToIoTsSchemaName ctx paramsName)
    paramsToDef :: Text -> [OAParameter] -> AppM Text
    paramsToDef pName xs = do
      let meta :: OAMeta = def & #name .~ pName
      let x =
            OAClass
              { extends = Nothing,
                fields = xs <&> paramToField,
                tag = Nothing,
                overrideNonRequiredRepresentation = Nothing
              }
      let partPrefix = Just $ deCapitalize pName
      optPart <- genOptionalPart ctx partPrefix x & liftEitherToAppM
      reqPart <- genRequiredPart ctx partPrefix x & liftEitherToAppM
      joinParts
        [ genDocs ctx meta & appendNewLineIfNotEmpty,
          optPart,
          reqPart,
          genClassSchemaConstruction ctx partPrefix x meta,
          genTypeExport ctx x meta
        ]
    paramToField :: OAParameter -> FieldOA
    paramToField OAParameter {..} =
      FieldOA
        { typ = schema,
          required = bool OptionalOA RequiredOA required,
          ..
        }

generateEndpointsTypeScript :: WorkContext -> EndpointsGenerationMode -> Text -> OARoutes -> Maybe CustomEndpointsMainFile -> AppM [GeneratedEndpointFile]
generateEndpointsTypeScript ctx egm tpl routes customEndpoints = do
  let dLog tag msg = liftIO $ ctx ^. #debugLog $ "generateEndpointsTypeScript: " <> tag <> " = " <> msg
  let endpoints :: [(URI, RequestType, OAEndpoint)] = routes & M.toList >>= uncurry convOneRoute
  dLog "endpoints" (pShow endpoints)
  let workPerFile = groupEndpointsWork egm endpoints
  dLog "workPerFile" (pShow workPerFile)
  forM workPerFile (uncurry3 processFile)
  where
    convOneRoute url rtEpMap = M.toList rtEpMap <&> (\(rt, ep) -> (url, rt, ep))
    processFile :: FilePath -> Text -> [(URI, RequestType, OAEndpoint)] -> AppM GeneratedEndpointFile
    processFile filePath name xs = do
      endpointToRenderInfos <- xs & sortOn (\(_, _, x) -> (x ^. #tag, x ^. #operationId)) & mapM (convEndpointToRenderInfo ctx)
      rCtx <- genDocRenderCtx ctx name customEndpoints endpointToRenderInfos <&> toJSON
      let errCtx = mkErrCtxFromFileNameOrDef $ ctx ^. #config % #apiFile
      let updateError x = GenerationError errCtx ("Failed to generate endpoints: " <> x)
      rendered <- renderTemplate tpl rCtx & mapLeft updateError & liftEitherToAppM
      return $ GeneratedEndpointFile {content = rendered, wholeFilePath = filePath}
