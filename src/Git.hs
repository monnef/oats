{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}

module Git
  ( getHash,
    getBranch,
  )
where

import qualified Data.Text as T
import System.Exit (ExitCode (..))
import Utils

runGitCmd :: Text -> Text -> Text -> IO (Either Text Text)
runGitCmd cmdDesc cmd targetDir = do
  (out, err, code) <- runCmd "sh" ["-c", cmd] False targetDir
  case code of
    ExitSuccess -> return $ Right (out <&> T.strip & joinNl)
    ExitFailure c -> return $ Left [qq|Failed to $cmdDesc, command `$cmd` retuned $c:${nl}$out${nl}$err|]

getHash :: Text -> IO (Either Text Text)
getHash = runGitCmd "get git hash" "git rev-parse HEAD"

getBranch :: Text -> IO (Either Text Text)
getBranch = runGitCmd "get git branch" "git rev-parse --abbrev-ref HEAD"
