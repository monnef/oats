{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}

module Configuration where

import Control.Applicative ((<|>))
import Control.Monad.Except (throwError)
import Control.Monad.Trans (liftIO)
import Data
import Data.Bifunctor (first)
import Data.ColorMode (ColorMode (..))
import Data.Default (def)
import Data.Either.Combinators (maybeToRight)
import Data.Maybe (fromMaybe)
import qualified Data.Yaml as Y
import Optics
import Text.RE.PCRE.Text (RE, compileRegex)
import Utils

prepareRegex :: Text -> AppM RE
prepareRegex x = x & toS & compileRegex & maybeToRight (ConfigError [qq|Failed to compile regex: $x|]) & liftEitherToAppM

prepareOptionalRegexFromConfigFile :: Text -> Text -> Maybe Text -> Either AppError (Maybe RE)
prepareOptionalRegexFromConfigFile errMsg optionName (Just x) =
  x & toS & compileRegex & maybeToRight (ConfigError [qq|Failed to parse $optionName regex ($errMsg): `$x`|]) <&> Just
prepareOptionalRegexFromConfigFile _ _ Nothing = return Nothing

maybeConfigToConfig :: MaybeConfig -> AppM Config
maybeConfigToConfig MaybeConfig {..} = do
  let d = def :: Config
  ignoreFilesRegex <- prepareOptionalRegexFromConfigFile "maybeConfigToConfig" "ignore files" ignoreFilesRegex & liftEitherToAppM
  ignoreComponentsRegex <- prepareOptionalRegexFromConfigFile "maybeConfigToConfig" "ignore files" ignoreComponentsRegex & liftEitherToAppM
  ignoreEndpointsByPathRegex <- prepareOptionalRegexFromConfigFile "maybeConfigToConfig" "ignore files" ignoreEndpointsByPathRegex & liftEitherToAppM
  ignoreRules <- ignoreRules & validateAndConvertIgnoreRules
  return $
    Config
      { nonRequiredRepresentation = fromMaybe (d ^. #nonRequiredRepresentation) nonRequiredRepresentation,
        schemaTranslation = fromMaybe (d ^. #schemaTranslation) schemaTranslation,
        propertyNameMap = fromMaybe (d ^. #propertyNameMap) propertyNameMap,
        useExcess = fromMaybe (d ^. #useExcess) useExcess,
        excessImport = excessImport <|> (d ^. #excessImport),
        apiPath = apiPath <|> (d ^. #apiPath),
        titleFromCmdArgs = Nothing,
        apiModule = fromMaybe (d ^. #apiModule) apiModule,
        ignoreWarnings = fromMaybe (d ^. #ignoreWarnings) ignoreWarnings,
        promoteWarningsToErrors = fromMaybe (d ^. #promoteWarningsToErrors) promoteWarningsToErrors,
        emptyRequestSchema = fromMaybe (d ^. #emptyRequestSchema) emptyRequestSchema,
        emptyResponseSchema = fromMaybe (d ^. #emptyResponseSchema) emptyResponseSchema,
        emptyResponseType = fromMaybe (d ^. #emptyResponseType) emptyResponseType,
        downloadFileResponseSchema = fromMaybe (d ^. #downloadFileResponseSchema) downloadFileResponseSchema,
        downloadFileResponseType = fromMaybe (d ^. #downloadFileResponseType) downloadFileResponseType,
        uploadFileRequestSchema = fromMaybe (d ^. #uploadFileRequestSchema) uploadFileRequestSchema,
        uploadFileRequestType = fromMaybe (d ^. #uploadFileRequestType) uploadFileRequestType,
        pathParamsParameterName = fromMaybe (d ^. #pathParamsParameterName) pathParamsParameterName,
        queryParamsParameterName = fromMaybe (d ^. #queryParamsParameterName) queryParamsParameterName,
        apiFilesToMerge = fromMaybe (d ^. #apiFilesToMerge) apiFilesToMerge,
        quiet = fromMaybe (d ^. #quiet) quiet,
        verbose = fromMaybe (d ^. #verbose) verbose,
        ignoreErrors = fromMaybe (d ^. #ignoreErrors) ignoreErrors,
        noSummary = d ^. #noSummary,
        noTiming = fromMaybe (d ^. #noTiming) noTiming,
        colorOutput = fromMaybe (d ^. #colorOutput) (color <&> bool NoColor UseColor),
        noPatternValidationWithExample = fromMaybe (d ^. #noPatternValidationWithExample) noPatternValidationWithExample,
        cleanOutputDirectory = fromMaybe (d ^. #cleanOutputDirectory) cleanOutputDirectory,
        dryRun = d ^. #dryRun,
        afterFileWriteCommand = afterFileWriteCommand,
        apiInfoEnabled = fromMaybe (d ^. #apiInfoEnabled) apiInfoEnabled,
        numberMinMaxLimitsEnabled = fromMaybe (d ^. #numberMinMaxLimitsEnabled) numberMinMaxLimitsEnabled,
        numberMinMaxLimitsTemplate = fromMaybe (d ^. #numberMinMaxLimitsTemplate) numberMinMaxLimitsTemplate,
        numberMinMaxLimitsMode = fromMaybe (d ^. #numberMinMaxLimitsMode) numberMinMaxLimitsMode,
        numberMinMaxLimitsCombinedEnabled = fromMaybe (d ^. #numberMinMaxLimitsCombinedEnabled) numberMinMaxLimitsCombinedEnabled,
        numberMinMaxLimitsCombinedTemplate = fromMaybe (d ^. #numberMinMaxLimitsCombinedTemplate) numberMinMaxLimitsCombinedTemplate,
        numberMinMaxLimitsCombinedMode = fromMaybe (d ^. #numberMinMaxLimitsCombinedMode) numberMinMaxLimitsCombinedMode,
        ..
      }

validateWarningNames :: [Text] -> Either AppError [Text]
validateWarningNames = mapM checkName
  where
    checkName :: Text -> Either AppError Text
    checkName name = if name `elem` appWarningNames then return name else Left (ConfigError [qq|Unknown warning name "$name"|])

validateErrorNames :: [Text] -> Either AppError [Text]
validateErrorNames = mapM checkName
  where
    checkName :: Text -> Either AppError Text
    checkName name = if name `elem` nonCriticalErrorNames then return name else Left (ConfigError [qq|Unknown error name "$name"|])

validateAndConvertIgnoreRules :: Maybe [IgnoreRuleRaw] -> AppM [IgnoreRule]
validateAndConvertIgnoreRules Nothing = return []
validateAndConvertIgnoreRules (Just xs) = mapM validateAndConvertIgnoreRule xs

validateAndConvertIgnoreRule :: IgnoreRuleRaw -> AppM IgnoreRule
validateAndConvertIgnoreRule irr@IgnoreRuleRaw {..} = do
  errors <- validateIgnoreRuleErrors errors
  warnings <- validateIgnoreRuleWarnings warnings
  selector <- validateAndConvertIgnoreRuleSelector irr

  return $
    IgnoreRule
      { selector,
        targets = IgnoreRuleTargets {..}
      }

validateAndConvertIgnoreRuleSelector :: IgnoreRuleRaw -> AppM IgnoreRuleSelector
validateAndConvertIgnoreRuleSelector IgnoreRuleRaw {operationId = Just opId, url = Nothing, schemaName = Nothing} = do
  operationIdRegex <- prepareRegex opId
  return $ IgnoreRuleEndpointByOperationId {..}
validateAndConvertIgnoreRuleSelector IgnoreRuleRaw {operationId = Nothing, url = Just url, schemaName = Nothing} = do
  urlRegex <- prepareRegex url
  return $ IgnoreRuleEndpointByUrl {..}
validateAndConvertIgnoreRuleSelector IgnoreRuleRaw {operationId = Nothing, url = Nothing, schemaName = Just schemaName} = do
  schemaNameRegex <- prepareRegex schemaName
  return $ IgnoreRuleSchemaByName {..}
validateAndConvertIgnoreRuleSelector x = throwError (ConfigError [qq|Invalid combination of selectors in ignore rule: $x|])

validateIgnoreRuleErrors :: Maybe [Text] -> AppM [Text]
validateIgnoreRuleErrors Nothing = return []
validateIgnoreRuleErrors (Just xs) = xs & validateErrorNames & liftEitherToAppM

validateIgnoreRuleWarnings :: Maybe [Text] -> AppM [Text]
validateIgnoreRuleWarnings Nothing = return []
validateIgnoreRuleWarnings (Just xs) = xs & validateWarningNames & liftEitherToAppM

augmentConfigWithCmdArgs :: Config -> CmdArgs -> Either AppError Config
augmentConfigWithCmdArgs cfg cmdArgs = do
  ignoreFilesRegexFromCmdArgs <- prepareOptionalRegexFromConfigFile "augmentConfigWithCmdArgs" "ignore files" (cmdArgs ^. #ignoreFilesRegex)
  return $
    Config
      { nonRequiredRepresentation = bool (cfg ^. #nonRequiredRepresentation) UndefinedAsNonRequired $ cmdArgs ^. #nonRequiredAsUndefined,
        schemaTranslation = cfg ^. #schemaTranslation,
        propertyNameMap = cfg ^. #propertyNameMap,
        useExcess = cfg ^. #useExcess,
        excessImport = cfg ^. #excessImport,
        quiet = cfg ^. #quiet || cmdArgs ^. #quiet,
        ignoreFilesRegex = ignoreFilesRegexFromCmdArgs <|> cfg ^. #ignoreFilesRegex,
        apiPath = cmdArgs ^. #apiPath <|> cfg ^. #apiPath,
        apiFile = cmdArgs ^. #apiFile <|> cfg ^. #apiFile,
        stringPatternImport = cfg ^. #stringPatternImport,
        stringPatternSchemaGenerator = cfg ^. #stringPatternSchemaGenerator,
        header = cfg ^. #header,
        titleFromCmdArgs = cmdArgs ^. #title,
        outPath = cmdArgs ^. #outPath <|> cfg ^. #outPath,
        apiModule = cfg ^. #apiModule,
        customSchemasPath = cfg ^. #customSchemasPath,
        ignoreComponentsRegex = cfg ^. #ignoreComponentsRegex,
        endpointsGenerationMode = cfg ^. #endpointsGenerationMode,
        endpointsTemplate = cfg ^. #endpointsTemplate,
        emptyRequestSchema = cfg ^. #emptyRequestSchema,
        emptyResponseType = cfg ^. #emptyResponseType,
        emptyResponseSchema = cfg ^. #emptyResponseSchema,
        downloadFileResponseSchema = cfg ^. #downloadFileResponseSchema,
        downloadFileResponseType = cfg ^. #downloadFileResponseType,
        uploadFileRequestSchema = cfg ^. #uploadFileRequestSchema,
        uploadFileRequestType = cfg ^. #uploadFileRequestType,
        pathParamsParameterName = cfg ^. #pathParamsParameterName,
        queryParamsParameterName = cfg ^. #queryParamsParameterName,
        ignoreEndpointsByPathRegex = cfg ^. #ignoreEndpointsByPathRegex,
        customEndpointsFile = cfg ^. #customEndpointsFile,
        endpointsTemplateFile = cfg ^. #endpointsTemplateFile,
        ignoreWarnings = cfg ^. #ignoreWarnings,
        promoteWarningsToErrors = cfg ^. #promoteWarningsToErrors,
        apiFilesToMerge = cfg ^. #apiFilesToMerge,
        verbose = cfg ^. #verbose || cmdArgs ^. #verbose,
        ignoreErrors = cfg ^. #ignoreErrors,
        noSummary = cmdArgs ^. #noSummary,
        ignoreRules = cfg ^. #ignoreRules,
        noTiming = cfg ^. #noTiming || cmdArgs ^. #noTiming,
        colorOutput = bool UseColor NoColor (cmdArgs ^. #noColor),
        noPatternValidationWithExample = cfg ^. #noPatternValidationWithExample,
        cleanOutputDirectory = cfg ^. #cleanOutputDirectory,
        dryRun = cmdArgs ^. #dryRun,
        afterFileWriteCommand = cfg ^. #afterFileWriteCommand,
        apiInfoEnabled = cfg ^. #apiInfoEnabled,
        numberMinMaxLimitsEnabled = cfg ^. #numberMinMaxLimitsEnabled,
        numberMinMaxLimitsTemplate = cfg ^. #numberMinMaxLimitsTemplate,
        numberMinMaxLimitsMode = cfg ^. #numberMinMaxLimitsMode,
        numberMinMaxLimitsCombinedEnabled = cfg ^. #numberMinMaxLimitsCombinedEnabled,
        numberMinMaxLimitsCombinedTemplate = cfg ^. #numberMinMaxLimitsCombinedTemplate,
        numberMinMaxLimitsCombinedMode = cfg ^. #numberMinMaxLimitsCombinedMode,
        ..
      }

loadConfig :: Text -> AppM MaybeConfig
loadConfig fileName = do
  cfgEitherY <- Y.decodeFileEither (toS fileName) & liftIO
  cfgEitherY & first (Y.prettyPrintParseException >>> toS >>> ParseError (mkErrCtxFromFileName fileName)) & liftEitherToAppM

validateConfig :: Config -> AppM Config
validateConfig cfg = do
  _ <- cfg ^. #ignoreWarnings & validateWarningNames & liftEitherToAppM
  _ <- cfg ^. #promoteWarningsToErrors & validateWarningNames & liftEitherToAppM
  _ <- cfg ^. #ignoreErrors & validateErrorNames & liftEitherToAppM
  return cfg
