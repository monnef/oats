{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}

module TerminalUtils
  ( printWarn,
    printError,
    printDryRun,
    printIO,
    filterOutColorCodes,
    printResult,
    AppResult (..),
    tSetSGRCode,
    colorText,
  )
where

import Data.ColorMode (ColorMode, shouldUseColor)
import System.Console.ANSI
import System.IO (hPutStr, stderr, stdout)
import Text.RE.PCRE.Text (ed, (*=~/))
import Utils

printMsgWithColoredTag :: ColorMode -> Bool -> Text -> Color -> Text -> IO ()
printMsgWithColoredTag colorMode isError tag color msg = do
  let useColor = shouldUseColor colorMode
  let channel = bool stdout stderr isError
  let p = toS >>> hPutStr channel
  p "["
  when useColor $ hSetSGR channel [SetColor Foreground Vivid color]
  p tag
  when useColor $ hSetSGR channel [Reset]
  p "] "
  p $ bool filterOutColorCodes id useColor $ msg <> "\n"

printWarn :: ColorMode -> Text -> IO ()
printWarn colorMode = printMsgWithColoredTag colorMode True "WARN" Yellow

printError :: ColorMode -> Text -> IO ()
printError colorMode = printMsgWithColoredTag colorMode True "ERROR" Red

printDryRun :: ColorMode -> Text -> IO ()
printDryRun colorMode = printMsgWithColoredTag colorMode False "DRY-RUN" Cyan

printIO :: ColorMode -> Text -> IO ()
printIO colorMode = printMsgWithColoredTag colorMode False "IO" Blue

printInColor :: ColorMode -> Color -> Bool -> Text -> IO ()
printInColor colorMode color isError message = do
  let useColor = shouldUseColor colorMode
  let channel = bool stdout stderr isError
  when useColor $ hSetSGR channel [SetColor Foreground Vivid color]
  hPutStr channel (toS message)
  when useColor $ hSetSGR channel [Reset]

data AppResult = OkResult | WarnResult | ErrResult deriving (Show, Eq)

printResult :: ColorMode -> AppResult -> Text -> IO ()
printResult useColor appRes message = do
  let c = case appRes of
        OkResult -> Green
        WarnResult -> Yellow
        ErrResult -> Red
  printInColor useColor c (appRes == ErrResult) message

tSetSGRCode :: [SGR] -> Text
tSetSGRCode xs = setSGRCode xs & toS

colorText :: ColorIntensity -> Color -> Text -> Text
colorText cl c = surround (tSetSGRCode [SetColor Foreground cl c]) (tSetSGRCode [])

filterOutColorCodes :: Text -> Text
filterOutColorCodes = (*=~/ [ed|(\x1B\[([0-9;]{1,}[A-Za-z]|m))///|]) -- >>> ("FILTERED" <>)
