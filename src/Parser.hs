{-# LANGUAGE OverloadedStrings #-}

module Parser where

import Data
import Data.Bifunctor (first)
import qualified Data.Yaml as Y
import Utils

parseOneSchema :: ErrorContext -> Text -> AppM RawOA
parseOneSchema ctx =
  emptyStrToEmptyObj >>> toS >>> Y.decodeEither'
    >>> first (Y.prettyPrintParseException >>> toS >>> ParseError ctx)
    >>> liftEitherToAppM
  where
    emptyStrToEmptyObj "" = "{}"
    emptyStrToEmptyObj x = x

parseSingleFileApi :: ErrorContext -> Text -> AppM RawOAFile
parseSingleFileApi ctx =
  toS >>> Y.decodeEither' >>> first (Y.prettyPrintParseException >>> toS >>> ParseError ctx) >>> liftEitherToAppM

parseCustomEndpoints :: ErrorContext -> Text -> AppM CustomEndpointsMainFile
parseCustomEndpoints ctx =
  toS >>> Y.decodeEither' >>> first (Y.prettyPrintParseException >>> toS >>> ParseError ctx) >>> liftEitherToAppM
