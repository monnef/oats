{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module DataProcessor where

import Control.Applicative ((<|>))
import Control.Monad (foldM, liftM2, unless)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Except (throwError)
import Data
import qualified Data.Aeson as A
import Data.Bifunctor (second)
import Data.Default (def)
import Data.Either.Combinators (maybeToRight)
import Data.Either.Extra (eitherToMaybe, mapLeft, maybeToEither)
import Data.Fixed (mod')
import Data.Function (on)
import qualified Data.HashMap.Lazy as M
import Data.List (find, groupBy, nub, sortBy, sortOn, (\\))
import Data.Maybe (catMaybes, fromMaybe, isJust, isNothing, maybeToList)
import Data.Ord (comparing)
import qualified Data.Text as T
import Optics hiding (allOf, re)
import PatternChecker (validateStringPatternAndExample)
import Safe (fromJustNote, headMay)
import System.FilePath (takeBaseName)
import Text.RE.PCRE.Text
import Text.RE.Replace ((!$$?))
import Utils

typeNameToTypeOA :: Text -> Maybe TypeOA
typeNameToTypeOA "string" = Just $ PrimitiveTOA StringPOA
typeNameToTypeOA "boolean" = Just $ PrimitiveTOA BooleanPOA
typeNameToTypeOA "integer" = Just $ PrimitiveTOA IntegerPOA
typeNameToTypeOA "number" = Just $ PrimitiveTOA NumberPOA
typeNameToTypeOA _ = Nothing

pathToSchemaToSchemaName :: Text -> Text
pathToSchemaToSchemaName = T.splitOn "/" >>> last >>> stripSuffixIfPresent ".yaml"

refToTypeOA :: SchemaTranslationTable -> Text -> Maybe TypeOA
refToTypeOA _ "" = Nothing
refToTypeOA tbl x = x & pathToSchemaToSchemaName & \name -> M.lookupDefault (SchemaTOA name) name tbl & Just

arrayPropToTypeOA :: ErrorContext -> SchemaTranslationTable -> RawOA -> AppM (Maybe TypeOA)
arrayPropToTypeOA errCtx tbl x = do
  rawItems <- case (x ^. #typ, x ^. #items) of
    (Just "array", Just y) -> return $ Just y
    _ -> return Nothing
  itemType <- rawItems <&> rawOAToTypeOA errCtx tbl & sequence <&> join
  return $ itemType <&> ArrayTOA

oneValueEnumToTypeOA :: RawOA -> AppM (Maybe TypeOA)
oneValueEnumToTypeOA x = return $ x ^. #enum <&> singleListToJust & join <&> OneValueEnumTOA

isStringFormatDate :: RawOA -> Bool
isStringFormatDate RawOA {typ = Just "string", format = Just "date"} = True
isStringFormatDate _ = False

isStringFormatDateTime :: RawOA -> Bool
isStringFormatDateTime RawOA {typ = Just "string", format = Just "date-time"} = True
isStringFormatDateTime _ = False

isStringFormatUuid :: RawOA -> Bool
isStringFormatUuid RawOA {typ = Just "string", format = Just "uuid"} = True
isStringFormatUuid _ = False

isStringWithFormat :: RawOA -> Bool
isStringWithFormat x = [isStringFormatDate, isStringFormatDateTime, isStringFormatUuid] & any ($ x)

stringFormatStringToSchemaName :: Text -> Maybe Text
stringFormatStringToSchemaName "date" = Just "Date"
stringFormatStringToSchemaName "date-time" = Just "DateTime"
stringFormatStringToSchemaName "uuid" = Just "Uuid"
stringFormatStringToSchemaName _ = Nothing

dateStringPropToTypeOA :: RawOA -> AppM (Maybe TypeOA)
dateStringPropToTypeOA x =
  if
      | isStringFormatDate x -> w $ SchemaTOA "Date"
      | isStringFormatDateTime x -> w $ SchemaTOA "DateTime"
      | isStringFormatUuid x -> w $ SchemaTOA "Uuid"
      | otherwise -> return Nothing
  where
    w = return . Just

expectNoValidationsFilled :: ErrorContext -> RawOA -> AppM RawOA
expectNoValidationsFilled errCtx x = do
  when (isAnyValidationFilled x) $
    addNonCriticalError $ NonCriticalError errCtx ForbiddenValidationError
  forM_ (x ^. #properties <&> M.toList & maybeToList & join) $
    \(k, v) -> expectNoValidationsFilled (errCtx & #subUnitName ?~ k) v
  return x

rawOAToTypeOA :: ErrorContext -> SchemaTranslationTable -> RawOA -> AppM (Maybe TypeOA)
rawOAToTypeOA errCtx tbl rawProp = do
  prop <- expectNoValidationsFilled errCtx rawProp
  let conversions :: [AppM (Maybe TypeOA)] =
        [ dateStringPropToTypeOA prop,
          oneValueEnumToTypeOA prop,
          prop ^. #typ >>= typeNameToTypeOA & return,
          prop ^. #ref >>= refToTypeOA tbl & return,
          arrayPropToTypeOA errCtx tbl prop
        ]
  foldl (liftM2 (<|>)) (return Nothing) conversions

propToField :: SchemaTranslationTable -> Maybe Text -> [Text] -> Maybe Text -> (Text, RawOA) -> AppM FieldOA
propToField tbl fileName requiredProps schemaName (name, prop) = do
  let errCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName & #subUnitName ?~ name
  typeMay <- rawOAToTypeOA errCtx tbl prop
  let schemaPart :: Text = schemaName <&> (\x -> [qq|'{x}'.|]) & fromMaybe ""
  let errText =
        [ [qq|Failed to convert a property to a field: {schemaPart}'{name}'.|],
          "This means the property has invalid or unsupported format. A common mistake is forgetting to use $ref.",
          pShow prop
        ]
          & T.intercalate nl
  let err = ValidationError (mkErrCtxFromFileNameOrDef fileName) errText
  typ <- maybeToRight err typeMay & liftEitherToAppM
  let required = bool OptionalOA RequiredOA $ name `elem` requiredProps
  let description = prop ^. #description <|> prop ^. #comment
  return $ FieldOA {required, typ, name, description}

applyPropNameMap :: PropertyNameMap -> FieldOA -> FieldOA
applyPropNameMap pnm fld = fld & #name .~ newName
  where
    oldName = fld ^. #name
    newName = M.lookupDefault oldName oldName pnm

isSimpleObject :: RawOA -> Bool
isSimpleObject RawOA {..} = isJust properties && elem typ [Just "object", Nothing]

isInheritingObject :: RawOA -> Bool
isInheritingObject RawOA {..} = isJust allOf

isUnion :: RawOA -> Bool
isUnion RawOA {..} = isJust oneOf

isEnum :: RawOA -> Bool
isEnum RawOA {..} = isJust enum

isArray :: RawOA -> Bool
isArray RawOA {..} = typ == Just "array"

isString :: RawOA -> Bool
isString RawOA {..} = typ == Just "string"

isBoolean :: RawOA -> Bool
isBoolean RawOA {..} = typ == Just "boolean"

isInteger :: RawOA -> Bool
isInteger RawOA {..} = typ == Just "integer"

isNumber :: RawOA -> Bool
isNumber RawOA {..} = typ == Just "number"

isStringDecimal :: RawOA -> Bool
isStringDecimal RawOA {..} = typ == Just "string" && isJust (description >>= extractDecimalParams)

isRef :: RawOA -> Bool
isRef RawOA {..} = isJust ref

getTranslationTable :: Config -> SchemaTranslationTable
getTranslationTable cfg = bool (M.fromList []) defaultSchemaTranslationTable $ cfg ^. #schemaTranslation

validateRequiredReferencesExist :: Config -> Maybe Text -> Maybe Text -> M.HashMap Text RawOA -> [Text] -> Either AppError ()
validateRequiredReferencesExist _ fileName schemaName props required = case wrongReqs of
  [] -> Right ()
  xs -> do
    let ctx = mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName
    let msg = "`required` is referencing one or more non-existing properties: " <> T.intercalate "," (xs <&> \x -> [qq|'{x}'|]) <> "."
    Left $ ValidationError ctx msg
  where 
    wrongReqs = required & filter (flip M.member props >>> not)

allValidators :: [ValidationItem]
allValidators =
  $( genValidationListTH
       [ "pattern",
         "minimum",
         "maximum",
         "minLength",
         "maxLength",
         "items",
         "exclusiveMinimum",
         "exclusiveMaximum",
         "multipleOf",
         "uniqueItems",
         "minItems",
         "maxItems"
       ]
   )

validateDisallowedValidators :: Config -> Maybe Text -> Maybe Text -> RawOA -> [ValidationItem] -> AppM ()
validateDisallowedValidators _ fileName schemaName x allowedValidators = do
  let ctx = mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName
  let unknownValidators = filter (`notElem` allValidators) allowedValidators
  _ <-
    if null unknownValidators
      then return ()
      else throwError $ ValidationError ctx $ "One or more allowed validators not found in all validators: " <> ((unknownValidators <&> \v -> v ^. #name) & T.intercalate ", ")
  let disallowedValidators = allValidators \\ allowedValidators
  let disallowedValidatorsWithValues = disallowedValidators <&> processValidator & catMaybes
  unless (null disallowedValidatorsWithValues) $
    addNonCriticalError $ NonCriticalError ctx $ DisallowedValidationError disallowedValidatorsWithValues
  where
    processValidator :: ValidationItem -> Maybe (Text, Text)
    processValidator v = (v ^. #getter) x <&> (v ^. #name,)

extractTitles :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> RawOA -> [(TitleSource, Text)]
extractTitles cfg mode fileName schemaName x =
  [ x ^. #title <&> (TitleFromSchemaField,),
    schemaName <&> (TitleFromSchemaKey,),
    if mode == SingleFileMode then Nothing else fileName <&> liftToText takeBaseName <&> (TitleFromFileName,),
    cfg ^. #titleFromCmdArgs <&> (TitleFromCmdArg,)
  ]
    <&> maybeToList
    & join

extractTitle :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> RawOA -> AppM Text
extractTitle cfg mode fileName schemaName x = do
  let titles = extractTitles cfg mode fileName schemaName x
  when (null titles) $ throwError $ ValidationError errCtx "Failed to extract/deduce title"
  validateAllTitlesAreSame titles
  addWarnOnMultipleSources titles
  return $ head titles & snd
  where
    errCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName
    validateAllTitlesAreSame :: [(TitleSource, Text)] -> AppM ()
    validateAllTitlesAreSame xs = do
      let uniqueNames = xs <&> snd & nub
      if length uniqueNames == 1
        then return ()
        else throwError $ ValidationError errCtx [qq|Schema has multiple different names: {uniqueNames}|]
    addWarnOnMultipleSources :: [(TitleSource, Text)] -> AppM ()
    addWarnOnMultipleSources xs = when (length xs > 1) $ addWarning $ AppWarning errCtx (MultipleTitleSources $ xs <&> fst)

extractDecimalParams :: Text -> Maybe (Int, Int)
extractDecimalParams x = (,) <$> (match !$$? [cp|1|]) <*> (match !$$? [cp|2|]) <&> over each (toS >>> read)
  where
    regex = [re|format:\s*decimal\(\s*(\d+)\s*,\s*(\d+)\s*\)|]
    match = x ?=~ regex

extractDecimalPlacesFromMultipleOf :: Double -> Int
extractDecimalPlacesFromMultipleOf x =
  if x == 0 then 0 else round (- (logBase 10 (abs x)))

genExample :: RawOA -> Maybe Text
genExample oa = (oa ^. #example) <&> (A.encode >>> toS)

depFromTOA :: TypeOA -> Maybe Text
depFromTOA (SchemaTOA x) = Just x
depFromTOA (ArrayTOA x) = depFromTOA x
depFromTOA _ = Nothing

removeTranslatedSchemas :: SchemaTranslationTable -> [Text] -> [Text]
removeTranslatedSchemas tbl xs = xs & filter notTranslated
  where
    notTranslated x = not $ M.member x tbl

processMissingTitleWarning :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> RawOA -> AppM ()
processMissingTitleWarning _ mode fileName schemaName x = do
  let noTitle = isNothing (x ^. #title)
  let shouldHaveTitle = mode /= SingleFileMode
  when (noTitle && shouldHaveTitle) $ addWarning $ AppWarning (mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName) MissingTitleWarning

-- -- | Check `example` field against OA type and its validators
processExampleChecks :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> Maybe Text -> RawOA -> AppM ()
processExampleChecks cfg mode fileName schemaName subUnitName' x = do
  let isEligible = ([isArray, isStringDecimal, isString, isInteger] <&> ($ x) & or) && not (isEnum x)
  title' <- x & extractTitle cfg mode fileName schemaName & evalAppM def <&> eitherToMaybe & liftIO
  let errCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName & #unitName .~ title' & #subUnitName .~ subUnitName'
  let missingWarn = AppWarning errCtx MissingExampleWarning
  let noExample = isNothing (x ^. #example)
  -- TODO: rewrite to check arrays recursively (currently only 1D and 2D are supported)
  let noExampleInItems = isNothing (x ^? #items % _Just % #example & join)
  let noExampleInItemsOfItems = isNothing (x ^? #items % _Just % #items % _Just % #example & join)
  let itemsAreRef = isJust (x ^? #items % _Just % #ref & join)
  let itemsOfItemsAreRef = isJust (x ^? #items % _Just % #items % _Just % #ref & join)
  when (isEligible && noExample && (noExampleInItems && not itemsAreRef && noExampleInItemsOfItems && not itemsOfItemsAreRef)) $ addWarning missingWarn
  forM_ (x ^. #example) $ validateExample errCtx
  where
    validateExample :: ErrorContext -> A.Value -> AppM ()
    validateExample errCtx ex = do
      let thr = ValidationError errCtx >>> throwError
      when (isString x) $ do
        case unpackAesonString ex of
          Just exStr -> processStringValidators thr exStr
          Nothing -> thr [qq|Expected string type to have a string as an example, but got $ex|]
      when (isNumber x) $ do
        case ex & unpackAesonNumberAsDouble of
          Just exNum -> processNumberValidators thr exNum
          Nothing -> thr [qq|Expected number type to have a number as an example, but got $ex|]
      when (isBoolean x && not (isAesonValueBoolean ex)) $
        thr [qq|Expected boolean type to have a boolean as an example, but got $ex|]
      let enumItems = x ^. #enum & fromMaybe []
      when (isEnum x && (ex & unpackAesonString & any (`notElem` enumItems))) $
        thr [qq|Expected enum type to have an item from `enum` as an example, but got $ex. Enum items are $enumItems.|]
    processStringValidators thr exStr = do
      let exLen = T.length exStr
      forM_ (x ^. #minLength) $ \ml ->
        when (exLen < ml) $
          thr [qq|Invalid string example: expected length ≧ $ml (`minLength`), got $exLen ({genJsStringLiteral exStr}).|]
      forM_ (x ^. #maxLength) $ \ml ->
        when (exLen > ml) $
          thr [qq|Invalid string example: expected length ≦ $ml (`maxLength`), got $exLen ({genJsStringLiteral exStr}).|]
    processNumberValidators :: (Text -> AppM ()) -> Double -> AppM ()
    processNumberValidators thr exNum = do
      forM_ (x ^. #minimum) $ \min ->
        when (exNum < min) $
          thr [qq|Invalid number example: expected value ≧ $min (`minimum`), but got $exNum.|]
      forM_ (x ^. #maximum) $ \max ->
        when (exNum > max) $
          thr [qq|Invalid number example: expected value ≦ $max (`maximum`), but got $exNum.|]
      forM_ (x ^. #multipleOf) $ \multOf ->
        unless (isMultipleOf exNum multOf) $
          thr [qq|Invalid number example: expected value {genJsNumberLiteral exNum} to be multiple of {genJsNumberLiteral multOf} (`multipleOf`). {genJsNumberLiteral $ exNum `mod'` multOf}|]

processSchemaData :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> RawOA -> AppM (OA, OAMeta)
processSchemaData cfg mode fileName schemaName x = do
  processMissingTitleWarning cfg mode fileName schemaName x
  processExampleChecks cfg mode fileName schemaName Nothing x
  processSchemaDataInner cfg mode fileName schemaName x

processSchemaDataInner :: Config -> AppOperationMode -> Maybe Text -> Maybe Text -> RawOA -> AppM (OA, OAMeta)
processSchemaDataInner cfg mode fileName schemaName x
  | isSimpleObject x = do
    title' <- extractTitle' x
    let allowedValidators = $(genValidationListTH [])
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let propsMap = x ^. #properties & fromMaybe (M.fromList [])
    let requiredList = x ^. #required & fromMaybe []
    _ <- validateRequiredReferencesExist cfg fileName schemaName propsMap requiredList & liftEitherToAppM
    let transTbl = getTranslationTable cfg
    let propsList = propsMap & M.toList & sortBy (comparing fst)
    forM_ propsList $ \(name, roa) -> processExampleChecks cfg mode fileName schemaName (Just name) roa
    fields :: [FieldOA] <-
      propsList
        <&> propToField transTbl fileName requiredList schemaName
        & sequence
        <&> fmap (applyPropNameMap $ cfg ^. #propertyNameMap)
    let dependencies = fields <&> (^. #typ) <&> depFromTOA & catMaybes & removeTranslatedSchemas transTbl
    let taggedUnionMapping = x ^? #discriminator % _Just % #mapping % _Just <&> fmap pathToSchemaToSchemaName
    let tag = x ^? #discriminator % _Just % #propertyName
    let description = x ^. #description
    let example = genExample x
    let overrideNonRequiredRepresentation = x ^. #xOatsOverrideNonRequiredRepresentation <|> x ^. #xOverrideNonRequiredRepresentation
    when (isJust $ x ^. #xOverrideNonRequiredRepresentation) $ do
      let warnCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName (Just title')
      addWarning $ AppWarning warnCtx $ DeprecationWarning "x-overrideNonRequiredRepresentation is deprecated, please use x-oats-overrideNonRequiredRepresentation. It will be removed in some future version."
    let comment = x ^. #comment
    return
      ( OAClass {fields, extends = Nothing, tag, overrideNonRequiredRepresentation},
        OAMeta {name = title', path = fileName, isRecursive = Nothing, ..}
      )
  | isInheritingObject x = do
    when (x ^. #required & isJust) $
      throwError $
        ValidationError (mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName) "required is not allowed alongside of allOf (move it to the object schema inside allOf)"
    let allowedValidators = $(genValidationListTH [])
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let aof :: [RawOA] = x ^? #allOf % _Just & fromMaybe []
    case (aof ^? ix 0 % #ref, aof ^? ix 1) of
      (Just ref', innerObjMay) -> do
        innerRes <- case innerObjMay of
          Just innerObj -> processSchemaDataInner cfg mode fileName schemaName innerObj
          Nothing -> do
            title' <- extractTitle' x
            let warnCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName (Just title')
            addWarning $ AppWarning warnCtx InheritingObjectWithoutOwnObjectWarning
            let meta :: OAMeta = def & #name .~ title' & #path .~ fileName
            let cls =
                  OAClass
                    { extends = Nothing,
                      fields = [],
                      tag = Nothing,
                      overrideNonRequiredRepresentation = Nothing
                    }
            return (cls, meta)
        let parentSchemaName = ref' <&> pathToSchemaToSchemaName
        let updateExtends = _1 % #extends .~ parentSchemaName
        let updateDeps = _2 % #dependencies %~ (maybeToList parentSchemaName <>)
        let addDescriptionIfPresentOnTop = _2 % #description %~ (\innerDesc -> x ^. #description <|> innerDesc)
        return $ innerRes & updateExtends & updateDeps & addDescriptionIfPresentOnTop
      (ref', innerObj) ->
        throwError $
          ValidationError (mkErrCtxFromFileNameOrDef fileName) $
            "Invalid format of inheriting object: ref' = " <> tshow ref' <> "; innerObj = " <> tshow innerObj
  | isUnion x = do
    let allowedValidators = $(genValidationListTH [])
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let errCtx = mkErrCtxFromFileNameOrDef fileName
    let mkErr = ValidationError errCtx
    oof :: [RawOA] <- x ^. #oneOf & maybeToRight (mkErr "Missing oneOf after isUnion == True") & liftEitherToAppM
    when (null oof) $ throwError (mkErr "Empty union")
    members <- oof <&> rawOAToTypeOA errCtx (getTranslationTable cfg) & sequence <&> fmap (maybeToRight (mkErr "Failed to process union type")) <&> liftEithersToAppM & join
    name <- extractTitle' x
    let tag = x ^. #discriminator <&> (\case DiscriminatorROA {propertyName} -> propertyName)
    let dependencies = members <&> depFromTOA & catMaybes
    let description = x ^. #description
    let comment = x ^. #comment
    return
      ( OAUnion {..},
        OAMeta {path = fileName, taggedUnionMapping = Nothing, example = Nothing, isRecursive = Just False, ..}
      )
  | isEnum x = do
    let allowedValidators = $(genValidationListTH [])
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let mkErr = ValidationError (mkErrCtxFromFileNameOrDef fileName)
    enumItems <- x ^. #enum & maybeToRight (mkErr "Expected enum field") & liftEitherToAppM
    name <- extractTitle' x
    let description = x ^. #description
    let example = genExample x <|> headMay enumItems
    let comment = x ^. #comment
    return
      ( OAEnum {..},
        OAMeta {path = fileName, dependencies = [], taggedUnionMapping = Nothing, isRecursive = Just False, ..}
      )
  | isArray x = do
    let errCtx = mkErrCtxFromFileNameOrDef fileName
    let mkErr = ValidationError errCtx
    name <- extractTitle' x
    itemsType :: TypeOA <-
      ((x ^. #items) :: Maybe RawOA)
        <&> rawOAToTypeOA errCtx (getTranslationTable cfg)
        & sequence
        <&> join
        <&> maybeToRight (mkErr "Failed to get items type")
        <&> liftEitherToAppM
        & join
    let dependencies = itemsType & depFromTOA & maybeToList
    let arrayUniqueItems = x ^. #uniqueItems & fromMaybe False
    let arrayMinItems = x ^. #minItems
    let arrayMaxItems = x ^. #maxItems
    let allowedValidators =
          $( genValidationListTH
               [ "uniqueItems",
                 "minItems",
                 "maxItems",
                 "items"
               ]
           )
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let description = x ^. #description
    let example = genExample x
    let comment = x ^. #comment
    return
      ( OAArray {..},
        OAMeta {path = fileName, taggedUnionMapping = Nothing, isRecursive = Just False, ..}
      )
  | isStringDecimal x = do
    let mkErr = ValidationError (mkErrCtxFromFileNameOrDef fileName)
    name <- extractTitle' x
    let allowedValidators = $(genValidationListTH [])
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    (decimalPrecision, decimalScale) <-
      (x ^. #description) >>= extractDecimalParams & maybeToRight (mkErr "Failed to parse decimal descriptor") & liftEitherToAppM
    let decimalMax = Just $ T.replicate (decimalPrecision - decimalScale) "9" <> "." <> T.replicate decimalScale "9"
    let decimalMin = decimalMax <&> ("-" <>)
    let description = x ^. #description
    let example = genExample x
    let comment = x ^. #comment
    return
      ( OADecimal {..},
        OAMeta {path = fileName, dependencies = [], taggedUnionMapping = Nothing, isRecursive = Just False, ..}
      )
  | isString x = do
    name <- extractTitle' x
    let stringPattern = x ^. #pattern
    let stringMinLength = x ^. #minLength
    let stringTrimmedMinLength = x ^. #xOatsTrimmedMinLength
    let stringMaxLength = x ^. #maxLength
    let allowedValidators =
          $( genValidationListTH
               [ "pattern",
                 "minLength",
                 "maxLength"
               ]
           )
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let description = x ^. #description
    let example = genExample x
    let comment = x ^. #comment
    let errCtx = mkErrCtxFromFileAndSchemaNameOrDef fileName (Just name)
    case stringMinLength of
      Nothing -> return ()
      Just len -> when (len <= 0) $ addWarning $ AppWarning errCtx $ UselessValidationWarning "minimum length of string ≤ 0"
    _ <- case (stringPattern, x ^. #example >>= unpackAesonString) of
      (Just patty, Just exampleStr) ->
        unless (cfg ^. #noPatternValidationWithExample) $
          validateStringPatternAndExample patty exampleStr
            <&> mapLeft (ValidationError errCtx)
              & liftEitherInIOToAppM
      (Just _patty, Nothing) -> addWarning $ AppWarning errCtx MissingExampleOfStringWithPatternWarning
      (_, _) -> return ()
    let path = fileName
    let taggedUnionMapping = Nothing
    let isRecursive = Just False
    if isStringWithFormat x
      then do
        aliasTarget <-
          x ^. #format <&> stringFormatStringToSchemaName
            & join
            & maybeToEither ImpossibleError
            & liftEitherToAppM
        return (OAAlias {aliasTarget}, OAMeta {dependencies = [aliasTarget], ..})
      else return (OAString {..}, OAMeta {dependencies = [], ..})
  | isInteger x = do
    name <- extractTitle' x
    let integerMinimum = x ^. #minimum <&> round
    let integerMaximum = x ^. #maximum <&> round
    _ <- validateDisallowedValidators cfg fileName schemaName x $(genValidationListTH ["minimum", "maximum"])
    let description = x ^. #description
    let example = genExample x
    let comment = x ^. #comment
    return
      ( OAInteger {..},
        OAMeta {path = fileName, dependencies = [], taggedUnionMapping = Nothing, isRecursive = Just False, ..}
      )
  | isNumber x = do
    name <- extractTitle' x
    let numberMinimum = x ^. #minimum
    let numberMaximum = x ^. #maximum
    let numberExclusiveMinimum = x ^. #exclusiveMinimum & fromMaybe False
    let numberExclusiveMaximum = x ^. #exclusiveMaximum & fromMaybe False
    let numberMultipleOf = x ^. #multipleOf
    let numberDecimalPrecision = x ^. #multipleOf <&> extractDecimalPlacesFromMultipleOf
    let allowedValidators =
          $( genValidationListTH
               [ "minimum",
                 "maximum",
                 "exclusiveMinimum",
                 "exclusiveMaximum",
                 "multipleOf"
               ]
           )
    _ <- validateDisallowedValidators cfg fileName schemaName x allowedValidators
    let description = x ^. #description
    let example = genExample x
    let comment = x ^. #comment
    return
      ( OANumber {..},
        OAMeta {path = fileName, dependencies = [], taggedUnionMapping = Nothing, isRecursive = Just False, ..}
      )
  | otherwise = do
    let errCtx = mkErrCtxFromFileNameOrDef fileName
    title' <- extractTitle' x
    let msg = [i|Failed to identify input type (typ = #{x ^. #typ}). title = #{title'}, fileName = #{fileName}. #{pShow x}|]
    throwError $ ValidationError errCtx msg
  where
    extractTitle' :: RawOA -> AppM Text
    extractTitle' = extractTitle cfg mode fileName schemaName

type HighLevelSchemasProcessor = WorkContext -> [(OA, OAMeta)] -> OA -> OAMeta -> AppM (OA, OAMeta)

processTaggedUnionMapping :: HighLevelSchemasProcessor
processTaggedUnionMapping ctx db x@OAClass {} meta = x ^?? #extends & maybe pass onPosExtends
  where
    pass = return (x, meta)
    onPosExtends exts = do
      -- TODO: refactor inheriting from union member
      case find (\(_oa, m) -> m ^. #name == exts) db of
        Nothing -> return ()
        Just (parentOa, parentMeta) ->
          case parentOa ^? #extends % _Just of
            Nothing -> return ()
            Just parentExts -> do
              parentTag <- getTagInfo ctx db parentExts (parentMeta ^. #name)
              case parentTag of
                Nothing -> return ()
                Just (_pTagName, _pTagVal) ->
                  throwError $
                    ValidationError
                      (mkErrCtxFromMeta meta)
                      ( let metaName = meta ^. #name; parentName = parentMeta ^. #name
                         in [i|Detected possible LSP violation - #{metaName & tshow} inherits from a union member #{parentName & tshow}. Extending union members is not allowed.|]
                      )
      tagMay <- getTagInfo ctx db exts (meta ^. #name)
      tagMay & maybe pass onPosTag
    onPosTag (tagName, tagVal) = do
      let tagFieldAlreadyExists = x ^? #fields & fromMaybe [] & any (\y -> y ^. #name == tagName)
      let tagFieldExistsErr = ValidationError (mkErrCtxFromMeta meta) [i|Cannot add tag field "{tagName}" into #{meta ^. #name}, field already exists: $x|]
      _ <- leftOnTrue tagFieldExistsErr tagFieldAlreadyExists & liftEitherToAppM
      let newField =
            FieldOA {name = tagName, typ = OneValueEnumTOA tagVal, required = RequiredOA, description = Nothing}
      let newX = x & #fields %~ cons newField
      return (newX, meta)
processTaggedUnionMapping _ _ x meta = return (x, meta)

getTagInfo :: WorkContext -> [(OA, OAMeta)] -> Text -> Text -> AppM (Maybe (Text, Text))
getTagInfo ctx db parentName childName = case (parentMay, parentsTableMay) of
  (Just parent, Just parentsTable) -> do
    let errCtx = mkErrCtxFromMetaOrDef parentMetaMay
    tagValue <- parentsTable & hashMapLookupValue childName & maybeToEither (ValidationError errCtx [qq|No record in tagged union mapping table of {parentName} for {childName}|]) & liftEitherToAppM
    tagName <- parent ^?? #tag & maybeToEither (ValidationError errCtx [qq|Missing tag (propertyName) in {parentName}, parent of {childName}|]) & liftEitherToAppM
    return $ Just (tagName, tagValue)
  (_, _) -> return Nothing
  where
    parentMetaMay = ctx ^. #metas & find (\y -> y ^. #name == parentName)
    parentsTableMay = parentMetaMay <&> (^. #taggedUnionMapping) & join
    parentMay = db & find (\(_, metaY) -> metaY ^. #name == parentName) <&> fst

getDependentSchemas :: [(OA, OAMeta)] -> (OA, OAMeta) -> [(OA, OAMeta)]
getDependentSchemas xs (_a, am) = xs & filter (\(_b, bm) -> (am ^. #name) `elem` (bm ^. #dependencies))

calcRecursiveSchemas :: [(OA, OAMeta)] -> [Text]
calcRecursiveSchemas xs = go xs [] <&> getName
  where
    getName = (^. _2 % #name)
    go ys prev =
      if srt ys == srt prev
        then ys
        else
          let newData = ys & filter (not . noDependenciesOrNoDependants ys) & removeMissingSchemasFromDependencies
           in go newData ys
    noDependenciesOrNoDependants ys (ao, am) = null (getDependentSchemas ys (ao, am)) || null (am ^. #dependencies)
    removeMissingSchemasFromDependencies :: [(OA, OAMeta)] -> [(OA, OAMeta)]
    removeMissingSchemasFromDependencies ys = ys & each %~ removeMissingSchemasFromDependenciesOfOne ys
    removeMissingSchemasFromDependenciesOfOne :: [(OA, OAMeta)] -> (OA, OAMeta) -> (OA, OAMeta)
    removeMissingSchemasFromDependenciesOfOne db (ao, am) = (ao, am & #dependencies %~ filter (isDependencyInDb db))
    isDependencyInDb db depName = any (\(_bo, bm) -> bm ^. #name == depName) db
    srt = sortOn getName

processRecursiveSchemas :: HighLevelSchemasProcessor
processRecursiveSchemas _ctx db x@OAClass {} meta = return (x, meta & #isRecursive ?~ isRec)
  where
    recNames = calcRecursiveSchemas db
    isRec :: Bool = (meta ^. #name) `elem` recNames
processRecursiveSchemas _ _ x meta = return (x, meta)

type HighLevelRoutesProcessor = WorkContext -> Tags -> OARoutes -> URI -> OARoute -> AppM (URI, OARoute)

highLevelSchemaProcessors :: [HighLevelSchemasProcessor]
highLevelSchemaProcessors = [processTaggedUnionMapping, processRecursiveSchemas]

type EndpointOpIdTagTuple = (OAEndpoint, Text, Text)

processOperationIdTagPairUniqueness :: HighLevelRoutesProcessor
processOperationIdTagPairUniqueness ctx _tags db url x = do
  let curOpIdTagPairs :: [(Text, EndpointOpIdTagTuple)] = (url, opIdTagPairsFromRoute x) & liftUrlAndEndpointOpIdTagTuple
  let otherRoutes = db & M.toList & filter (\(u, _) -> u /= url)
  let otherOpIdTagPairs :: [(Text, EndpointOpIdTagTuple)] = otherRoutes <&> second opIdTagPairsFromRoute <&> liftUrlAndEndpointOpIdTagTuple & join
  let cmpFunc = case ctx ^. #config % #endpointsGenerationMode of
        Just EndpointsWithSameTagInOneFile -> tripleEq23
        Just AllEndpointsInOneFile -> tripleEq2
        Nothing -> \_ _ -> False
  let colliding :: [(Text, EndpointOpIdTagTuple)] = otherOpIdTagPairs & filter (\(_, x) -> any (cmpFunc x) (curOpIdTagPairs ^.. each % _2))
  unless (null colliding) $ do
    let errList = curOpIdTagPairs <> colliding <&> (\(u, (ep, op, tg)) -> (u, ep ^. #requestType, op, tg)) & pShow
    ValidationError def [qq|Found duplicate operation ids (or combination of operation id and tag): $errList|] & Left & liftEitherToAppM
  return (url, x)
  where
    opIdTagFromPath :: OAEndpoint -> EndpointOpIdTagTuple
    opIdTagFromPath e = (e, e ^. #operationId, e ^. #tag)
    opIdTagPairsFromRoute :: OARoute -> [EndpointOpIdTagTuple]
    opIdTagPairsFromRoute r = M.toList r & each %~ (view _2 >>> opIdTagFromPath)
    liftUrlAndEndpointOpIdTagTuple :: (Text, [EndpointOpIdTagTuple]) -> [(Text, EndpointOpIdTagTuple)]
    liftUrlAndEndpointOpIdTagTuple (u, xs) = xs <&> (u,)
    tripleEq23 :: (Eq b, Eq c) => (a, b, c) -> (d, b, c) -> Bool
    tripleEq23 (_, a, b) (_, a', b') = a == a' && b == b'
    tripleEq2 :: (Eq b) => (a, b, c) -> (d, b, e) -> Bool
    tripleEq2 (_, b, _) (_, b', _) = b == b'

processTagValidity :: HighLevelRoutesProcessor
processTagValidity ctx tags _db url route = do
  when (ctx ^. #mode == SingleFileMode) $ do
    forM_ (M.elems route) $ \(x :: OAEndpoint) -> do
      let found :: Bool = (x ^. #tag) `elem` (tags ^.. each % #name)
      unless found $ do
        -- TODO: file name in error context
        let errCtx = def & #endpointOperationId ?~ x ^. #operationId
        throwError $ ValidationError errCtx ("Invalid tag" <> x ^. #tag <> ".")
  return (url, route)

highLevelRouteProcessors :: [HighLevelRoutesProcessor]
highLevelRouteProcessors = [processOperationIdTagPairUniqueness, processTagValidity]

type HighLevelDataInOut = (WorkContext, [(OA, OAMeta)], OARoutes, Tags)

processHighLevelData :: HighLevelDataInOut -> AppM HighLevelDataInOut
processHighLevelData (ctx, schemas, routes, tags) = do
  resSchemas <- foldM (applySchemaProcessor ctx) schemas highLevelSchemaProcessors
  resRoutes <- foldM (applyRouteProcessor ctx tags) routes highLevelRouteProcessors
  return (ctx, resSchemas, resRoutes, tags)
  where
    applySchemaProcessor :: WorkContext -> [(OA, OAMeta)] -> HighLevelSchemasProcessor -> AppM [(OA, OAMeta)]
    applySchemaProcessor curCtx curDb f = mapM (uncurry (f curCtx curDb)) curDb
    applyRouteProcessor :: WorkContext -> Tags -> OARoutes -> HighLevelRoutesProcessor -> AppM OARoutes
    applyRouteProcessor curCtx curTags curDb f = M.toList curDb & mapM (uncurry (f curCtx curTags curDb)) <&> M.fromList

rawParamToParam :: Config -> Maybe Text -> Text -> Text -> RequestType -> RawOAParameter -> AppM OAParameter
rawParamToParam cfg fileName opId url rt x@RawOAParameter {..} = do
  let errCtx = mkErrCtxForEndpoints fileName opId url Nothing rt
  let mkErr (msg :: Text) = ValidationError errCtx msg
  schema <-
    x ^. #schema
      & rawOAToTypeOA errCtx (getTranslationTable cfg)
      <&> maybeToRight (mkErr [i|Failed to get type of schema in parameter #{x ^. #name & tshow} (possibly unsupported schema type)|])
      <&> liftEitherToAppM
      & join
  return $ OAParameter {required = x ^. #required == Just True, ..}

mimeAppJson :: Text
mimeAppJson = "application/json"

rawRequestOrResponseToResponse :: Config -> Maybe Text -> CommunicationDirection -> Text -> RequestType -> RawOAPath -> RawOARequestOrResponse -> AppM OARequestOrResponse
rawRequestOrResponseToResponse cfg fileName dir url reqType rawPath x = do
  mimeSchemaPairMay :: Maybe (Text, RawOA) <- extractMimeSchemaPairMay x
  let (mime, rawSchema) = mimeSchemaPairMay & unzipMaybe
  schema <-
    rawSchema
      <&> rawOAToTypeOA errCtx (getTranslationTable cfg)
      & sequence
      <&> join
  when (mime == Just mimeAppJson) $
    case rawSchema of
      Nothing -> throwError $ ValidationError errCtx [qq|Invalid schema of JSON {comDirToText dir}.|]
      Just rs -> unless (isRef rs || isArray rs) $ do
        addNonCriticalError $ NonCriticalError errCtx $ PrimitiveTypeInRequestOrResponseBodyError dir x
  return OARequestOrResponse {description = x ^. #description, ..}
  where
    errCtx = mkErrCtxForEndpoints fileName (rawPath ^. #operationId) url (Just dir) reqType
    extractMimeSchemaPairMay :: RawOARequestOrResponse -> AppM (Maybe (Text, RawOA))
    extractMimeSchemaPairMay rr =
      case rr ^. #content of
        Nothing -> return Nothing
        Just c -> do
          when (M.size c /= 1) $ throwError $ ValidationError errCtx [qq|Expected "content" to have exactly one item, but a {comDirToText dir} has {M.size c}.$nl{pShow x}.|]
          let itemPair = c & M.toList & headMay & fromJustNote [qq|Logic error: (M.size c) == 1, but there is no item.$nl$url$nl{show c}|]
          return $ Just $ itemPair & (_2 %~ view #schema)

rawPathToEndpoint :: Config -> Maybe Text -> Text -> (RequestType, RawOAPath) -> AppM (RequestType, OAEndpoint)
rawPathToEndpoint cfg fileName url (reqType, x) = do
  let errCtx = mkErrCtxForEndpoints fileName (x ^. #operationId) url Nothing reqType
  let missingTagErr = ValidationError errCtx "Path is missing a tag."
  tag <- x ^? #tags % _head & maybeToEither missingTagErr & liftEitherToAppM
  let rawParameters = x ^. #parameters & fromMaybe []
  forM_ rawParameters $ \p -> do
    when (p ^. #inn == PathParameterInType && p ^. #required /= Just True) $
      throwError $ ValidationError errCtx [i|Parameter #{p ^. #name & tshow} has invalid/missing value in "required" field.|]
    when (p ^. #inn == CookieParameterInType) $ addWarning $ AppWarning errCtx CookieParameterWarning
  parameters <- rawParameters & mapM (rawParamToParam cfg fileName (x ^. #operationId) url reqType)
  let duplicateParameters =
        parameters
          & groupBy ((==) `on` (\y -> (y ^. #inn, y ^. #name)))
          & filter (length >>> (> 1))
          <&> headMay
          & catMaybes
          <&> (^. #name)
  unless (null duplicateParameters) $
    let fmtParams = T.intercalate ", " duplicateParameters
     in throwError $ ValidationError (errCtx & #endpointRequestType ?~ reqType) [qq|Duplicate parameters: $fmtParams.|]
  let processResponse (code, y) = rawRequestOrResponseToResponse cfg fileName ResponseDirection url reqType x y <&> (code,)
  responses <- x ^. #responses & M.toList & mapM processResponse <&> M.fromList
  forM_ (x ^. #responses & M.toList) $ \(code, resp) ->
    when (isJust (resp ^. #ref) && isHttpSuccessCode code) $ do
      let errCtx = mkErrCtxForEndpoints fileName (x ^. #operationId) url (Just ResponseDirection) reqType
      addWarning $ AppWarning errCtx UnsupportedResponseRefWarning
  request <- case x ^. #requestBody of
    Just y -> do
      r <- rawRequestOrResponseToResponse cfg fileName RequestDirection url reqType x y
      return $ Just r
    Nothing -> return Nothing
  let permissions = x ^.. #security % _Just % traversed % #permissions % _Just & join
  let meta = x ^. #xOatsMeta <|> x ^. #xMeta
  when (isJust $ x ^. #xMeta) $ do
    addWarning $ AppWarning errCtx $ DeprecationWarning "x-meta is deprecated, please use x-oats-meta. It will be removed in some future version."
  let res =
        OAEndpoint
          { operationId = x ^. #operationId,
            summary = x ^. #summary,
            description = x ^. #description,
            requestType = reqType,
            ..
          }
  return (reqType, res)

rawPathsToRoute :: Config -> Maybe Text -> (Text, RawOAPaths) -> AppM (Text, OARoute)
rawPathsToRoute cfg fileName (url, raw) = do
  res <- raw & M.toList & mapM (rawPathToEndpoint cfg fileName url) <&> M.fromList
  return (url, res)

rawTagToTag :: RawOATag -> AppM OATag
rawTagToTag x = return $ OATag {name = x ^. #name, description = x ^. #description}
