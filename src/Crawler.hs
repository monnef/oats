{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}

module Crawler
  ( crawl,
    findFilesWithExtension,
  )
where

import Control.Monad (unless)
import Control.Monad.IO.Class (MonadIO, liftIO)
import Control.Monad.Except (throwError)
import Data
import Data.List (partition)
import Optics
import System.Directory.Extra (doesDirectoryExist)
import System.FilePath (makeRelative)
import qualified System.FilePath.Find as Find
import Text.RE.PCRE.Text (matched, (?=~))
import Utils

-- | Gets all files' paths relative to given 'path'.
findFilesWithExtension :: (MonadIO m) => Text -> FilePath -> m [FilePath]
findFilesWithExtension ext path =
  Find.find Find.always (Find.extension Find.==? toS ext) path <&> fmap (makeRelative path) & liftIO

crawl :: WorkContext -> AppM CrawlResult
crawl ctx = do
  let apiPathMay = ctx ^. #config % #apiPath <&> toS
  fileNames <- case apiPathMay of
    Just ap -> do
      apExists <- liftIO $ doesDirectoryExist ap
      unless apExists $ throwError (CrawlerError [qq|"{ap}" doesn't exist|])
      tryIO (CrawlerError "Crawling failed") (findFilesWithExtension ".yaml" ap)
    Nothing -> return []
  let filterByIgnoreRegex =
        case ctx ^. #config % #ignoreFilesRegex of
          Just x -> toS >>> (?=~ x) >>> matched >>> not
          Nothing -> const True
  let (sourceFiles, ignoredFilesByRegex) = fileNames & partition filterByIgnoreRegex
  liftIO $
    ctx ^. #debugLog $
      "skipped because of ignoreFilesRegex:"
        <> if null ignoredFilesByRegex
          then " nothing"
          else "\n" <> indent 2 (pShow ignoredFilesByRegex)
  let customSchemasPathMay = ctx ^. #config % #customSchemasPath <&> toS
  customSchemaFiles <- case customSchemasPathMay of
    Just csp -> do
      apExists <- liftIO $ doesDirectoryExist csp
      unless apExists $ throwError (CrawlerError [qq|"{csp}" doesn't exist|])
      tryIO (CrawlerError "Crawling failed") (findFilesWithExtension ".ts" csp)
    Nothing -> return []
  return $ CrawlResult {..}
