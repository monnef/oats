{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}

module BuildInfoTH where

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import Data.Time.Clock (getCurrentTime)
import Data.Time.Format.ISO8601 (iso8601Show)
import Language.Haskell.TH
import System.Exit (ExitCode (..))
import System.Process (readCreateProcessWithExitCode, shell)
import Utils

data BuildInfo = BuildInfo
  { biCommitHash :: Text,
    biTag :: Text,
    biDirty :: Bool,
    biTagAndHash :: Text,
    biCommitDate :: Text,
    biBranch :: Text,
    biBuildDate :: Text
  }
  deriving (Show)

debugTH :: Bool
debugTH = False

debugLog :: Text -> IO ()
debugLog x = when debugTH $ TIO.putStrLn x

runCommand :: Text -> IO (Either Text Text)
runCommand cmd = do
  debugLog $ "runGitCommand - start. '" <> cmd <> "'"
  let sCmd = T.unpack cmd
  (code, stdOut, stdErr) <- readCreateProcessWithExitCode (shell sCmd) ""
  debugLog $ "runGitCommand - got results. " <> tshow (code, stdOut, stdErr)
  case code of
    ExitSuccess -> return $ Right $ T.stripEnd $ T.pack $ stdOut
    ExitFailure c ->
      return $
        Left $
          mconcat
            [ "\nFailed to run '",
              sCmd,
              "' - it returned ",
              show c,
              ".\nstdOut:\n",
              stdOut,
              "\nstdErr:\n",
              stdErr
            ]
            & T.pack

orFail :: Either Text a -> IO a
orFail (Left x) = fail $ T.unpack x
orFail (Right x) = return x

orDefaultWithWarning :: a -> Either Text a -> Q a
orDefaultWithWarning _ (Right x) = return x
orDefaultWithWarning d (Left x) = reportWarning (toS x) $> d

getGitCommit :: Q Text
getGitCommit = runCommand "git rev-parse HEAD" & runIO >>= orDefaultWithWarning "<unknown commit>"

getGitTag :: Q Text
getGitTag =
  runCommand "git name-rev --tags --name-only $(git rev-parse HEAD)"
    & runIO
    <&> handleUndefinedTag
    >>= orDefaultWithWarning unknownTagStr
  where
    handleUndefinedTag :: Either Text Text -> Either Text Text
    handleUndefinedTag (Right "undefined") = Left unknownTagStr
    handleUndefinedTag x = x
    unknownTagStr = "<unknown tag>"

getGitDirty :: Q Bool
getGitDirty = runCommand "git diff --stat" & runIO <&> fmap mapOutput >>= orDefaultWithWarning True
  where
    mapOutput :: Text -> Bool
    mapOutput = (/= "")

getGitTagAndHash :: Q Text
getGitTagAndHash = runCommand "git describe --always --tags" & runIO >>= orDefaultWithWarning "<unknown tag+hash>"

getGitCommitDate :: Q Text
getGitCommitDate = runCommand "git log HEAD -1 --format=%cd" & runIO >>= orDefaultWithWarning "<unknown commit date>"

getGitBranch :: Q Text
getGitBranch = runCommand "git rev-parse --abbrev-ref HEAD" & runIO >>= orDefaultWithWarning "<unknown branch>"

getBuildDate :: Q Text
getBuildDate = runIO $ do
  t <- getCurrentTime
  return $ toS $ iso8601Show t

getBuildInfoTH :: Q Exp
getBuildInfoTH = do
  commit <- getGitCommit
  tag <- getGitTag
  dirty <- getGitDirty
  gitTagAndHash <- getGitTagAndHash
  commitDate <- getGitCommitDate
  branch <- getGitBranch
  bTime <- getBuildDate
  [|BuildInfo {biCommitHash = commit, biTag = tag, biDirty = dirty, biTagAndHash = gitTagAndHash, biCommitDate = commitDate, biBranch = branch, biBuildDate = bTime}|]
