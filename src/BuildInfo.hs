{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE OverloadedStrings #-}

module BuildInfo where

import qualified Data.Text as T
import Data.Version (showVersion)
import Paths_oats
import Utils
import BuildInfoTH

versionText :: Text
versionText = showVersion version & toS

bi :: BuildInfo
bi = $(getBuildInfoTH)

extendedBuildInfo :: Text
extendedBuildInfo = T.strip [qq|{buildDateTimeText} from {gitBranchAndLongHash} ({biCommitDate bi}) {gitTagText} {gitDirtyText}|]

gitBranchAndLongHash :: Text
gitBranchAndLongHash = toS $ biBranch bi <> "@" <> biCommitHash bi

gitShortHash :: Text
gitShortHash = T.take 8 (biCommitHash bi)

gitBranchAndShortHash :: Text
gitBranchAndShortHash = biBranch bi <> "@" <> gitShortHash

isGitDirty :: Bool
isGitDirty = biDirty bi

gitDirtyText :: Text
gitDirtyText = bool "" "[DIRTY]" isGitDirty

gitTagText :: Text
gitTagText = [qq|<{biTagAndHash bi}>|]

buildDateTimeText :: Text
buildDateTimeText = biBuildDate bi
