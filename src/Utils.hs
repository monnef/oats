{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}
{-# LANGUAGE TypeFamilies #-}

module Utils
  ( tshow,
    tPutStr,
    tPutStrLn,
    tPutStrLnLn,
    tErrPutStr,
    tErrPutStrLn,
    tReadFile,
    tReadFileEither,
    tWriteFile,
    flushStdOut,
    flushStdErr,
    flushStds,
    (>>>),
    (&),
    (<&>),
    ($>),
    (<$),
    (??),
    (^?<>),
    (^??),
    Text,
    bool,
    orCrash,
    skip,
    toS,
    forM,
    forM_,
    void,
    qq,
    nl,
    pShow,
    firstErrorFromEithers,
    stripSuffixIfPresent,
    capitalize,
    deCapitalize,
    liftToText,
    indent,
    backSlashToSlash,
    hashMapLookupValue,
    leftOnTrue,
    emptyTextAsNothing,
    lift,
    fmtFloat,
    fmtFloat2,
    renderTemplate,
    textToMaybe,
    justFalseToNothing,
    lookupPair,
    zipMaybe,
    unzipMaybe,
    eq,
    surround,
    isHttpSuccessCode,
    isAesonValueString,
    isAesonValueNumber,
    isAesonValueBoolean,
    unpackAesonString,
    unpackAesonNumberAsDouble,
    filterOut,
    genJsStringLiteral,
    genJsNumberLiteral,
    runCmd,
    when,
    maybeEitherToEitherMaybe,
    sequencePair,
    singleListToJust,
    joinNl,
    numberOfDigits,
    isMultipleOf,
    currentDateTimeStr,
    i,
    none,
    join,
  )
where

-- import qualified Data.ByteString.Lazy as BL
-- import qualified Data.ByteString.Lazy.Char8 as CL

-- import qualified Data.Text.Lazy as TL
-- import qualified Data.Text.Lazy.Encoding as TLE

import Control.Arrow ((>>>))
import Control.Exception (SomeException, try)
import Control.Monad (forM, forM_, join, void, when)
import Control.Monad.Trans (lift)
import qualified Data.Aeson as AES
import Data.Bool (bool)
import qualified Data.Decimal as Dec
import Data.Either.Extra (mapLeft)
import Data.Fixed (mod')
import Data.Functor (($>))
import qualified Data.HashMap.Lazy as M
import Data.Hashable (Hashable)
import Data.List (find)
import Data.Maybe (fromMaybe, maybeToList)
import Data.Scientific (toRealFloat)
import Data.String.Conv (toS)
import Data.String.Interpolate (i)
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time (getCurrentTime)
import Data.Time.Format.ISO8601 (iso8601Show)
import Numeric (showFFloat)
import Optics
import System.Exit (ExitCode)
import System.IO (hFlush, hPutStr, hPutStrLn, hSetNewlineMode, noNewlineTranslation, stderr, stdout)
import System.IO.Extra (IOMode (..), hSetEncoding, utf8_bom, withFile)
import System.IO.Strict (hGetContents)
import System.Process (StdStream (CreatePipe), createProcess, cwd, proc, std_err, std_out, waitForProcess)
import Text.InterpolatedString.Perl6 (qq)
import qualified Text.Pretty.Simple as PrettySimple
import UtilsTH (capitalizeWordStr, deCapitalizeWordStr)
import TemplateUtils (renderTemplate)

tshow :: Show a => a -> Text
tshow = show >>> T.pack

tPutStr :: Text -> IO ()
tPutStr = T.unpack >>> putStr

tPutStrLn :: Text -> IO ()
tPutStrLn = T.unpack >>> putStrLn

tPutStrLnLn :: Text -> IO ()
tPutStrLnLn x = x <> "\n" & tPutStrLn

tErrPutStr :: Text -> IO ()
tErrPutStr = toS >>> hPutStr stderr

tErrPutStrLn :: Text -> IO ()
tErrPutStrLn = toS >>> hPutStrLn stderr

flushStdOut :: IO ()
flushStdOut = hFlush stdout

flushStdErr :: IO ()
flushStdErr = hFlush stderr

flushStds :: IO ()
flushStds = flushStdOut >> flushStdErr

-- | Strict file read in UTF8 encoding.
tReadFile :: Text -> IO Text
tReadFile fileName = do
  withFile (toS fileName) ReadMode $
    \h -> do
      hSetEncoding h utf8_bom
      contents <- System.IO.Strict.hGetContents h
      return $ toS contents

tReadFileEither :: Text -> IO (Either Text Text)
tReadFileEither fileName = do
  c :: Either SomeException Text <- try $ tReadFile fileName
  return $ mapLeft tshow c

tWriteFile :: Text -> Text -> IO ()
tWriteFile fileName contents =
  withFile (toS fileName) WriteMode $ \h -> do
    hSetNewlineMode h noNewlineTranslation
    hPutStr h (toS contents)

orCrash :: Show e => Either e a -> a
orCrash (Right x) = x
orCrash (Left e) = error $ show e

skip :: Monad m => m ()
skip = return ()

nl :: Text
nl = "\n"

pShow :: Show a => a -> Text
pShow = PrettySimple.pShowOpt opts >>> toS
  where
    opts = PrettySimple.defaultOutputOptionsDarkBg {PrettySimple.outputOptionsIndentAmount = 2}

firstErrorFromEithers :: [Either l r] -> Either l [r]
firstErrorFromEithers [] = Right []
firstErrorFromEithers ((Left e) : _) = Left e
firstErrorFromEithers ((Right r) : xs) = firstErrorFromEithers xs <&> (r :)

stripSuffixIfPresent :: Text -> Text -> Text
stripSuffixIfPresent s x = x & T.stripSuffix s & fromMaybe x

capitalize :: Text -> Text
capitalize = toS >>> capitalizeWordStr >>> toS

deCapitalize :: Text -> Text
deCapitalize = toS >>> deCapitalizeWordStr >>> toS

liftToText :: (String -> String) -> (Text -> Text)
liftToText f = toS >>> f >>> toS

indent :: Int -> Text -> Text
indent n x = x & T.lines <&> (T.replicate n " " <>) & T.unlines

backSlashToSlash :: Text -> Text
backSlashToSlash = T.replace "\\" "/"

hashMapLookupValue :: Eq v => v -> M.HashMap k v -> Maybe k
hashMapLookupValue x m = m & M.toList & find (\(_, v) -> v == x) <&> fst

leftOnTrue :: l -> Bool -> Either l ()
leftOnTrue _ False = skip
leftOnTrue l True = Left l

emptyTextAsNothing :: Text -> Maybe Text
emptyTextAsNothing "" = Nothing
emptyTextAsNothing x = Just x

fmtFloat :: RealFloat a => a -> Text
fmtFloat x = showFFloat Nothing x "" & toS

fmtFloat2 :: RealFloat a => a -> Text
fmtFloat2 x = showFFloat (Just 2) x "" & toS


textToMaybe :: Text -> Maybe Text
textToMaybe "" = Nothing
textToMaybe x = Just x

justFalseToNothing :: Maybe Bool -> Maybe Bool
justFalseToNothing (Just False) = Nothing
justFalseToNothing x = x

lookupPair :: (Hashable k) => k -> M.HashMap k v -> Maybe (k, v)
lookupPair k x = M.lookup k x <&> (k,)

unzipMaybe :: Maybe (a, b) -> (Maybe a, Maybe b)
unzipMaybe Nothing = (Nothing, Nothing)
unzipMaybe (Just (x, y)) = (Just x, Just y)

zipMaybe :: Maybe a -> Maybe b -> Maybe (a, b)
zipMaybe (Just x) (Just y) = Just (x, y)
zipMaybe _ _ = Nothing

eq :: Eq a => a -> a -> Bool
eq = (==)

surround :: Text -> Text -> Text -> Text
surround l r x = l <> x <> r

isHttpSuccessCode :: Int -> Bool
isHttpSuccessCode x = x >= 200 && x < 300

isAesonValueString :: AES.Value -> Bool
isAesonValueString (AES.String _) = True
isAesonValueString _ = False

isAesonValueNumber :: AES.Value -> Bool
isAesonValueNumber (AES.Number _) = True
isAesonValueNumber _ = False

isAesonValueBoolean :: AES.Value -> Bool
isAesonValueBoolean (AES.Bool _) = True
isAesonValueBoolean _ = False

unpackAesonString :: AES.Value -> Maybe Text
unpackAesonString (AES.String x) = Just $ toS x
unpackAesonString _ = Nothing

unpackAesonNumberAsDouble :: AES.Value -> Maybe Double
unpackAesonNumberAsDouble (AES.Number x) = Just $ toRealFloat x
unpackAesonNumberAsDouble _ = Nothing

filterOut :: (a -> Bool) -> [a] -> [a]
filterOut f = filter (f >>> not)

genJsStringLiteral :: Text -> Text
genJsStringLiteral x = AES.encode x & toS

genJsNumberLiteral :: Double -> Text
genJsNumberLiteral x = showFFloat Nothing x "" & toS

runCmd :: Text -> [Text] -> Bool -> Text -> IO ([Text], [Text], ExitCode)
runCmd cmd args printOutput targetDir = do
  (_, Just hout, Just herr, jHandle) <-
    createProcess
      (proc (toS cmd) (args <&> toS))
        { cwd = Just (toS targetDir),
          std_out = CreatePipe,
          std_err = CreatePipe
        }

  --  when printOutput $ hSetEcho hout True
  --  hSetBuffering hout NoBuffering
  out <- hGetContents hout
  when printOutput $ putStr out

  err <- hGetContents herr
  when printOutput $ putStr err

  exitCode <- waitForProcess jHandle
  return (lines out <&> toS, lines err <&> toS, exitCode)

maybeEitherToEitherMaybe :: Maybe (Either l r) -> Either l (Maybe r)
maybeEitherToEitherMaybe Nothing = Right Nothing
maybeEitherToEitherMaybe (Just (Right r)) = Right (Just r)
maybeEitherToEitherMaybe (Just (Left l)) = Left l

sequencePair :: Monad m => (m a, m b) -> m (a, b)
sequencePair (mx, my) = do
  x <- mx
  y <- my
  return (x, y)

singleListToJust :: [a] -> Maybe a
singleListToJust [x] = Just x
singleListToJust _ = Nothing

joinNl :: [Text] -> Text
joinNl = T.intercalate "\n"

-- | Number of digits in integer without sign
numberOfDigits :: Int -> Int
numberOfDigits = abs >>> show >>> length

isMultipleOf :: Double -> Double -> Bool
isMultipleOf val mult =
  toDec val `mod'` toDec mult < epsilon
  where
    decPrecision = 15
    epsilon = 0.000000001 :: Dec.Decimal
    toDec = Dec.realFracToDecimal decPrecision

currentDateTimeStr :: IO Text
currentDateTimeStr = getCurrentTime <&> iso8601Show <&> toS

-- | Get list on prism (empty list when non-fitting type)
(^?<>) :: Is k An_AffineFold => s -> Optic' k is s [a] -> [a]
x ^?<> l = x ^? l & maybeToList & join

-- | Get maybe on prism (nothing when non-fitting type)
(^??) :: Is k An_AffineFold => s -> Optic' k is s (Maybe a) -> Maybe a
x ^?? l = x ^? l & join

-- | From lens
(??) :: Functor f => f (a -> b) -> a -> f b
fab ?? a = fmap ($ a) fab
{-# INLINE (??) #-}

none :: Foldable f => (a -> Bool) -> f a -> Bool
none f = any f >>> not
