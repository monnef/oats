{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module TemplateUtils where

import Control.Arrow ((>>>))
import Control.Monad.Identity (Identity, runIdentity)
import Control.Monad.Writer (Writer)
import Data.Aeson (ToJSON, toJSON)
import Data.Either.Extra (mapLeft)
import Data.Function ((&))
import Data.Functor ((<&>))
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromJust)
import Data.String.Conv (toS)
import Data.String.Interpolate (i)
import Data.Text (Text)
import qualified Data.Text as T
import Text.Casing (fromAny, toCamel, toKebab, toPascal, toQuietSnake, toScreamingSnake)
import Text.Ginger (GVal, IncludeResolver, Run, SourcePos, Template, easyRender, parseGinger, rawJSONToGVal)
import Text.Ginger.GVal (Function, asHashMap, asText, fromFunction, toGVal)

type MyGinger = Run SourcePos (Writer Text) Text

mkTemplateFunction :: Text -> (Text -> Text) -> (Monad m => Function m)
mkTemplateFunction fName f args = return $ case args of
  [(_, gValText)] -> gValText & asText & f & toGVal
  _ -> toGVal ([i|<Error: wrong number of arguments given to template function #{fName}.>|] :: Text) -- TODO: improve error handling

caseFunctions :: [(Text, Function MyGinger)]
caseFunctions = rawCaseFunctionsWithNames <&> (\(fName, f) -> (fName, toFunction fName f))
  where
    apCaseTransform transform = toS >>> fromAny >>> transform >>> toS
    toFunction fName transform = mkTemplateFunction fName (apCaseTransform transform)
    rawCaseFunctionsWithNames =
      [ ("toPascal", toPascal),
        ("toCamel", toCamel),
        ("toScreamingSnake", toScreamingSnake),
        ("toQuietSnake", toQuietSnake),
        ("toKebab", toKebab)
      ]

renderTemplate :: ToJSON c => Text -> c -> Either Text Text
renderTemplate template ctx = do
  tpl <- tplEither
  let json = toJSON ctx
  let ctxGVal :: GVal MyGinger = rawJSONToGVal json
  let ctxGValWithFunctions = foldl (\x (name, func) -> addField name func x) ctxGVal caseFunctions

  let r :: Text = easyRender ctxGValWithFunctions tpl
  return r
  where
    tplEither :: Either Text (Template SourcePos)
    tplEither = parseGinger nullResolver Nothing (toS template) & runIdentity & mapLeft (show >>> T.pack >>> ("Render of Ginger template failed: " <>))
    nullResolver :: IncludeResolver Identity
    nullResolver = const $ return Nothing
    addField :: Text -> Function m -> GVal m -> GVal m
    addField fieldName fieldFunc ctxGVal =
      let fieldMap = M.singleton fieldName (fromFunction fieldFunc)
          mergedMap = (asHashMap ctxGVal & fromJust) `M.union` fieldMap
       in toGVal mergedMap
