{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Data.AppM where

import Control.Monad.Except (ExceptT, runExceptT, runExceptT, runExceptT, runExceptT, mapExceptT)
import Control.Monad.State (StateT, evalStateT, get, runStateT)
import Control.Monad.Trans.Except (except)
import Data.AppState
import Data.Default (def)
import Data.Either
import Data.Either.Extra (fromLeft', fromRight', mapLeft, eitherToMaybe)
import Data.Errors
import Data.Maybe (catMaybes)
import Data.Timing
import GHC.Generics (Generic)
import Optics
import Optics.State.Operators
import System.Clock (Clock (..), getTime, toNanoSecs)
import UnliftIO (liftIO, tryAny)
import Utils

type AppM = (ExceptT AppError (StateT AppState IO))

runAppM :: AppState -> AppM a -> IO (Either AppError a, AppState)
runAppM startState x = x & runExceptT & (runStateT ?? startState)

runAppMgetResUnsafe :: AppM a -> IO a
runAppMgetResUnsafe x = runAppM def x <&> (fst >>> fromRight')

evalAppM :: AppState -> AppM a -> IO (Either AppError a)
evalAppM startState x = x & runExceptT & (evalStateT ?? startState)

tryIO :: AppError -> IO a -> AppM a
tryIO err io = do
  tried <- tryAny io & liftIO
  tried & mapLeft (const err) & liftEitherToAppM

liftEitherToAppM :: Either AppError a -> AppM a
liftEitherToAppM = except

liftEithersToMultiError :: [Either AppError a] -> Either AppError [a]
liftEithersToMultiError xs = if length rights == length xs then Right rights else err
  where
    rights = xs <&> eitherToMaybe & catMaybes
    err = Left $ MultiError (xs & filter isLeft <&> fromLeft')

liftEithersToAppM :: [Either AppError a] -> AppM [a]
liftEithersToAppM = liftEithersToMultiError >>> liftEitherToAppM

liftEitherInIOToAppM :: IO (Either AppError a) -> AppM a
liftEitherInIOToAppM = (<&> liftEitherToAppM) >>> liftIO >>> join

mapAppErr :: (AppError -> AppError) -> AppM a -> AppM a
mapAppErr f = mapExceptT (<&> mapLeft f)

addWarning :: AppWarning -> AppM ()
addWarning x = #appWarnings %= (<> [x])

removeIgnoredWarnings :: [Text] -> AppM ()
removeIgnoredWarnings ignoredWarns =
  #appWarnings %= filter (getAppWarningWithoutContext >>> getAppWarningName >>> flip elem ignoredWarns >>> not)

addNonCriticalError :: NonCriticalError -> AppM ()
addNonCriticalError x = #appErrors %= (<> [x])

demoteError :: NonCriticalError -> AppM ()
demoteError x = do
  #appErrors %= filter (/= x)
  #appWarnings %= (<> [x & convToWarn])
  where
    convToWarn (NonCriticalError ctx e) = AppWarning ctx (DemotedError e)

removeIgnoredErrors :: [Text] -> AppM ()
removeIgnoredErrors ignoredErrs = do
  st <- get
  let isIgnoredErr = getNonCriticalErrorWithoutContext >>> getNonCriticalErrorName >>> flip elem ignoredErrs
  let errsToIgnore = st ^. #appErrors & filter isIgnoredErr
  forM_ errsToIgnore demoteError

data IgnoreRuleObject = IRWarning AppWarning | IRError NonCriticalError deriving (Eq, Show, Generic)

ctxFromIgnoreRuleObject :: IgnoreRuleObject -> ErrorContext
ctxFromIgnoreRuleObject (IRWarning (AppWarning ctx _)) = ctx
ctxFromIgnoreRuleObject (IRError (NonCriticalError ctx _)) = ctx

-- TODO: support for sub-timings
measureTiming :: Text -> AppM a -> AppM a
measureTiming name action = do
  startTime <- liftIO $ getTime Monotonic
  res <- action
  stopTime <- liftIO $ getTime Monotonic
  let time = fromIntegral (toNanoSecs stopTime - toNanoSecs startTime) / 1000000000
  #timing .= (Just $ TimingItem {timingName = name, subTimings = [], elapsedTime = time})
  return res
