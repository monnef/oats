{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.AppState where

import Data.ColorMode (ColorMode (..))
import Data.Default
import Data.Errors
import Data.Timing (TimingItem)
import GHC.Generics (Generic)
import Optics

data AppState = AppState
  { appWarnings :: [AppWarning],
    appErrors :: [NonCriticalError],
    timing :: Maybe TimingItem,
    finalColorOutput :: ColorMode
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''AppState

instance Default AppState where
  def =
    AppState
      { appWarnings = [],
        appErrors = [],
        timing = Nothing,
        finalColorOutput = NoColor
      }
