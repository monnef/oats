{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoFieldSelectors #-}

module Data.RawOA where

import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types as AT
import Data.Configuration
import Data.Data
import Data.Default (Default, def)
import qualified Data.HashMap.Lazy as M
import Data.Hashable (Hashable)
import Data.Maybe (fromJust, isJust)
import qualified Data.Text as T
import GHC.Generics (Generic)
import Language.Haskell.TH (Exp, Q)
import qualified Language.Haskell.TH as TH
import qualified Language.Haskell.TH.Syntax as THS
import Optics
import Utils
import UtilsTH

data DiscriminatorROA = DiscriminatorROA
  { propertyName :: Text,
    mapping :: Maybe (M.HashMap Text Text)
  }
  deriving (Eq, Show, Generic, Data)

makeFieldLabelsNoPrefix ''DiscriminatorROA

deriveJSON derJsonOpts ''DiscriminatorROA

data RawOA = RawOA
  { typ :: Maybe Text,
    ref :: Maybe Text,
    title :: Maybe Text,
    required :: Maybe [Text],
    properties :: Maybe (M.HashMap Text RawOA),
    allOf :: Maybe [RawOA],
    items :: Maybe RawOA,
    oneOf :: Maybe [RawOA],
    discriminator :: Maybe DiscriminatorROA,
    enum :: Maybe [Text],
    example :: Maybe Value,
    pattern :: Maybe Text,
    minLength :: Maybe Int,
    xOatsTrimmedMinLength :: Maybe Int,
    maxLength :: Maybe Int,
    minimum :: Maybe Double,
    maximum :: Maybe Double,
    exclusiveMinimum :: Maybe Bool,
    exclusiveMaximum :: Maybe Bool,
    multipleOf :: Maybe Double,
    format :: Maybe Text,
    uniqueItems :: Maybe Bool,
    description :: Maybe Text,
    minItems :: Maybe Int,
    maxItems :: Maybe Int,
    xOverrideNonRequiredRepresentation :: Maybe NonRequiredRepresentation,
    xOatsOverrideNonRequiredRepresentation :: Maybe NonRequiredRepresentation,
    comment :: Maybe Text
  }
  deriving (Eq, Show, Generic, Data)

instance Default RawOA where
  def =
    RawOA
      { typ = Nothing,
        ref = Nothing,
        title = Nothing,
        required = Nothing,
        properties = Nothing,
        allOf = Nothing,
        items = Nothing,
        oneOf = Nothing,
        discriminator = Nothing,
        enum = Nothing,
        example = Nothing,
        pattern = Nothing,
        minLength = Nothing,
        maxLength = Nothing,
        xOatsTrimmedMinLength = Nothing,
        minimum = Nothing,
        maximum = Nothing,
        exclusiveMinimum = Nothing,
        exclusiveMaximum = Nothing,
        multipleOf = Nothing,
        format = Nothing,
        uniqueItems = Nothing,
        description = Nothing,
        minItems = Nothing,
        maxItems = Nothing,
        xOverrideNonRequiredRepresentation = Nothing,
        xOatsOverrideNonRequiredRepresentation = Nothing,
        comment = Nothing
      }

makeFieldLabelsNoPrefix ''RawOA

deriveJSON derJsonOpts ''RawOA

isAnyValidationFilled :: RawOA -> Bool
isAnyValidationFilled x =
  [ [#pattern] & p,
    [#minLength, #maxLength, #minItems, #maxItems] & p,
    [#minimum, #maximum, #multipleOf] & p,
    [#exclusiveMinimum, #exclusiveMaximum, #uniqueItems] & p
  ]
    & or
  where
    p :: [Optic' A_Lens is RawOA (Maybe a)] -> Bool
    p labels = labels <&> (x ^.) & any isJust

data ValidationItem = ValidationItem {name :: Text, getter :: RawOA -> Maybe Text}

makeFieldLabelsNoPrefix ''ValidationItem

instance Eq ValidationItem where
  x == y = x ^. #name == y ^. #name

genValidationTupleTH :: Text -> Q Exp
genValidationTupleTH g = do
  let name = return $ TH.LitE $ TH.StringL $ toS g
  let label = return $ THS.LabelE $ toS g
  [|ValidationItem $(name) (\x -> x ^. $(label) <&> tshow)|]

genValidationListTH :: [Text] -> Q Exp
genValidationListTH xs = xs & mapM genValidationTupleTH <&> TH.ListE

newtype RawOAComponents = RawOAComponents {schemas :: M.HashMap Text RawOA} deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RawOAComponents

deriveJSON derJsonOpts ''RawOAComponents

data RawOATag = RawOATag
  { name :: Text,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RawOATag

deriveJSON derJsonOpts ''RawOATag

data OAParameterIn = QueryParameterInType | PathParameterInType | CookieParameterInType
  deriving (Eq, Show, Generic)

deriveJSON derJsonOptsOfOAParameterIn ''OAParameterIn

data RawOAParameter = RawOAParameter
  { inn :: OAParameterIn,
    name :: Text,
    schema :: RawOA,
    required :: Maybe Bool,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RawOAParameter

deriveJSON derJsonOpts ''RawOAParameter

instance Default RawOAParameter where
  def = RawOAParameter {inn = QueryParameterInType, name = "?", schema = def, required = Nothing, description = Nothing}

data RawOAContentItem = RawOAContentItem {schema :: RawOA} deriving (Eq, Show, Generic, Data)

makeFieldLabelsNoPrefix ''RawOAContentItem

deriveJSON derJsonOpts ''RawOAContentItem

instance Default RawOAContentItem where
  def = RawOAContentItem {schema = def}

type RawOAContent = M.HashMap Text RawOAContentItem

data RawOARequestOrResponse = RawOARequestOrResponse
  { content :: Maybe RawOAContent,
    description :: Maybe Text,
    ref :: Maybe Text
  }
  deriving (Eq, Show, Generic, Data)

makeFieldLabelsNoPrefix ''RawOARequestOrResponse

deriveJSON derJsonOpts ''RawOARequestOrResponse

instance Default RawOARequestOrResponse where
  def = RawOARequestOrResponse {content = Nothing, description = Nothing, ref = Nothing}

data OASecurityItem = OASecurityItem {permissions :: Maybe [Text]} deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OASecurityItem
deriveJSON derJsonOpts ''OASecurityItem

type OASecurity = [OASecurityItem]

data RawOAPath = RawOAPath
  { operationId :: Text,
    summary :: Maybe Text,
    description :: Maybe Text,
    tags :: [Text],
    parameters :: Maybe [RawOAParameter],
    responses :: M.HashMap Int RawOARequestOrResponse,
    requestBody :: Maybe RawOARequestOrResponse,
    xMeta :: Maybe Value,
    xOatsMeta :: Maybe Value,
    security :: Maybe OASecurity
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RawOAPath

deriveJSON derJsonOpts ''RawOAPath

instance Default RawOAPath where
  def =
    RawOAPath
      { operationId = "?",
        summary = Nothing,
        description = Nothing,
        tags = [],
        parameters = Nothing,
        responses = M.fromList [],
        requestBody = Nothing,
        xMeta = Nothing,
        xOatsMeta = Nothing,
        security = Nothing
      }

data RequestType = GetRequest | PostRequest | PutRequest | PatchRequest | DeleteRequest deriving (Eq, Show, Generic, Hashable, Data)

instance Default RequestType where
  def = GetRequest

-- TODO: use TH?

instance FromJSON RequestType where
  parseJSON (AT.String x) =
    case x of
      "get" -> pure GetRequest
      "post" -> pure PostRequest
      "put" -> pure PutRequest
      "patch" -> pure PatchRequest
      "delete" -> pure DeleteRequest
      _ -> fail [qq|"$x" is not a valid RequestType|]
  parseJSON invalid =
    prependFailure
      "parsing RequestType failed, "
      (typeMismatch "Text" invalid)

instance ToJSON RequestType where
  toJSON GetRequest = String "get"
  toJSON PostRequest = String "post"
  toJSON PutRequest = String "put"
  toJSON PatchRequest = String "patch"
  toJSON DeleteRequest = String "delete"

instance FromJSONKey RequestType where
  fromJSONKey = FromJSONKeyText (AT.String >>> parseEither parseJSON >>> orCrash)

instance ToJSONKey RequestType where
  toJSONKey = toJSONKeyText (toJSON >>> unpackString)
    where
      unpackString (String x) = x
      unpackString _ = error "unexpected non-String in RequestType"

requestTypeToText :: RequestType -> Text
requestTypeToText = toJSON >>> unpackAesonString >>> fromJust >>> T.toUpper

type RawOAPaths = M.HashMap RequestType RawOAPath

data RawOAFile = RawOAFile
  { components :: RawOAComponents,
    tags :: Maybe [RawOATag],
    paths :: Maybe (M.HashMap Text RawOAPaths)
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RawOAFile

deriveJSON derJsonOpts ''RawOAFile
