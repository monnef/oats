{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Data.CommandLineArguments where

import Data.Default (Default, def)
import GHC.Generics (Generic)
import Optics
import Utils

data CmdArgs = CmdArgs
  { verbose :: Bool,
    printVersion :: Bool,
    nonRequiredAsUndefined :: Bool,
    configPath :: Maybe Text,
    quiet :: Bool,
    apiPath :: Maybe Text,
    apiFile :: Maybe Text,
    ignoreFilesRegex :: Maybe Text,
    title :: Maybe Text,
    outPath :: Maybe Text,
    printWarningsList :: Bool,
    printDefaultSchemaTranslations :: Bool,
    printErrorsList :: Bool,
    noSummary :: Bool,
    noTiming :: Bool,
    noColor :: Bool,
    dryRun :: Bool
  }
  deriving (Eq, Show, Generic)

makeFieldsNoPrefix ''CmdArgs

instance Default CmdArgs where
  def =
    CmdArgs
      { verbose = False,
        printVersion = False,
        nonRequiredAsUndefined = False,
        configPath = Nothing,
        quiet = False,
        apiPath = Nothing,
        apiFile = Nothing,
        ignoreFilesRegex = Nothing,
        title = Nothing,
        outPath = Nothing,
        printWarningsList = False,
        printDefaultSchemaTranslations = False,
        printErrorsList = False,
        noSummary = False,
        noTiming = False,
        noColor = False,
        dryRun = False
      }
