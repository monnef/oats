{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Timing where

import GHC.Generics (Generic)
import Optics
import Utils

data TimingItem = TimingItem
  { timingName :: Text,
    subTimings :: [TimingItem],
    elapsedTime :: Double
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''TimingItem
