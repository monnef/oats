{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# LANGUAGE NoFieldSelectors #-}

module Data.Model(
  module Data.Model,
  module Data.RawOA
) where

import Data.Aeson.TH (deriveJSON)
import Data.Aeson.Types as AT
import Data.Configuration
import Data.Data
import Data.RawOA
import Data.Default (Default, def)
import qualified Data.HashMap.Lazy as M
import GHC.Generics (Generic)
import Optics
import Utils
import UtilsTH

data OATag = OATag
  { name :: Text,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OATag

deriveJSON derJsonOpts ''OATag

type Tags = [OATag]


data PrimTypeOA = StringPOA | BooleanPOA | IntegerPOA | NumberPOA deriving (Eq, Show)

data TypeOA
  = PrimitiveTOA PrimTypeOA
  | ArrayTOA TypeOA
  | SchemaTOA Text
  | OneValueEnumTOA Text
  deriving (Eq, Show)

data IsRequiredOA = RequiredOA | OptionalOA deriving (Eq, Show)

data FieldOA = FieldOA
  { name :: Text,
    typ :: TypeOA,
    required :: IsRequiredOA,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''FieldOA

data OA
  = OAClass
      { extends :: Maybe Text,
        fields :: [FieldOA],
        tag :: Maybe Text, -- from discriminator.propertyName
        overrideNonRequiredRepresentation :: Maybe NonRequiredRepresentation
      }
  | OAString
      { stringPattern :: Maybe Text,
        stringMinLength :: Maybe Int,
        stringTrimmedMinLength :: Maybe Int,
        stringMaxLength :: Maybe Int
      }
  | OAInteger
      { integerMinimum :: Maybe Int,
        integerMaximum :: Maybe Int
      }
  | OANumber
      { numberMinimum :: Maybe Double,
        numberMaximum :: Maybe Double,
        numberExclusiveMinimum :: Bool,
        numberExclusiveMaximum :: Bool,
        numberMultipleOf :: Maybe Double,
        numberDecimalPrecision :: Maybe Int
      }
  | OAArray
      { itemsType :: TypeOA,
        arrayUniqueItems :: Bool,
        arrayMinItems :: Maybe Int,
        arrayMaxItems :: Maybe Int
      }
  | OAUnion
      { members :: [TypeOA],
        tag :: Maybe Text
      }
  | OAEnum
      { enumItems :: [Text]
      }
  | OACustom
      { customSchemaCode :: Text
      }
  | OADecimal
      { decimalPrecision :: Int, -- total digits count
        decimalScale :: Int, -- decimal digits count
        decimalMin :: Maybe Text,
        decimalMax :: Maybe Text
      }
  | OAAlias
      { aliasTarget :: Text
      }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OA
makePrisms ''OA

type TaggedUnionMapping = M.HashMap Text Text

data OAMeta = OAMeta
  { name :: Text,
    path :: Maybe Text,
    dependencies :: [Text],
    taggedUnionMapping :: Maybe TaggedUnionMapping,
    example :: Maybe Text,
    description :: Maybe Text,
    comment :: Maybe Text,
    isRecursive :: Maybe Bool
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OAMeta

instance Default OAMeta where
  def =
    OAMeta
      { name = "?",
        path = Nothing,
        dependencies = [],
        taggedUnionMapping = Nothing,
        example = Nothing,
        description = Nothing,
        comment = Nothing,
        isRecursive = Nothing
      }

type SchemaTranslationTable = M.HashMap Text TypeOA

defaultSchemaTranslationTable :: SchemaTranslationTable
defaultSchemaTranslationTable =
  M.fromList
    [ ("Boolean", PrimitiveTOA BooleanPOA),
      ("Double", PrimitiveTOA NumberPOA),
      ("Int32", PrimitiveTOA IntegerPOA),
      ("Int32Array", ArrayTOA $ PrimitiveTOA IntegerPOA),
      ("Int64", PrimitiveTOA IntegerPOA),
      ("Int64Array", ArrayTOA $ PrimitiveTOA IntegerPOA)
    ]

data CommunicationDirection = RequestDirection | ResponseDirection deriving (Eq, Show, Generic, Data)

comDirToText :: CommunicationDirection -> Text
comDirToText RequestDirection = "request"
comDirToText ResponseDirection = "response"

data OARequestOrResponse = OARequestOrResponse
  { mime :: Maybe Text,
    schema :: Maybe TypeOA,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

instance Default OARequestOrResponse where
  def =
    OARequestOrResponse
      { mime = Nothing,
        schema = Nothing,
        description = Nothing
      }

makeFieldLabelsNoPrefix ''OARequestOrResponse

data OAParameter = OAParameter
  { inn :: OAParameterIn,
    name :: Text,
    schema :: TypeOA,
    required :: Bool,
    description :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OAParameter

data OAEndpoint = OAEndpoint
  { operationId :: Text,
    summary :: Maybe Text,
    description :: Maybe Text,
    tag :: Text,
    parameters :: [OAParameter],
    responses :: M.HashMap Int OARequestOrResponse,
    requestType :: RequestType,
    request :: Maybe OARequestOrResponse,
    meta :: Maybe Value,
    permissions :: [Text]
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''OAEndpoint

instance Default OAEndpoint where
  def =
    OAEndpoint
      { operationId = "?",
        summary = Nothing,
        description = Nothing,
        tag = "?",
        parameters = [],
        responses = M.fromList [],
        requestType = def,
        request = Nothing,
        meta = Nothing,
        permissions = []
      }

type OARoute = M.HashMap RequestType OAEndpoint

type URI = Text

type OARoutes = M.HashMap URI OARoute
