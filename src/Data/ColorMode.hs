{-# LANGUAGE DeriveGeneric #-}

module Data.ColorMode where

import GHC.Generics (Generic)

data ColorMode = UseColor | NoColor deriving (Eq, Show, Generic)

shouldUseColor :: ColorMode -> Bool
shouldUseColor UseColor = True
shouldUseColor NoColor = False
