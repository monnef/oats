{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Data.Configuration where

import Data.Aeson.TH (deriveJSON, rejectUnknownFields)
import Data.ColorMode (ColorMode (..))
import Data.Data (Data, Typeable)
import Data.Default (Default, def)
import qualified Data.HashMap.Lazy as M
import GHC.Generics (Generic)
import Generics.Deriving.ConNames (conNames)
import Optics
import Text.RE.PCRE.Text (RE, reSource)
import Utils
import UtilsTH
import Data.Aeson (FromJSON, toJSON, ToJSON)
import qualified Data.Aeson as AES
import Data.Aeson.Types (typeMismatch)

data NonRequiredRepresentation = NullAsNonRequired | UndefinedAsNonRequired deriving (Eq, Show, Generic, Data, Typeable)

deriveJSON derJsonOpts ''NonRequiredRepresentation

validNonRequiredRepresentationNamesForConfig :: [Text]
validNonRequiredRepresentationNamesForConfig = conNames (undefined :: NonRequiredRepresentation) <&> toS <&> deCapitalize

data EndpointsGenerationMode = AllEndpointsInOneFile | EndpointsWithSameTagInOneFile deriving (Eq, Show, Generic, Data, Typeable)

deriveJSON derJsonOpts ''EndpointsGenerationMode

instance Show RE where
  show = reSource >>> \x -> [qq|/$x/|]

instance Eq RE where
  x == y = show x == show y

data ApiFileToMerge = ApiFileToMerge
  { path :: Text
  }
  deriving (Show, Eq)

makeFieldLabelsNoPrefix ''ApiFileToMerge
deriveJSON derJsonOpts ''ApiFileToMerge

data IgnoreRuleTargets = IgnoreRuleTargets
  { errors :: [Text],
    warnings :: [Text]
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''IgnoreRuleTargets

instance Default IgnoreRuleTargets where
  def = IgnoreRuleTargets {errors = [], warnings = []}

data IgnoreRuleSelector
  = IgnoreRuleEndpointByOperationId
      { operationIdRegex :: RE
      }
  | IgnoreRuleEndpointByUrl
      { urlRegex :: RE
      }
  | IgnoreRuleSchemaByName
      { schemaNameRegex :: RE
      }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''IgnoreRuleSelector

data IgnoreRule = IgnoreRule
  { selector :: IgnoreRuleSelector,
    targets :: IgnoreRuleTargets
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''IgnoreRule

data NumberMinMaxLimitsMode
  = InclusiveOnly
  | ExclusiveOnly
  | InclusiveAndExclusive
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''NumberMinMaxLimitsMode

instance FromJSON NumberMinMaxLimitsMode where
  parseJSON (AES.String "inclusive") = return InclusiveOnly
  parseJSON (AES.String "exclusive") = return ExclusiveOnly
  parseJSON (AES.String "both") = return InclusiveAndExclusive
  parseJSON invalid = typeMismatch "NumberMinMaxLimitsMode" invalid

instance ToJSON NumberMinMaxLimitsMode where
  toJSON InclusiveOnly = AES.String "inclusive"
  toJSON ExclusiveOnly = AES.String "exclusive"
  toJSON InclusiveAndExclusive = AES.String "both"

-- | Configuration for internal use
data Config = Config
  { nonRequiredRepresentation :: NonRequiredRepresentation,
    schemaTranslation :: Bool,
    propertyNameMap :: M.HashMap Text Text,
    useExcess :: Bool,
    excessImport :: Maybe Text,
    quiet :: Bool,
    apiPath :: Maybe Text,
    apiFile :: Maybe Text,
    apiFilesToMerge :: [ApiFileToMerge],
    ignoreFilesRegex :: Maybe RE,
    stringPatternImport :: Maybe Text,
    stringPatternSchemaGenerator :: Maybe Text,
    header :: Maybe Text,
    titleFromCmdArgs :: Maybe Text,
    outPath :: Maybe Text,
    apiModule :: Text,
    customSchemasPath :: Maybe Text,
    ignoreComponentsRegex :: Maybe RE,
    ignoreWarnings :: [Text],
    promoteWarningsToErrors :: [Text],
    endpointsGenerationMode :: Maybe EndpointsGenerationMode,
    endpointsTemplate :: Maybe Text,
    endpointsTemplateFile :: Maybe Text,
    emptyRequestSchema :: Text,
    emptyResponseType :: Text,
    emptyResponseSchema :: Text,
    downloadFileResponseSchema :: Text,
    downloadFileResponseType :: Text,
    uploadFileRequestSchema :: Text,
    uploadFileRequestType :: Text,
    pathParamsParameterName :: Text,
    queryParamsParameterName :: Text,
    ignoreEndpointsByPathRegex :: Maybe RE,
    customEndpointsFile :: Maybe Text,
    verbose :: Bool,
    ignoreErrors :: [Text],
    noSummary :: Bool,
    ignoreRules :: [IgnoreRule],
    noTiming :: Bool,
    colorOutput :: ColorMode,
    noPatternValidationWithExample :: Bool,
    cleanOutputDirectory :: Bool,
    dryRun :: Bool,
    afterFileWriteCommand :: Maybe Text,
    apiInfoEnabled :: Bool,
    numberMinMaxLimitsEnabled :: Bool,
    numberMinMaxLimitsTemplate :: Text,
    numberMinMaxLimitsMode :: NumberMinMaxLimitsMode,
    numberMinMaxLimitsCombinedEnabled :: Bool,
    numberMinMaxLimitsCombinedTemplate :: Text,
    numberMinMaxLimitsCombinedMode :: NumberMinMaxLimitsMode
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''Config

instance Default Config where
  def =
    Config
      { nonRequiredRepresentation = NullAsNonRequired,
        schemaTranslation = True,
        propertyNameMap = M.fromList [],
        useExcess = False,
        excessImport = Nothing,
        quiet = False,
        apiPath = Nothing,
        apiFilesToMerge = [],
        apiFile = Nothing,
        ignoreFilesRegex = Nothing,
        stringPatternImport = Nothing,
        stringPatternSchemaGenerator = Nothing,
        header = Nothing,
        titleFromCmdArgs = Nothing,
        outPath = Nothing,
        apiModule = "gen",
        customSchemasPath = Nothing,
        ignoreComponentsRegex = Nothing,
        ignoreWarnings = [],
        promoteWarningsToErrors = [],
        endpointsGenerationMode = Nothing,
        endpointsTemplate = Nothing,
        endpointsTemplateFile = Nothing,
        emptyRequestSchema = "t.void",
        emptyResponseType = "void",
        emptyResponseSchema = "t.void",
        downloadFileResponseSchema = "ArrayBufferSchema",
        downloadFileResponseType = "ArrayBuffer",
        uploadFileRequestSchema = "FormDataSchema",
        uploadFileRequestType = "FormData",
        pathParamsParameterName = "pathParams",
        queryParamsParameterName = "params",
        ignoreEndpointsByPathRegex = Nothing,
        customEndpointsFile = Nothing,
        verbose = False,
        ignoreErrors = [],
        noSummary = False,
        ignoreRules = [],
        noTiming = False,
        colorOutput = UseColor,
        noPatternValidationWithExample = False,
        cleanOutputDirectory = False,
        dryRun = False,
        afterFileWriteCommand = Nothing,
        apiInfoEnabled = True,
        numberMinMaxLimitsEnabled = True,
        numberMinMaxLimitsTemplate = "export const {{schemaName|toCamel()}}{{limitName|toPascal()}}{{isInclusive ? 'Inclusive' : 'Exclusive'}} = {{limitValue}};",
        numberMinMaxLimitsMode = InclusiveOnly,
        numberMinMaxLimitsCombinedEnabled = True,
        numberMinMaxLimitsCombinedTemplate = "export const {{schemaName|toCamel()}}MinMax{{isInclusive ? 'Inclusive' : 'Exclusive'}}: [number | null, number | null] = [{{limitMinValue}}, {{limitMaxValue}}];",
        numberMinMaxLimitsCombinedMode = InclusiveOnly
      }

type PropertyNameMap = M.HashMap Text Text

data IgnoreRuleRaw = IgnoreRuleRaw
  { errors :: Maybe [Text],
    warnings :: Maybe [Text],
    operationId :: Maybe Text,
    url :: Maybe Text,
    schemaName :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''IgnoreRuleRaw
deriveJSON (derJsonOpts {rejectUnknownFields = True}) ''IgnoreRuleRaw

instance Default IgnoreRuleRaw where
  def =
    IgnoreRuleRaw
      { errors = Nothing,
        warnings = Nothing,
        operationId = Nothing,
        url = Nothing,
        schemaName = Nothing
      }

-- | Configuration read from file
data MaybeConfig = MaybeConfig
  { nonRequiredRepresentation :: Maybe NonRequiredRepresentation,
    schemaTranslation :: Maybe Bool,
    propertyNameMap :: Maybe PropertyNameMap,
    useExcess :: Maybe Bool,
    excessImport :: Maybe Text,
    apiPath :: Maybe Text,
    apiFile :: Maybe Text,
    apiFilesToMerge :: Maybe [ApiFileToMerge],
    ignoreFilesRegex :: Maybe Text,
    stringPatternImport :: Maybe Text,
    stringPatternSchemaGenerator :: Maybe Text,
    header :: Maybe Text,
    outPath :: Maybe Text,
    apiModule :: Maybe Text,
    customSchemasPath :: Maybe Text,
    ignoreComponentsRegex :: Maybe Text,
    ignoreWarnings :: Maybe [Text],
    promoteWarningsToErrors :: Maybe [Text],
    endpointsGenerationMode :: Maybe EndpointsGenerationMode,
    endpointsTemplate :: Maybe Text,
    endpointsTemplateFile :: Maybe Text,
    emptyRequestSchema :: Maybe Text,
    emptyResponseType :: Maybe Text,
    emptyResponseSchema :: Maybe Text,
    downloadFileResponseSchema :: Maybe Text,
    downloadFileResponseType :: Maybe Text,
    uploadFileRequestSchema :: Maybe Text,
    uploadFileRequestType :: Maybe Text,
    pathParamsParameterName :: Maybe Text,
    queryParamsParameterName :: Maybe Text,
    ignoreEndpointsByPathRegex :: Maybe Text,
    customEndpointsFile :: Maybe Text,
    quiet :: Maybe Bool,
    verbose :: Maybe Bool,
    ignoreErrors :: Maybe [Text],
    ignoreRules :: Maybe [IgnoreRuleRaw],
    noTiming :: Maybe Bool,
    noPatternValidationWithExample :: Maybe Bool,
    color :: Maybe Bool,
    cleanOutputDirectory :: Maybe Bool,
    afterFileWriteCommand :: Maybe Text,
    apiInfoEnabled :: Maybe Bool,
    numberMinMaxLimitsEnabled :: Maybe Bool,
    numberMinMaxLimitsTemplate :: Maybe Text,
    numberMinMaxLimitsMode :: Maybe NumberMinMaxLimitsMode,
    numberMinMaxLimitsCombinedEnabled :: Maybe Bool,
    numberMinMaxLimitsCombinedTemplate :: Maybe Text,
    numberMinMaxLimitsCombinedMode :: Maybe NumberMinMaxLimitsMode
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''MaybeConfig
deriveJSON (derJsonOpts {rejectUnknownFields = True}) ''MaybeConfig

instance Default MaybeConfig where
  def =
    MaybeConfig
      { nonRequiredRepresentation = Nothing,
        schemaTranslation = Nothing,
        propertyNameMap = mempty,
        useExcess = Nothing,
        excessImport = Nothing,
        apiPath = Nothing,
        apiFile = Nothing,
        apiFilesToMerge = Nothing,
        ignoreFilesRegex = Nothing,
        stringPatternImport = Nothing,
        stringPatternSchemaGenerator = Nothing,
        header = Nothing,
        outPath = Nothing,
        apiModule = Nothing,
        customSchemasPath = Nothing,
        ignoreComponentsRegex = Nothing,
        ignoreWarnings = Nothing,
        promoteWarningsToErrors = Nothing,
        endpointsGenerationMode = Nothing,
        endpointsTemplate = Nothing,
        endpointsTemplateFile = Nothing,
        emptyRequestSchema = Nothing,
        emptyResponseType = Nothing,
        emptyResponseSchema = Nothing,
        downloadFileResponseSchema = Nothing,
        downloadFileResponseType = Nothing,
        uploadFileRequestSchema = Nothing,
        uploadFileRequestType = Nothing,
        pathParamsParameterName = Nothing,
        queryParamsParameterName = Nothing,
        ignoreEndpointsByPathRegex = Nothing,
        customEndpointsFile = Nothing,
        quiet = Nothing,
        verbose = Nothing,
        ignoreErrors = Nothing,
        ignoreRules = Nothing,
        noTiming = Nothing,
        noPatternValidationWithExample = Nothing,
        color = Nothing,
        cleanOutputDirectory = Nothing,
        afterFileWriteCommand = Nothing,
        apiInfoEnabled = Nothing,
        numberMinMaxLimitsEnabled = Nothing,
        numberMinMaxLimitsTemplate = Nothing,
        numberMinMaxLimitsMode = Nothing,
        numberMinMaxLimitsCombinedEnabled = Nothing,
        numberMinMaxLimitsCombinedTemplate = Nothing,
        numberMinMaxLimitsCombinedMode = Nothing
      }

defaultConfigFileName :: Text
defaultConfigFileName = "oats.yaml"
