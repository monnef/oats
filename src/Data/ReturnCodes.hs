module Data.ReturnCodes where

import Data.Maybe (fromJust)
import Data.Tuple (swap)

data ReturnCode
  = RetOk
  | RetErrParsingRE
  | RetErrConfigParsing
  | RetErrConfig
  | RetErrCrawler
  | RetErrProcessing
  | RetErrPromotedWarning
  | RetErrIO
  | RetErrUnknown
  deriving (Eq, Show)

instance Enum ReturnCode where
  fromEnum = fromJust . flip lookup retErrTable
  toEnum = fromJust . flip lookup (map swap retErrTable)

retErrTable :: [(ReturnCode, Int)]
retErrTable =
  [ (RetOk, 0),
    (RetErrParsingRE, 1),
    (RetErrConfigParsing, 2),
    (RetErrConfig, 3),
    (RetErrCrawler, 4),
    (RetErrProcessing, 5),
    (RetErrPromotedWarning, 6),
    (RetErrIO, 7),
    (RetErrUnknown, 255)
  ]

retErrVal :: ReturnCode -> Int
retErrVal = fromEnum
