{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE GADTs #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Data.Errors where

import Control.Applicative ((<|>))
import Data.Data
import Data.Default (Default, def)
import Data.Maybe (fromMaybe)
import Data.Model (CommunicationDirection, RawOARequestOrResponse, RequestType, comDirToText, requestTypeToText)
import Data.ReturnCodes
import qualified Data.Text as T
import GHC.Generics (Generic)
import Generics.Deriving.ConNames (conNameOf, conNames)
import Optics
import System.Console.ANSI
import TerminalUtils (colorText)
import Utils

data ErrorContext = ErrorContext
  { fileName :: Maybe Text,
    unitName :: Maybe Text,
    lineNumber :: Maybe Int,
    subUnitName :: Maybe Text,
    -- for ignore rules
    endpointOperationId :: Maybe Text,
    endpointUrl :: Maybe Text,
    schemaName :: Maybe Text,
    -- /for ignore rules
    endpointCommunicationDirection :: Maybe CommunicationDirection,
    endpointRequestType :: Maybe RequestType
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''ErrorContext

instance Default ErrorContext where
  def =
    ErrorContext
      { fileName = Nothing,
        unitName = Nothing,
        lineNumber = Nothing,
        subUnitName = Nothing,
        endpointOperationId = Nothing,
        endpointUrl = Nothing,
        schemaName = Nothing,
        endpointCommunicationDirection = Nothing,
        endpointRequestType = Nothing
      }

mkErrCtxFromFileName :: Text -> ErrorContext
mkErrCtxFromFileName fn = def & #fileName ?~ fn

mkErrCtxFromSchemaName :: Text -> ErrorContext
mkErrCtxFromSchemaName schName = def & #unitName ?~ schName

mkErrCtxFromFileNameOrDef :: Maybe Text -> ErrorContext
mkErrCtxFromFileNameOrDef = fmap mkErrCtxFromFileName >>> fromMaybe def

mkErrCtxFromFileAndSchemaNameOrDef :: Maybe Text -> Maybe Text -> ErrorContext
mkErrCtxFromFileAndSchemaNameOrDef fileNam schemaName =
  def
    & #fileName .~ fileNam
    & #unitName .~ schemaName
    & #schemaName .~ schemaName

mkErrCtxFromFileNameOperationIdAndUrlOrDef :: Maybe Text -> Text -> Text -> ErrorContext
mkErrCtxFromFileNameOperationIdAndUrlOrDef fileNam opId url =
  mkErrCtxFromFileNameOrDef fileNam
    & #endpointOperationId ?~ opId
    & #endpointUrl ?~ url
    & #unitName ?~ opId

mkErrCtxForEndpoints :: Maybe Text -> Text -> Text -> Maybe CommunicationDirection -> RequestType -> ErrorContext
mkErrCtxForEndpoints fileNam opId url dir rt =
  mkErrCtxFromFileNameOperationIdAndUrlOrDef fileNam opId url
    & #endpointCommunicationDirection .~ dir
    & #endpointRequestType ?~ rt

data NonCriticalErrorWithoutContext where
  PrimitiveTypeInRequestOrResponseBodyError :: CommunicationDirection -> RawOARequestOrResponse -> NonCriticalErrorWithoutContext
  ForbiddenValidationError :: NonCriticalErrorWithoutContext
  DisallowedValidationError :: [(Text, Text)] -> NonCriticalErrorWithoutContext
  deriving (Eq, Show, Generic, Data)

nonCriticalErrorSuffix :: Text
nonCriticalErrorSuffix = "Error"

nonCriticalErrorNames :: [Text]
nonCriticalErrorNames = conNames (undefined :: NonCriticalErrorWithoutContext) <&> toS <&> stripSuffixIfPresent nonCriticalErrorSuffix

isNonCriticalErrorName :: Text -> Bool
isNonCriticalErrorName = flip elem nonCriticalErrorNames

getNonCriticalErrorName :: NonCriticalErrorWithoutContext -> Text
getNonCriticalErrorName = conNameOf >>> toS >>> stripSuffixIfPresent nonCriticalErrorSuffix

-- | Ignorable error
data NonCriticalError = NonCriticalError ErrorContext NonCriticalErrorWithoutContext deriving (Eq, Show, Generic)

getNonCriticalErrorWithoutContext :: NonCriticalError -> NonCriticalErrorWithoutContext
getNonCriticalErrorWithoutContext (NonCriticalError _ e) = e

data AppError where
  ConfigError :: Text -> AppError
  ParseError :: ErrorContext -> Text -> AppError
  NonCriticalAppError :: NonCriticalError -> AppError
  ValidationError :: ErrorContext -> Text -> AppError
  GenerationError :: ErrorContext -> Text -> AppError
  CrawlerError :: Text -> AppError
  IOError :: Text -> AppError
  PromotedWarning :: Text -> AppError
  MultiError :: [AppError] -> AppError
  ErrorAlreadyHandled :: AppError -> AppError
  ImpossibleError :: AppError
  deriving (Eq, Show, Generic)

makePrisms ''AppError

appErrToReturnCode :: AppError -> ReturnCode
appErrToReturnCode ImpossibleError = RetErrUnknown
appErrToReturnCode (ErrorAlreadyHandled x) = appErrToReturnCode x
appErrToReturnCode (MultiError (x : _)) = appErrToReturnCode x
appErrToReturnCode (MultiError []) = RetErrUnknown
appErrToReturnCode (CrawlerError _) = RetErrCrawler
appErrToReturnCode (GenerationError _ _) = RetErrProcessing
appErrToReturnCode (ValidationError _ _) = RetErrProcessing
appErrToReturnCode (NonCriticalAppError _) = RetErrProcessing
appErrToReturnCode (ParseError _ _) = RetErrParsingRE
appErrToReturnCode (ConfigError _) = RetErrConfig
appErrToReturnCode (PromotedWarning _) = RetErrPromotedWarning
appErrToReturnCode (IOError _) = RetErrIO

updateErrorContextInAppError :: (ErrorContext -> ErrorContext) -> AppError -> AppError
updateErrorContextInAppError f (ParseError ctx x) = ParseError (f ctx) x
updateErrorContextInAppError f (ValidationError ctx x) = ValidationError (f ctx) x
updateErrorContextInAppError f (GenerationError ctx x) = GenerationError (f ctx) x
updateErrorContextInAppError f (MultiError xs) = MultiError $ xs <&> updateErrorContextInAppError f
updateErrorContextInAppError _ x = x

formatErrorContext :: ErrorContext -> Maybe Text
formatErrorContext ErrorContext {..} =
  case (fileName, endpointOperationId <|> unitName, lineNumber, subUnitName, endpointUrl, endpointCommunicationDirection, endpointRequestType) of
    (Nothing, Nothing, Nothing, Nothing, Nothing, Nothing, Nothing) -> Nothing
    (fileName', unitNameOrOpId, lineNum, subUnitName', url, dir, reqTyp) ->
      Just $
        mconcat
          [ fileName' <&> colorText Vivid Black & fromMaybe "<unknown>",
            lineNum <&> tshow <&> colorText Vivid Green <&> (":" <>) & fromMaybe "",
            unitNameOrOpId <&> colorText Vivid Cyan <&> ("#" <>) & fromMaybe "",
            subUnitName' <&> colorText Vivid Magenta <&> ("." <>) & fromMaybe "",
            url
              <&> ( \url ->
                      [ fromMaybe "" (dir <&> comDirToText),
                        fromMaybe "" (reqTyp <&> requestTypeToText),
                        url
                      ]
                        <&> colorText Vivid Black
                        & T.intercalate "|"
                  )
              <&> (<> ")")
              <&> ("(" <>)
              & fromMaybe ""
          ]

formatTextWithErrorContext :: ErrorContext -> Text -> Text -> Text
formatTextWithErrorContext ctx errName errText = [qq|{ctxPart}{errNamePart}{errTextPart}|]
  where
    ctxPart = formatErrorContext ctx <&> (<> " > ") & fromMaybe ""
    errTextPart :: Text = bool [qq| : {errText}|] "" $ T.null errText
    errNamePart = colorText Vivid Blue errName

formatNonCriticalErrorWithoutContext :: NonCriticalErrorWithoutContext -> Text
formatNonCriticalErrorWithoutContext (PrimitiveTypeInRequestOrResponseBodyError dir _) = [qq|Only references (\$ref) and arrays are allowed in a JSON {comDirToText dir}.|]
formatNonCriticalErrorWithoutContext ForbiddenValidationError = "Use of validation in this place is not allowed."
formatNonCriticalErrorWithoutContext (DisallowedValidationError xs) = "This kind of validation with this type is not allowed: " <> valsText <> "."
  where
    valsText = xs <&> (\(n, v) -> n <> ": <" <> chopDownIfLong v <> ">") & T.intercalate ", "
    chopDownIfLong x = if T.length x > 35 then T.take 32 x <> "..." else x

formatAppError :: AppError -> [Text]
formatAppError (ParseError ctx x) = [formatTextWithErrorContext ctx "Parser" x]
formatAppError (ValidationError ctx x) = [formatTextWithErrorContext ctx "Validation" x]
formatAppError (NonCriticalAppError (NonCriticalError ctx x)) = [formatTextWithErrorContext ctx "Validation" $ formatNonCriticalErrorWithoutContext x]
formatAppError (GenerationError ctx x) = [formatTextWithErrorContext ctx "Generation" x]
formatAppError (ConfigError x) = ["Configuration: " <> x]
formatAppError (CrawlerError x) = ["Crawler: " <> x]
formatAppError (MultiError xs) = xs <&> formatAppError & join
formatAppError (ErrorAlreadyHandled x) = ["ErrorAlreadyHandled: "] <> formatAppError x
formatAppError ImpossibleError = ["ImpossibleError"]
formatAppError (PromotedWarning x) = [x]
formatAppError (IOError x) = ["IO: " <> x]

wasAppErrorHandled :: AppError -> Bool
wasAppErrorHandled (ErrorAlreadyHandled _) = True
wasAppErrorHandled _ = False

data TitleSource = TitleFromCmdArg | TitleFromSchemaField | TitleFromSchemaKey | TitleFromFileName deriving (Eq, Show, Data)

data AppWarningWithoutContext
  = MissingTitleWarning
  | MissingExampleWarning
  | MissingExampleOfStringWithPatternWarning
  | InheritingObjectWithoutOwnObjectWarning
  | UnsupportedResponseRefWarning
  | MultipleTitleSources [TitleSource]
  | UselessValidationWarning Text
  | DeprecationWarning Text
  | DemotedError NonCriticalErrorWithoutContext
  | CookieParameterWarning
  deriving (Eq, Show, Generic, Data, Typeable)

makePrisms ''AppWarningWithoutContext
makeFieldLabelsNoPrefix ''AppWarningWithoutContext

appWarningSuffix :: Text
appWarningSuffix = "Warning"

appWarningNames :: [Text]
appWarningNames = conNames (undefined :: AppWarningWithoutContext) <&> toS <&> stripSuffixIfPresent appWarningSuffix

isAppWarningName :: Text -> Bool
isAppWarningName = flip elem appWarningNames

getAppWarningName :: AppWarningWithoutContext -> Text
getAppWarningName = conNameOf >>> toS >>> stripSuffixIfPresent appWarningSuffix

formatAppWarningWithoutContext :: AppWarningWithoutContext -> Text
formatAppWarningWithoutContext MissingTitleWarning = "Missing title"
formatAppWarningWithoutContext MissingExampleWarning = "Missing example"
formatAppWarningWithoutContext MissingExampleOfStringWithPatternWarning = "Missing example of string with pattern"
formatAppWarningWithoutContext InheritingObjectWithoutOwnObjectWarning = "Inheritance used without its own object"
formatAppWarningWithoutContext UnsupportedResponseRefWarning = "$ref in response is not supported"
formatAppWarningWithoutContext (DeprecationWarning x) = "Deprecated: " <> x
formatAppWarningWithoutContext (UselessValidationWarning x) = "Useless validation: " <> x
formatAppWarningWithoutContext (MultipleTitleSources xs) =
  let src = xs <&> tshow & T.intercalate ", " in [qq|Multiple title/name sources ($src)|]
formatAppWarningWithoutContext (DemotedError e) = formatNonCriticalErrorWithoutContext e
formatAppWarningWithoutContext CookieParameterWarning = "Cookie parameter type encountered, no code will be generated for it."

data AppWarning = AppWarning ErrorContext AppWarningWithoutContext deriving (Eq, Show, Generic)

formatAppWarning :: AppWarning -> Text
formatAppWarning (AppWarning ctx warn) = formatTextWithErrorContext ctx (formatAppWarningWithoutContext warn) ""

getAppWarningWithoutContext :: AppWarning -> AppWarningWithoutContext
getAppWarningWithoutContext (AppWarning _ w) = w
