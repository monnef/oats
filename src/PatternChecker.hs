{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}

module PatternChecker where

import qualified Data.Text as T
import System.Exit (ExitCode (ExitSuccess))
import Utils

validateStringPatternAndExample :: Text -> Text -> IO (Either Text ())
validateStringPatternAndExample regex example = do
  let js = genPatternCheckJsCode regex example
  (out, err, exitCode) <- runCmd "node" ["-e", js] False "."
  let errRes =
        Left $
          [ [qq|Failed to run node to check example {genJsStringLiteral example} against regular expression {genJsStringLiteral regex}.|],
            [qq|Exit code: $exitCode|],
            [qq|Generated code: $js|],
            [qq|Output:|]
          ]
            <> out
            <> [ [qq|Error output:|]
               ]
            <> err
            & T.intercalate nl
  case (out, exitCode) of
    (["true"], ExitSuccess) -> return $ Right ()
    (["false"], ExitSuccess) -> return $ Left [qq|Example {genJsStringLiteral example} doesn't match given pattern {genJsStringLiteral regex}.|]
    (_, _) -> return errRes

genPatternCheckJsCode :: Text -> Text -> Text
genPatternCheckJsCode regex example =
  [qq|console.log(new RegExp({genJsStringLiteral regex}).test({genJsStringLiteral example}))|]
