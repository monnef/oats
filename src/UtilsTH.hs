{-# LANGUAGE OverloadedStrings #-}

module UtilsTH where

import Control.Arrow ((>>>))
import Control.Lens ((&))
import Data.Aeson.TH
  ( SumEncoding (..),
    constructorTagModifier,
    defaultOptions,
    fieldLabelModifier,
    sumEncoding,
  )
import Data.Aeson.Types (Options)
import Data.Char (toLower, toUpper)
import qualified Data.Text as T
import Data.Maybe (fromJust)

convertFieldName :: String -> String -> String
convertFieldName prefix x = x & drop (length prefix) & deCapitalizeWordStr

deCapitalizeWordStr :: String -> String
deCapitalizeWordStr "" = ""
deCapitalizeWordStr x = (x & head & toLower) : (x & tail)

capitalizeWordStr :: String -> String
capitalizeWordStr "" = ""
capitalizeWordStr x = (x & head & toUpper) : (x & tail)

convertIllegalFieldNames :: String -> String
convertIllegalFieldNames "typ" = "type"
convertIllegalFieldNames "ref" = "$ref"
convertIllegalFieldNames "inn" = "in"
convertIllegalFieldNames "comment" = "$comment"
convertIllegalFieldNames "xOatsTrimmedMinLength" = "x-oats-trimmedMinLength"
convertIllegalFieldNames "xOverrideNonRequiredRepresentation" = "x-overrideNonRequiredRepresentation"
convertIllegalFieldNames "xOatsOverrideNonRequiredRepresentation" = "x-oats-overrideNonRequiredRepresentation"
convertIllegalFieldNames "xMeta" = "x-meta"
convertIllegalFieldNames "xOatsMeta" = "x-oats-meta"
convertIllegalFieldNames x = x

derJsonOpts :: Options
derJsonOpts =
  defaultOptions
    { fieldLabelModifier = convertIllegalFieldNames,
      constructorTagModifier = deCapitalizeWordStr,
      sumEncoding = UntaggedValue
    }

derJsonOptsOfOAParameterIn :: Options
derJsonOptsOfOAParameterIn =
  defaultOptions
    { constructorTagModifier = deCapitalizeWordStr >>> T.pack >>> T.stripSuffix "ParameterInType" >>> fromJust >>> T.unpack,
      sumEncoding = UntaggedValue
    }
