{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}

module IgnoreRules (doesIgnoreRuleSelectorMatch, doesIgnoreRuleTargetMatch, applyIgnoreRules) where

import Data
import Data.Maybe (fromMaybe)
import Optics
import Optics.State.Operators
import Text.RE.PCRE.Text (RE, anyMatches, (*=~))
import Utils

regexMatchesMaybeText :: RE -> Maybe Text -> Bool
regexMatchesMaybeText r x = x <&> (*=~ r) <&> anyMatches & fromMaybe False

doesIgnoreRuleSelectorMatch :: IgnoreRuleSelector -> ErrorContext -> Bool
doesIgnoreRuleSelectorMatch IgnoreRuleSchemaByName {schemaNameRegex} ctx =
  regexMatchesMaybeText schemaNameRegex (ctx ^. #schemaName)
doesIgnoreRuleSelectorMatch IgnoreRuleEndpointByUrl {urlRegex} ErrorContext {endpointUrl} =
  regexMatchesMaybeText urlRegex endpointUrl
doesIgnoreRuleSelectorMatch IgnoreRuleEndpointByOperationId {operationIdRegex} ErrorContext {endpointOperationId} =
  regexMatchesMaybeText operationIdRegex endpointOperationId

doesIgnoreRuleTargetMatch :: IgnoreRuleTargets -> IgnoreRuleObject -> Bool
doesIgnoreRuleTargetMatch x (IRError e) = getNonCriticalErrorName (getNonCriticalErrorWithoutContext e) `elem` (x ^. #errors)
doesIgnoreRuleTargetMatch x (IRWarning w) = getAppWarningName (getAppWarningWithoutContext w) `elem` (x ^. #warnings)

doesMatchIgnoreRule :: IgnoreRule -> IgnoreRuleObject -> Bool
doesMatchIgnoreRule ir x = doesIgnoreRuleTargetMatch (ir ^. #targets) x && doesIgnoreRuleSelectorMatch (ir ^. #selector) (ctxFromIgnoreRuleObject x)

doesMatchAnyIgnoreRule :: [IgnoreRule] -> IgnoreRuleObject -> Bool
doesMatchAnyIgnoreRule irs x = any (`doesMatchIgnoreRule` x) irs

applyIgnoreRules :: [IgnoreRule] -> AppM ()
applyIgnoreRules rules = do
  #appWarnings %= filterOut (IRWarning >>> doesMatchAnyIgnoreRule rules)
  use #appErrors <&> filter (IRError >>> doesMatchAnyIgnoreRule rules) >>= mapM_ demoteError
