{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}
{-# LANGUAGE UndecidableInstances #-}

module Data
  ( module Data,
    module Data.Configuration,
    module Data.Errors,
    module Data.ReturnCodes,
    module Data.CommandLineArguments,
    module Data.Model,
    module Data.AppM,
    module Data.AppState,
  )
where

import Data.Aeson (Value)
import Data.Aeson.TH (deriveJSON)
import Data.AppM
import Data.AppState
import Data.CommandLineArguments
import Data.Configuration
import Data.Default (Default, def)
import Data.Errors
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromMaybe)
import Data.Model
import Data.ReturnCodes
import GHC.Generics (Generic)
import Optics
import Utils
import UtilsTH

data AppOperationMode = SimpleSchemaMode | BatchMode | SingleFileMode deriving (Eq, Show, Generic)

instance Default AppOperationMode where
  def = SimpleSchemaMode

data WorkContext = WorkContext
  { debugLog :: Text -> IO (),
    skipOnDryRunAndPrintAlways :: Text -> IO () -> IO (),
    skipOnDryRunAndPrintInVerbose :: Text -> IO () -> IO (),
    config :: Config,
    metas :: [OAMeta],
    mode :: AppOperationMode
  }

makeFieldLabelsNoPrefix ''WorkContext

instance Default WorkContext where
  def =
    WorkContext
      { debugLog = const skip,
        skipOnDryRunAndPrintAlways = const $ const skip,
        skipOnDryRunAndPrintInVerbose = const $ const skip,
        config = def,
        metas = [],
        mode = SimpleSchemaMode
      }

instance Show WorkContext where
  show WorkContext {..} = "WorkContext { config = " <> show config <> ", metas = " <> show metas <> ", mode = " <> show mode <> "}"

data CrawlResult = CrawlResult
  { sourceFiles :: [FilePath],
    customSchemaFiles :: [FilePath]
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''CrawlResult

deriveJSON derJsonOpts ''CrawlResult

mkErrCtxFromMeta :: OAMeta -> ErrorContext
mkErrCtxFromMeta OAMeta {..} = def & #unitName ?~ name & #fileName .~ path

mkErrCtxFromMetaOrDef :: Maybe OAMeta -> ErrorContext
mkErrCtxFromMetaOrDef = fmap mkErrCtxFromMeta >>> fromMaybe def

data RenderEndpointInfo = RenderEndpointInfo
  { url :: Text,
    urlWithParams :: Text,
    requestType :: RequestType,
    operationId :: Text,
    summary :: Maybe Text,
    description :: Maybe Text,
    requestTsType :: Maybe Text,
    requestSchema :: Maybe Text,
    responseTsType :: Maybe Text,
    responseSchema :: Maybe Text,
    queryParamsTypeDefinition :: Maybe Text,
    queryParamsTypeName :: Maybe Text,
    queryParamsSchemaName :: Maybe Text,
    queryParamsArgumentName :: Maybe Text,
    pathParamsTypeDefinition :: Maybe Text,
    pathParamsTypeName :: Maybe Text,
    pathParamsSchemaName :: Maybe Text,
    pathParamsArgumentName :: Maybe Text,
    preparedFunctionArguments :: Text,
    imports :: Text,
    dependencies :: [Text],
    expectedStatusCode :: Int,
    isResponseFileDownload :: Bool,
    isRequestFileUpload :: Bool,
    meta :: Maybe Value,
    permissions :: [Text]
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''RenderEndpointInfo
deriveJSON derJsonOpts ''RenderEndpointInfo

data CustomEndpointsOneFile = CustomEndpointsOneFile
  { imports :: Maybe Text,
    code :: Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''CustomEndpointsOneFile
deriveJSON derJsonOpts ''CustomEndpointsOneFile

data CustomEndpointsMainFile = CustomEndpointsMainFile
  { files :: M.HashMap Text CustomEndpointsOneFile
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''CustomEndpointsMainFile
deriveJSON derJsonOpts ''CustomEndpointsMainFile

data ApiInfo = ApiInfo
  { gitHash :: Maybe Text,
    gitHashShort :: Maybe Text,
    gitBranch :: Maybe Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''ApiInfo
deriveJSON derJsonOpts ''ApiInfo

data DocRenderContext = DocRenderContext
  { version :: Text,
    endpoints :: [RenderEndpointInfo],
    customEndpoints :: CustomEndpointsOneFile,
    imports :: Text,
    groupName :: Text,
    apiInfo :: Maybe ApiInfo,
    dateTime :: Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''DocRenderContext
deriveJSON derJsonOpts ''DocRenderContext

data DocRenderLimitsContext = DocRenderLimitsContext
  { schemaName :: Text,
    limitName :: Text,
    limitValue :: Text,
    isInclusive :: Bool
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''DocRenderLimitsContext
deriveJSON derJsonOpts ''DocRenderLimitsContext

data DocRenderLimitsCombinedContext = DocRenderLimitsCombinedContext
  { schemaName :: Text,
    limitMinValue :: Text,
    limitMaxValue :: Text,
    isInclusive :: Bool
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''DocRenderLimitsCombinedContext
deriveJSON derJsonOpts ''DocRenderLimitsCombinedContext

data GeneratedEndpointFile = GeneratedEndpointFile
  { wholeFilePath :: FilePath,
    content :: Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''GeneratedEndpointFile

data AfterFileWriteRenderContext = AfterFileWriteRenderContext
  { fileName :: Text,
    path :: Text,
    sourceApiFile :: Text
  }
  deriving (Eq, Show, Generic)

makeFieldLabelsNoPrefix ''AfterFileWriteRenderContext
deriveJSON derJsonOpts ''AfterFileWriteRenderContext

data TestRecord = TestRecord
  { a :: Text,
    b :: Maybe Int
  }

makeFieldLabelsNoPrefix ''TestRecord
