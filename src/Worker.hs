{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TupleSections #-}

module Worker where

import Control.Monad (unless, zipWithM)
import Control.Monad.Except (throwError)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Extra (whenJust)
import Crawler (crawl)
import Data
import Data.Default (def)
import Data.Either.Combinators (maybeToRight)
import Data.Either.Extra (mapLeft, maybeToEither)
import qualified Data.HashMap.Lazy as M
import Data.List (nub)
import Data.Maybe (catMaybes, fromMaybe, isJust, isNothing)
import Data.Tuple.Extra (fst3, uncurry3)
import DataProcessor (processHighLevelData, processSchemaData, rawPathsToRoute, rawTagToTag)
import Generator (generateEndpointsTypeScript, generateTypeScript)
import Optics
import Parser (parseCustomEndpoints, parseOneSchema, parseSingleFileApi)
import System.Directory (doesDirectoryExist)
import System.Directory.Extra (canonicalizePath, createDirectory, createDirectoryIfMissing, makeAbsolute, removeDirectoryRecursive)
import System.Exit (ExitCode (ExitSuccess))
import System.FilePath (makeRelative, replaceExtension, takeBaseName, takeDirectory, takeFileName, (</>))
import Text.RE.PCRE.Text (matched, (?=~))
import Utils

singleSchemaYamlToTypeScript :: AppOperationMode -> WorkContext -> Maybe Text -> Text -> AppM Text
singleSchemaYamlToTypeScript mode ctx fileName x =
  x & parseOneSchema errCtx >>= processSchemaData (ctx ^. #config) mode fileName Nothing >>= uncurry (generateTypeScript ctx)
  where
    errCtx = mkErrCtxFromFileNameOrDef fileName

createOARecordsForCustomSchema :: WorkContext -> Text -> Text -> Text -> (OA, OAMeta)
createOARecordsForCustomSchema ctx fileName tsFilePath code =
  ( OACustom {customSchemaCode = code},
    OAMeta
      { name = fileName & liftToText takeBaseName,
        path = Just $ makeRelative (ctx ^. #config % #customSchemasPath & fromMaybe "" & toS) (toS tsFilePath) & toS,
        dependencies = [],
        taggedUnionMapping = Nothing,
        example = Nothing,
        description = Just [qq|Custom schema from $tsFilePath|],
        comment = Nothing,
        isRecursive = Just False
      }
  )

addNameFromMetaToError :: OAMeta -> AppError -> AppError
addNameFromMetaToError meta (GenerationError ctx x) = GenerationError (ctx & #fileName ?~ (meta ^. #name)) x
addNameFromMetaToError _ x = x

-- | ctx -> [(fileName, yamlFilePath, yaml)] -> [(fileName, tsFilePath, ts)] -> ...
yamlsToTypeScripts :: AppOperationMode -> WorkContext -> [(Text, Text, Text)] -> [(Text, Text, Text)] -> AppM [Text]
yamlsToTypeScripts mode ctx xs customSchemas = do
  parsed :: [RawOA] <- mapM (\(fileName, _ {- yamlFilePath -}, yaml) -> parseOneSchema (mkErrCtxFromFileName fileName) yaml) xs
  processedYamls <- zipWithM (\x -> processSchemaData (ctx ^. #config) mode x Nothing) (xs <&> fst3 <&> Just) parsed
  let processedCustomSchemas = customSchemas <&> uncurry3 (createOARecordsForCustomSchema ctx)
  let processedLow = processedYamls <> processedCustomSchemas
  let ctxLow = ctx & #metas .~ (processedLow <&> snd)
  (ctxHigh, processedHigh, _, _) <- processHighLevelData (ctxLow, processedLow, mempty, mempty)
  let gen oa meta = generateTypeScript ctxHigh oa meta & mapAppErr (addNameFromMetaToError meta)
  mapM (uncurry gen) processedHigh

mergeRawOAFile :: (RawOAFile, Text) -> RawOAFile -> AppM RawOAFile
mergeRawOAFile (toMerge, _ {- toMergeFileName -}) x = do
  -- TODO: check conflicts?
  let components = RawOAComponents {schemas = M.union (toMerge ^. #components % #schemas) (x ^. #components % #schemas)}
  let tags = Just $ fromMaybe [] (x ^. #tags) <> fromMaybe [] (toMerge ^. #tags) & nub
  let paths = Just $ M.union (fromMaybe M.empty (toMerge ^. #paths)) (fromMaybe M.empty (x ^. #paths))
  return RawOAFile {..}

validateTagsInRawFile :: RawOAFile -> AppM ()
validateTagsInRawFile x = forM_ (x ^. #tags & fromMaybe []) $ \t -> do
  when (t ^. #description & isNothing) $ throwError $ ValidationError def [i|Tag #{t ^. #name & tshow} is missing description|]

singleFileApiYamlToTypeScripts :: AppOperationMode -> WorkContext -> Maybe Text -> Text -> [(Text, Text, Text)] -> Maybe CustomEndpointsMainFile -> [(Text, Text)] -> AppM [(FilePath, FilePath, Text)]
singleFileApiYamlToTypeScripts mode ctx fileName inputYaml customSchemas customEndpoints toMerge = do
  parsedWithoutToMerge :: RawOAFile <- parseSingleFileApi (mkErrCtxFromFileNameOrDef fileName) inputYaml
  parseOfToMerge :: [(RawOAFile, Text)] <- forM toMerge $ \(fileName, inputYaml) -> do
    parse <- parseSingleFileApi (mkErrCtxFromFileNameOrDef $ Just fileName) inputYaml
    return (parse, fileName)
  parsed <- foldl (\acc x -> acc >>= mergeRawOAFile x) (return parsedWithoutToMerge) parseOfToMerge
  validateTagsInRawFile parsed
  oaTags <- parsed ^. #tags & fromMaybe [] & mapM rawTagToTag
  let inputData :: [(Maybe Text, RawOA)] = parsed ^. #components % #schemas & M.toList <&> _1 %~ Just & filter (not . isIgnored)
  processedSchemas <- mapM (processSchemaData (ctx ^. #config) mode fileName & uncurry) inputData
  let processedCustomSchemas = customSchemas <&> uncurry3 (createOARecordsForCustomSchema ctx)
  let processedCustomSchemasWithFileNames = processedCustomSchemas <&> \(oa, meta) -> (meta ^. #name & toS, oa, meta)
  let processedSchemasWithFileNames = processedSchemas <&> \(oa, meta) -> (fromMaybe "???" fileName & toS, oa, meta)
  let processedLow = processedSchemasWithFileNames <> processedCustomSchemasWithFileNames
  let ctxLow = ctx & #metas .~ (processedLow ^.. each % _3)
  let (processedLowWithoutFileNames, processedLowFileNames) = processedLow <&> (\(name, oa, meta) -> ((oa, meta), name)) & unzip
  let filterByIgnoreEndpointsByPathRegex =
        case ctx ^. #config % #ignoreEndpointsByPathRegex of
          Just x -> toS >>> (?=~ x) >>> matched >>> not
          Nothing -> const True
  oaRoutesLow <-
    parsed ^. #paths & fromMaybe (M.fromList [])
      & M.toList
      & filter (fst >>> filterByIgnoreEndpointsByPathRegex)
      & mapM (rawPathsToRoute (ctx ^. #config) fileName) <&> M.fromList
  (ctxHigh, processedHighWithoutFileNames, oaRoutesHigh, _oaTagsHigh) <- processHighLevelData (ctxLow, processedLowWithoutFileNames, oaRoutesLow, oaTags)
  let processedHigh = zip processedLowFileNames processedHighWithoutFileNames <&> (\(name, (oa, meta)) -> (name, oa, meta))
  resSchemas <- mapM (uncurry3 $ gen ctxHigh) processedHigh
  liftIO $ (ctx ^. #debugLog) ("oaRoutes: " <> pShow oaRoutesHigh)
  epTpl <- case (ctx ^. #config % #endpointsTemplate, ctx ^. #config % #endpointsTemplateFile) of
    (Just tpl, Nothing) -> return $ Just tpl
    (Nothing, Just tplFile) -> do
      r <- tReadFileEither tplFile & liftIO
      r & mapLeft (\x -> GenerationError def [qq|Reading of templates file failed: $x|]) & liftEitherToAppM <&> Just
    (Just _, Just _) -> throwError $ ConfigError [qq|endpointsTemplate and endpointsTemplateFile are not allowed to be both filled.|]
    (Nothing, Nothing) -> return Nothing
  resRoutes <- case (ctx ^. #config % #endpointsGenerationMode, epTpl) of
    (Just egm, Just tpl) -> generateEndpointsTypeScript ctxHigh egm tpl oaRoutesHigh customEndpoints
    (Just egm, Nothing) -> throwError $ ConfigError [qq|endpointsGenerationMode is $egm, but endpoints template is missing.|]
    _ -> return []
  liftIO $ (ctx ^. #debugLog) ("resRoutes: " <> pShow resRoutes)
  return $ resSchemas <> (resRoutes <&> genEndpointToTriple)
  where
    gen :: WorkContext -> FilePath -> OA -> OAMeta -> AppM (FilePath, FilePath, Text)
    gen newCtx fn oa meta = generateTypeScript newCtx oa meta & mapAppErr (addNameFromMetaToError meta) <&> (fn,meta ^. #name & toS,)
    isIgnored :: (Maybe Text, RawOA) -> Bool
    isIgnored (nameMay, _) =
      let cond (name :: Text) rgx = name & toS & (?=~ rgx) & matched
       in (cond <$> nameMay <*> ctx ^. #config % #ignoreComponentsRegex) & fromMaybe False
    genEndpointToTriple GeneratedEndpointFile {..} = ("?", wholeFilePath, content)

---

workWithStdInOut :: WorkContext -> AppM ()
workWithStdInOut ctx@WorkContext {..} = do
  when (isJust $ config ^. #outPath) $
    throwError $ ConfigError "Output path must not be specified when using single-file mode."
  input <- liftIO $ getContents <&> toS
  result <- singleSchemaYamlToTypeScript SimpleSchemaMode ctx Nothing input
  liftIO $ debugLog $ result & pShow
  liftIO $ unless (ctx ^. #config % #quiet) $ tPutStrLn result

writeResult :: WorkContext -> FilePath -> (FilePath, FilePath, Either AppError Text) -> AppM ()
writeResult WorkContext {..} outDir res@(apiFileName, fileNameWithDir, Right ts) = do
  absOutPath <- liftIO $ makeAbsolute outDir <&> toS
  let outFilePath = outDir </> replaceExtension fileNameWithDir ".ts"
  liftIO $
    debugLog $
      "writeResult: outDir = " <> toS outDir <> " (" <> absOutPath <> "), " <> toS outFilePath <> nl <> pShow res
  liftIO $ createDirectoryIfMissing True $ takeDirectory outFilePath
  liftIO $ tWriteFile (toS outFilePath) ts
  whenJust (config ^. #afterFileWriteCommand) $ \rawCmd -> do
    let rCtx = AfterFileWriteRenderContext {path = toS outFilePath, sourceApiFile = toS apiFileName, fileName = toS $ takeFileName outFilePath}
    case renderTemplate rawCmd rCtx of
      Right cmd -> do
        (out, err, exitCode) <- liftIO $ runCmd "sh" ["-c", cmd] True "."
        when (exitCode /= ExitSuccess) $ do
          let wholeOutput = joinNl [joinNl out, joinNl err]
          throwError $ IOError [qq|afterFileWriteCommand failed with {exitCode}{nl}{wholeOutput}|]
      Left err -> throwError $ ConfigError $ "Failed to compile afterFileWriteCommand template: " <> tshow err
writeResult _ _ _ = skip

showResult :: WorkContext -> (FilePath, FilePath, Either AppError Text) -> AppM ()
showResult WorkContext {..} res@(justFileName, fileName, Right ts) = do
  liftIO $ debugLog $ toS justFileName <> ": " <> pShow res
  unless (config ^. #quiet) $
    liftIO $ do
      tPutStrLn $ ">> " <> toS fileName <> ":"
      tPutStrLn ts
showResult _ _ = skip

processErrors :: [(FilePath, FilePath, Either AppError Text)] -> AppM ValidatedResults
processErrors results = do
  let errList = results <&> toError & catMaybes
  if null errList
    then return $ ValidatedResults results
    else do
      Left (MultiError $ errList <&> fileErrTupleToErrWithCtx) & liftEitherToAppM
  where
    toError (_, _, Right _) = Nothing
    toError (_, fileName, Left err) = Just (fileName, err)
    fileErrTupleToErrWithCtx (fn :: FilePath, err :: AppError) = updateErrorContextInAppError (#fileName ?~ toS fn) err

newtype ValidatedResults = ValidatedResults [(FilePath, FilePath, Either AppError Text)]

validateOutPathForRemoval :: Text -> AppM Text
validateOutPathForRemoval path = do
  cp <- toS path & canonicalizePath & liftIO <&> toS
  when (cp == "/") $ throwError $ IOError "Cannot delete output directory which is root of filesystem"
  return cp

doCleaningOfOutputDir :: WorkContext -> Text -> AppM ()
doCleaningOfOutputDir ctx outPath = do
  path <- validateOutPathForRemoval outPath <&> toS
  let dryRunMessage = "Removing and recreating directory '" <> outPath <> "' (canonical path is '" <> toS path <> "')"
  dirExists <- liftIO $ doesDirectoryExist path
  when dirExists $
    liftIO $
      (ctx ^. #skipOnDryRunAndPrintInVerbose) dryRunMessage $ do
        removeDirectoryRecursive path
        createDirectory path

processResults :: WorkContext -> ValidatedResults -> AppM ()
processResults ctx@WorkContext {..} (ValidatedResults results) = do
  prepareOutDirIfNeeded
  forM_ results processResult
  where
    prepareOutDirIfNeeded =
      case config ^. #outPath of
        Just out -> when (config ^. #cleanOutputDirectory) $ doCleaningOfOutputDir ctx out
        Nothing -> return ()
    processResult =
      case config ^. #outPath of
        Just out -> writeResult ctx (toS out)
        Nothing -> showResult ctx

readCustomSchemas :: FilePath -> [FilePath] -> AppM [(Text, Text, Text)]
readCustomSchemas customSchemaDir customSchemaFileNames =
  forM customSchemaFileNames $ \fileName -> do
    let tsFilePath = customSchemaDir </> fileName & toS & backSlashToSlash
    ts :: Text <-
      tReadFileEither (toS tsFilePath)
        <&> mapLeft (\x -> ParseError def [qq|Failed to read custom schema $fileName: $x|])
        <&> liftEitherToAppM
        & liftIO
        & join
    return (toS fileName, tsFilePath, ts)

getCustomSchemaArgs :: WorkContext -> CrawlResult -> (FilePath, [FilePath])
getCustomSchemaArgs WorkContext {..} crawlRes =
  config ^. #customSchemasPath & fromMaybe "MISSING" & \csp -> (toS csp, crawlRes ^. #customSchemaFiles)

workWithApiPath :: WorkContext -> AppM ()
workWithApiPath ctx@WorkContext {..} = do
  crawlRes <- crawl ctx
  apiPath' <- config ^. #apiPath & maybeToRight ImpossibleError & liftEitherToAppM
  liftIO $ debugLog $ "crawlRes = " <> pShow crawlRes
  results <-
    processFiles
      (toS apiPath', crawlRes ^. #sourceFiles)
      (getCustomSchemaArgs ctx crawlRes)
  results & processErrors >>= processResults ctx
  where
    -- TODO: improve types (return and intermediate ones, e.g. yamls)
    processFiles :: (FilePath, [FilePath]) -> (FilePath, [FilePath]) -> AppM [(FilePath, FilePath, Either AppError Text)]
    processFiles (apiDir, fileNames) (customSchemaDir, customSchemaFileNames) = do
      yamls <- forM fileNames $ \fileName -> do
        let yamlFilePath = apiDir </> fileName
        yaml <-
          tReadFileEither (toS yamlFilePath)
            <&> mapLeft (\x -> GenerationError def [qq|Falied to read YAML API file $yamlFilePath: $x|])
            & liftEitherInIOToAppM
        return (toS fileName, toS yamlFilePath, yaml)
      customSchemas <- readCustomSchemas customSchemaDir customSchemaFileNames
      liftIO $ debugLog $ "customSchemas = " <> pShow customSchemas
      tss <- yamlsToTypeScripts BatchMode ctx yamls customSchemas
      return $ tss `zip` (yamls <> customSchemas) <&> \(ts, (fileName, yamlFilePath, _)) -> (toS yamlFilePath, toS fileName, return ts)

workWithApiFile :: WorkContext -> AppM ()
workWithApiFile ctx@WorkContext {..} = do
  crawlRes <- crawl ctx
  customSchemas <- (readCustomSchemas & uncurry) (getCustomSchemaArgs ctx crawlRes)
  liftIO $ debugLog $ "customSchemas = " <> pShow customSchemas
  yamlFileName <- config ^. #apiFile & maybeToEither (ConfigError "Expected apiFile option is missing") & liftEitherToAppM
  yaml :: Text <-
    tReadFileEither yamlFileName
      <&> mapLeft (\x -> GenerationError def [qq|Failed to read YAML API file $yamlFileName: $x|])
      & liftEitherInIOToAppM
  toMergeYamls :: [(Text, Text)] <- forM (config ^. #apiFilesToMerge <&> (^. #path)) $ \filePath -> do
    c <-
      tReadFileEither filePath
        <&> mapLeft (\x -> GenerationError def [qq|Failed to read YAML API file $filePath: $x|])
        & liftEitherInIOToAppM
    return (filePath, c)
  customEndpoints <-
    case config ^. #customEndpointsFile of
      Nothing -> return Nothing
      Just ceFileName ->
        tReadFileEither ceFileName
          <&> mapLeft (\x -> GenerationError def [qq|Failed to read custom endpoints file $ceFileName: $x|])
          & liftEitherInIOToAppM
          >>= parseCustomEndpoints (mkErrCtxFromFileName ceFileName)
          <&> Just
  -- TODO: improve error handling (don't halt on a first error; but collect all errors and show them)
  results <- singleFileApiYamlToTypeScripts SingleFileMode ctx (Just yamlFileName) yaml customSchemas customEndpoints toMergeYamls
  results <&> (_3 %~ Right) & processErrors >>= processResults ctx
