{-# LANGUAGE OverloadedStrings #-}

module RangeUtils where

import Data.Decimal

data RangeLimit = Exclusive | Inclusive deriving (Eq, Show)

isExclusiveToRangeLimit :: Bool -> RangeLimit
isExclusiveToRangeLimit True = Exclusive
isExclusiveToRangeLimit False = Inclusive

isExclusive :: RangeLimit -> Bool
isExclusive Exclusive = True
isExclusive Inclusive = False

data MinOrMax = Min | Max deriving (Eq, Show)

calcLimitVal :: MinOrMax -> Double -> (RangeLimit, RangeLimit) -> Double -> Double
calcLimitVal minOrMax multipleOf (fromLimit, toLimit) value =
  res
  where
    decPrecision = 15
    toDec = realFracToDecimal decPrecision
    val = toDec value
    resDec' =
      if fromLimit == toLimit
        then val
        else (case minOrMax of Max -> (-); Min -> (+)) val (toDec multipleOf)
    res = (realToFrac :: Decimal -> Double) resDec'
