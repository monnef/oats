[[_TOC_]]

# Development

## Tests

```sh
$ ./run_all_tests.sh
```

## Contributing

Please consult with the maintainer before implementing new features or doing significant changes.

Any changes or new features should be covered by some tests. But usually not by all kinds (unit/integration tests in `/test`, tests of validation and endpoints in `/ts`, output/formatting/parameters in `/e2e`). If you are unsure I would recommend asking.

All your code has to be free of copyright, you must explicitly do so in the PR (e.g. use a phrase like "All code has been written by me, and I am releasing it under [unlicense](https://unlicense.org).").
