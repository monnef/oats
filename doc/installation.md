[[_TOC_]]

# Docker - automatic (recommended)

See [oats-docker](https://www.npmjs.com/package/oats-docker) for more information.

# Installation from sources

## Requirements

* [stack](http://haskellstack.org)

## Install to project directory (local)

```sh
$ npm i -D oats-hs
```

Then you can use `oats` in your tasks in `package.json` (or run directly from `node_modules/.bin/oats`).
If you are using CI and aren't running API generation there, you probably want to move `oats` from `devDependencies` to `optionalDependencies` (so `npm i`/`npm ci` won't fail when `stack` is missing).

```json
{
  ...
  "scripts": {
    ...
    "oats": "oats",
    "oats-clip": "oats-clip",
    "gen-api": "run-s gen-api:clean oats",
    "gen-api:clean": "rimraf src/api/gen",
  },
  ...
  "optionalDependencies": {
    "oats-hs": "0.10.0"
  }
}
```
Example above uses packages `rimraf` and `npm-run-all` in `scripts`.

## Install to stack bin directory (global)

This way is usually used for developing `oats`.
When only using `oats`, it is recommended to use locally-installed instances via `npm`, see [Install to project directory](#install-to-project-directory-local).

Clone this repository and navigate inside its directory.

Install it:

```sh
$ stack install
```

First run will take a long time to get and compile all libraries, subsequent builds will be much faster.

Make sure you have bin directory to which stack installs binaries in `PATH` (e.g. on Linux it's `~/.local/bin`).

# Docker - manual

Linux amd64 builds are published on [https://hub.docker.com/r/mon7/oats/tags](https://hub.docker.com/r/mon7/oats/tags).
These images can be used for example in CI (on GitHub, BitBucket, GitLab, etc.) or locally when working on a front-end.

## Example of local use

All commands are run from `front-end` directory.

### Setup

```sh
$ sudo docker pull mon7/oats:v0.10.0
```

### Context

```sh
$ tree ..
..
├── back-end
│   └── openapi.yaml
└── front-end
    └── oats.yaml
$ cat oats.yaml
apiFile: ../back-end/openapi.yaml
```

### Usage

#### Temporary container

```sh
$ sudo docker run -it --rm -v "$(pwd)"/..:/data mon7/oats:v0.10.0 -c /data/front-end/oats.yaml
Finished with no warnings and no errors in 0.00s.
```

#### Persistent container

If you want to speed it up a bit, you can use persistent container by replacing `--rm` with `--name` and then running the existing container.

Create a container specific for a project from the `front-end` directory:
```sh
$ sudo docker run -it --name projectA-oats -v "$(pwd)"/..:/data mon7/oats:v0.10.0 -c /data/front-end/oats.yaml
Finished with no warnings and no errors in 0.00s.
```
Then you can run it from any directory:
```sh
$ sudo docker start -a projectA-oats
Finished with no warnings and no errors in 0.00s.
```
