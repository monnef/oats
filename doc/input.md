[[_TOC_]]

# Input

`oats` accepts only limited OpenAPI YAML (otherwise generation of TypeScript code would be either impossible or result would be very hard to use, bordering with useless).

## Input modes

### Simple mode

It is the default mode. *Simple mode* accepts input from stdin and typically produces output to stdout. It doesn't support generation of imports, nor other features which depend on having access to other schemas (e.g. tagged unions described via `discriminator.mapping`).

It can be used as a simple tool to quickly convert one OpenAPI YAML schema to a partial schema + type TypeScript code. Example with a clipboard:

```sh
$ clippaste | oats --title TODO | clipcopy
```

### Batch mode

A mode most common when OpenAPI schemas are written by hand - each schema corresponds to one file. It is enabled by passing `-a,--api PATH` argument or in a configuration file using `apiPath`.

```sh
$ tree ts/api
ts/api
├── Account.yaml
├── Basket.yaml
├── ExactPercentage.yaml
├── MultipleSameRefs.yaml
├── NativeDecimal.yaml
├── ObjWithObjArr.yaml
├── pet
│   ├── Cat.yaml
│   ├── Dog.yaml
│   ├── PetBase.yaml
│   └── Pet.yaml
├── SingleMemberUnion.yaml
└── user
    ├── Age.yaml
    ├── Email.yaml
    └── Name.yaml
```

You can inspect an example of it in [ts/api](ts/api) (click on `</>` icon to see source code) with configuration being [ts/oats.yaml](ts/oats.yaml).

### Single file mode

The mode accepts one OpenAPI YAML file with a complete API. This mode is typically used when an API is generated from a back-end. It can be enabled by passing `-f,--api-file PATH` argument or from a configuration file by `apiFile` field.

You can see example of it in [ts/api-singlefile/openapi.yaml](ts/api-singlefile/openapi.yaml) with configuration being [ts/oats-singlefile.yaml](ts/oats-singlefile.yaml).

Several other YAML files can be merged before processing via `apiFilesToMerge` option in a configuration file. It can be useful when a front-end is using multiple back-ends. Example of use is available in [ts/oats-singlefile.yaml](ts/oats-singlefile.yaml).

## Custom schemas

To override a resulting TypeScript file with other one, you can use `customSchemasPath` in a configuration file. Example of usage can be seen in [ts/oats.yaml](ts/oats.yaml) (custom `DateTime`).

Please note that replacing a generated file with custom one does not imply skipping the input file from processing. If you want to ignore files or schemas please see the [ignoring](#ignoring) section.

## Built-in custom validators

### `x-oats-trimmedMinLength`

Similar to `minLength`, but `trim`  is called on the string before length comparison.

```yaml
TrimmedMinLength:
  type: string
  x-oats-trimmedMinLength: 2
  example: ab
```

will generate code similar to this

```ts
// Example: "ab"

export const trimmedMinLengthTrimmedMinLength = 2;

export const TrimmedMinLengthSchema = t.refinement(t.string, n => n.trim().length >= trimmedMinLengthTrimmedMinLength, 'TrimmedMinLength');

export type TrimmedMinLength = string;
```

## Ignoring

To ignore a file or schema means to omit it from processing and later phases. In case of a batch mode, files are also skipped from reading and parsing phases. In the case of a single file mode oats won't omit any schemas from parsing, an input document must be a valid YAML and OpenAPI.

### Ignore files

Input files in a batch mode can be ignored via `ignoreFilesRegex` field in a configuration file.

### Ignore schemas

To ignore a schema in a single file mode you can use `ignoreComponentsRegex` field in a configuration file.

## Warnings

A warning doesn't halt a generation of schemas and won't result in an error code returned upon exiting.

```
[WARN] Missing title @ api-singlefile/openapi.yaml#Ship
```

| Name                               | Description |
| ---------------------------------- | ----------- |
| `MissingTitle`                     | Universal way of getting names of io-ts schemas and types is to use `title` field in OA schema. The field is not required in some modes. |
| `MissingExample`                   | `example` fields are useful for mock servers and are copied to generated files, serving as a documentation. |
| `InheritingObjectWithoutOwnObject` | Reported when encountered an object schema "inheriting" from some other schema, but is missing own object definition, thus being just an alias. |

You can tell oats to print a list of all possible warnings via `--print-warnings-list` switch.

### Ignoring warnings

You can use `ignoreWarnings` field in a configuration file to suppress specific warnings. It's usually not recommended doing so, but in some cases it may be a good thing to do (e.g. disable `MissingTitle` in a single file mode when OA API generator is not capable of outputting them, oats is able to use keys in `components.schemas` from the schema file as titles).

```yaml
ignoreWarnings:
  - MissingTitle
  - MissingExample
```

### Promoting warnings to errors

Particularly useful when running automatically - checking a generated OpenAPI file. Warnings listed in a configuration file in `promoteWarningsToErrors` field will cause an error (schema generation may or may not occur, process will return an error code).

```yaml
promoteWarningsToErrors:
  - MissingExample
```

## Errors

### Ignoring errors

Majority of errors are critical and always lead to termination. A small group of errors are "non-critical errors" and can be demoted to warnings via `ignoreErrors` (global) or [ignoreRules](#ignore-rules) (more granular) in a configuration file.

## Ignore rules

A configuration option `ignoreRules` allows ignoring of warnings and demotion of errors to warnings. It's an array of objects. Those objects contain either `errors` array with names of errors or `warnings` array with names of warnings. The second part is a targeting field which is one of `operationId` (endpoints), `url` (endpoints) or `schemaName` (schemas). Values are strings interpreted as a regular expression.

You can get a list of names of warnings and errors by using a command line arguments `--print-warnings-list` and `--print-errors-list`, respectively.

### Ignore rules example

Let's consider a configuration to contain:
```yaml
ignoreRules:
  - errors: [PrimitiveTypeInRequestOrResponseBody]
    operationId: '^(getUsers|deleteProduct)$'
  - errors: [PrimitiveTypeInRequestOrResponseBody]
    url: '^/some/things/'
  - warnings: [MissingTitle, MissingExample]
    schemaName: 'Test$'
```

The first item instructs oats to demote `PrimitiveTypeInRequestOrResponseBody` error for endpoints with names `getUsers` and `deleteProduct`.

The second item tells oats to demote `PrimitiveTypeInRequestOrResponseBody` error for endpoints with url (path) starting with `/some/things/`.

The third item suppresses warnings `MissingTitle` and `MissingExample` for all schemas which end with `Test`.

## Schema translation

Enabled via `schemaTranslation` field in a configuration.

```yaml
schemaTranslation: true
```

When this option is enabled, selected schema names in `$ref` will be translated to their primitive counterparts.

| Input schema name | Output schema in generated file |
|-------------------|---------------------------------|
| `Boolean` | `t.boolean` |
| `Double` | `t.number` |
| `Int32` | `t.Integer` |
| `Int32Array` | `t.array(t.Integer)` |
| `Int64` | `t.Integer` |
| `Int64Array` | `t.array(t.Integer)` |

A list of all default schema translations can be viewed by passing oats `--print-default-schema-translations` switch.

## Endpoints Generation
Generation of code for endpoints can be enabled by setting `endpointsGenerationMode` in configuration file to `allEndpointsInOneFile`.

