[[_TOC_]]

# Output

## Output modes

### Standard output

A default output mode. Probably only useful in a single file input mode.

### Output directory

Can be specified via `-o,--out PATH` argument or `outPath` in a configuration file.

### Cleaning of output directory

Can be enabled via `cleanOutputDirectory: yes` in a configuration file.
You can use `--dry-run` to see which changes will be made.

## Non-required fields

Configurable via flag `--non-required-as-undefined` or `nonRequiredRepresentation` in a configuration file.

### Null

The default option. Can be explicitly enabled by setting `nonRequiredRepresentation` to `nullAsNonRequired` in a configuration file.

```ts
const requiredPart = t.interface({
  createdDate: t.union([DateTimeSchema, t.null]),
  email: t.union([EmailSchema, t.null]),
});

export const AccountSchema = t.exact(requiredPart);
```

### Undefined

> ⚠️ Using `undefined` is **not recommended**, since it is less safe than `null`.

Can be enabled by using `--non-required-as-undefined` flag or setting `nonRequiredRepresentation` to `undefinedAsNonRequired` in a configuration file.

```ts
const optionalPart = t.partial({
  createdDate: DateTimeSchema,
  email: EmailSchema,
});

const requiredPart = t.interface({
});

export const AccountSchema = t.exact(t.intersection([optionalPart, requiredPart]));
```

### Overriding

It is possible to override a non-required mode just for one schema (object) via `x-overrideNonRequiredRepresentation` option:

```yaml
title: User
x-overrideNonRequiredRepresentation: undefinedAsNonRequired
properties:
  id:
    type: integer
```

## String patterns

Relevant part of configuration:
```yaml
stringPatternImport: "import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';"
stringPatternSchemaGenerator: StringMatchingRegularExpressionSchema
```

Example description in OpenAPI:
```yaml
title: Ico
type: string
pattern: '^[0-9]{8}$'
example: 27733772
```

results in

```ts
import * as t from 'io-ts';
import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';

// Example: "27733772"

export const icoRegexGen = (): RegExp => new RegExp("^[0-9]{8}$");

export const IcoSchema = StringMatchingRegularExpressionSchema<Ico>(icoRegexGen());

export type Ico = string;
```

Examples of a string schemas with patterns are also validated by oats using `node`.
If you want to turn this validation off, you can do so by adding `noPatternValidationWithExample` to a configuration file and setting it to `true`.

## API module

`oats` only supports one API module which is used for all imports of dependant schemas (so no relative imports). This setup is usually accomplished via `resolve.alias` in webpack or `compilerOptions.paths` in TypeScript compiler.

In the example bellow the `apiModule` (in a configuration file) is set to `generated`.

```ts
// @ts-ignore

import * as t from 'io-ts';

import { CatSchema } from 'generated/pet/Cat';
import { DogSchema } from 'generated/pet/Dog';

export const PetSchema = t.union([DogSchema, CatSchema]);

export type Pet = t.TypeOf<typeof PetSchema>;
```

## Excess

`exact` in io-ts has changed and in newer versions stopped checking excessive fields. If you want this old behaviour which is more suitable for validation, you can use `excess` (quite good implementation is available [here](https://github.com/gcanti/io-ts/issues/322#issuecomment-584658211)). In a configuration file enable `useExcess` and set an import code by `excessImport`.

```yaml
useExcess: true
excessImport: "import {excess} from 'utils/excess';"
```


## Header

A short code snippet can be inserted in every generated file. It is defined in a configuration file via `header` field.

It is recommended (especially if you don't work alone) to add a comment mentioning a file was automatically generated, so it should not be updated manually.

### Template variables

| Name      | Meaning                                       | Example   |
|-----------|-----------------------------------------------|-----------|
| `version` | version of oats used for generating this file | `0.6.1`   |

### Template language

Variables are enclosed in `{{` and `}}`. For more details, please see the documentation of [ginger](https://ginger.tobiasdammers.nl/guide/).

### Example

Having in a configuration file

```yaml
header: "export const oatsVersion = '{{ version }}';\n// @ts-ignore"
```

results in

```ts
export const oatsVersion = '0.6.1';
// @ts-ignore
...
```

## Property name mapping

An automatic field renaming can be set up via `propertyNameMap` in a configuration file.

```yaml
propertyNameMap:
  testName1: newName1
```

The example above will to lead to renaming of every property `testName1` (from OpenAPI schema file) to `newName1` (in generated io-ts schemas).

Consider this example input OA schema:

```yaml
type: object
properties:
  testName1:
    type: string
```

when we use the configuration above, it will lead to the following io-ts schema:

```ts
import * as t from 'io-ts';


const requiredPart = t.interface({
  newName1: t.union([t.string, t.null]),
});

export const PropertyNameMapTestSchema = t.exact(requiredPart);

export interface PropertyNameMapTest extends t.TypeOf<typeof PropertyNameMapTestSchema> {}
```

## Number Min/Max Limits

Emitting of recalculated min, max and combined constants for number types.
Example when using the default template and mode:

```ts
export const decimal20MinimumInclusive = -99.0;
export const decimal20MaximumInclusive = 99.0;
export const decimal20MinMaxInclusive: [number | null, number | null] = [-99.0, 99.0];
```

Related configuration fields:

| Name                               | Meaning                                                      | Example     |
|------------------------------------|--------------------------------------------------------------|-------------|
| numberMinMaxLimitsEnabled          | Enable generation of recalculated min/max constants          | `yes`       |
| numberMinMaxLimitsTemplate         | Template, see all variables in example bellow                | 
| numberMinMaxLimitsMode             | `inclusive` (default), `exclusive` or `both`                 | `inclusive` |
| numberMinMaxLimitsCombinedEnabled  | Enable generation of recalculated combined min max constants | `yes`       |
| numberMinMaxLimitsCombinedTemplate | Template, see all variables in example bellow                |
| numberMinMaxLimitsCombinedMode     | `inclusive` (default), `exclusive` or `both`                 | `inclusive` |

### numberMinMaxLimitsTemplate
```
export const Validation{{schemaName|toPascal()}}{{limitName|toPascal()}}{{isInclusive ? \"Incl\" : \"Excl\"}} = {{limitValue}};
```

### numberMinMaxLimitsCombinedTemplate
```
export const {{schemaName|toCamel()}}MinMax{{isInclusive ? 'Inclusive' : 'Exclusive'}}: [number | null, number | null] = [{{limitMinValue}}, {{limitMaxValue}}];
```

## Endpoints Generation

Working examples are at [ts](ts) directory ([configuration file](ts/oats-singlefile.yaml), [template](ts/api-singlefile/endpointsTemplateFile.ts)).

### Configuration

Fields in configuration file related to endpoints generation:

| Name      | Meaning                                       | Example   |
|-----------|-----------------------------------------------|-----------|
| `endpointsTemplate` | A code template for a generation of an endpoint file. | |
| `endpointsTemplateFile` | A path to a file containing a template for a generation of an endpoint file. | |
| `emptyRequestSchema` | A schema used when a request body is missing. | `t.void` |
| `emptyResponseType` | A type used when a response body is missing. | `EmptyString` |
| `emptyResponseSchema` | A schema used when a response body is missing. | `EmptyStringSchema` |
| `ignoreEndpointsByPathRegex` | When this regular expression matches, related endpoint(s) will be ignored. | `^/test/` |
| `customEndpointsFile` | A path to a YAML file describing custom endpoints per generated endpoints file. Data will be exposed via `customEndpoints` variable (default name of single endpoints file is `Api`, see [Custom endpoints](#custom-endpoints)). | `src/api/custom-endpoints.yaml` |
| `downloadFileResponseSchema` | Schema to use for download file endpoints. | `ArrayBufferSchema` |
| `downloadFileResponseType` | Type to use for download file endpoints. | `ArrayBuffer` |
| `pathParamsParameterName` | Name of pathParams parameter of endpoint method/function. Is used for generation of `urlWithParams`. | `pathParams` |


### DocRenderContext

Template variables related to endpoints:

| Name              | Type                         | Meaning                                                                        | Example   |
|-------------------|------------------------------|--------------------------------------------------------------------------------|-----------|
| `customEndpoints` | `string \| null`             |                                                                                |
| `imports`         | `string`                     | All imports for dependent types and schemas (from all endpoints).              | |
| `groupName`       | `string`                     | Usually a name of the API class. For `allEndpointsInOneFile` it will be `Api`. |
| `endpoints`       | `Array<RenderEndpointInfo>`↓ |                                                                                |
| `apiInfo`         | `ApiInfo \| null`↓           | |

### RenderEndpointInfo

| Name      | Type | Meaning                                       | Example   |
|-----------|------|-----------------------------------------------|-----------|
| `url` | `string` | Original URL as in OA | `'/orders/{orderId}'` |
| `urlWithParams` | `string` | URL with process path params. | `'/orders/${pathParams.orderId}'` |
| `requestType` | `RequestType` | Request method, valid values: `'get'`, `'post'`, `'put'`, `'patch'`, `'delete'` | `'get'` |
| `operationId` | `string` | | |
| `summary` | `string \| null` | | |
| `requestTsType` | `string \| null` | | |
| `requestSchema` | `string \| null` | | |
| `responseTsType` | `string \| null` | | |
| `responseSchema` | `string \| null` | | |
| `queryParamsTypeDefinition` | `string \| null` | | |
| `queryParamsTypeName` | `string \| null` | | |
| `queryParamsSchemaName` | `string \| null` | | |
| `queryParamsArgumentName` | `string \| null` | | |
| `pathParamsTypeDefinition` | `string \| null` | | |
| `pathParamsTypeName` | `string \| null` | | |
| `pathParamsSchemaName` | `string \| null` | | |
| `pathParamsArgumentName` | `string \| null` | | |
| `preparedFunctionArguments` | `string` | Convenience variable joining all data and params arguments. | `'data: SampleSheetRequest, params: GenerateSampleSheetQueryParams'` |
| `imports` | `string` | Imports of dependent types and schemas (of this endpoint). | |
| `dependencies` | `Array<string>` | | |
| `expectedStatusCode` | `number` | | `204` |
| `isResponseFileDownload` | `boolean` | Does endpoint serve for downloading a file? It usually means you want to set response type (decoding) to `arraybuffer` or `blob`. Response schema and type are configured via `downloadFileResponseSchema` and `downloadFileResponseType`.| `true` |
| `isRequestFileUpload` | `boolean` | Does endpoint serve for uploading a file? It is marked as such when request body is present and not JSON. | `false` |
| `meta` | `Unknown` | Custom metadata passed from an OpenAPI file from `x-meta` field in an endpoint (located on a same level as `operationId`) to the template. | `{ useLegacyAPI: true }` |

### ApiInfo
Info generated from the API directory.

| Name            | Type     | Meaning | Example                                      |
|-----------------|----------|---------|----------------------------------------------|
| `gitHash`       | `string` |         | `'917dbe570375ec52a6445d0f40f0093eae9b5f05'` |
| `gitHashShort`  | `string` |         | `'917dbe57'`                                 |
| `gitHashBranch` | `string` |         | `'master'`                                   |

Template
```ninja
// Generated from {{apiInfo.gitHashShort}} ({{apiInfo.gitBranch}})
```
can result into
```ts
// Generated from 917dbe57 (master)
```

### Custom endpoints

Custom endpoints YAML file:
```yaml
files: # dictionary with group names
  Api: # default name when using single file for generated endpoints is "Api"
    code: 'public static async getFleets(): Promise<Response<Fleet[]>> { ...' # required
    imports: "import { Fleet, FleetSchema } from 'generated-singlefile/Fleet';" # optional
```

Partial example of endpoints template:
```jinja
import * as t from 'io-ts';
{{ imports }}
{{ customEndpoints.imports }}

export class {{ groupName }} {
{% for it in endpoints %}
...
{% endfor %}
  {{ customEndpoints.code }}
}
```
Data are passed to the template via `customEndpoints` variable (appropriate value is selected from `files` dictionary based on group name).

## Run custom command on a file write

Configured via `afterFileWriteCommand`.
Example:
```yaml
afterFileWriteCommand: 'echo {{path}}'
```

Following variables can be used in a template:

| Name            | Description                                      | Example                      |
|-----------------|--------------------------------------------------|------------------------------|
| `path`          | Path to a file                                   | `/tmp/oats-gen/OkObj.ts`     |
| `fileName`      | File name only                                   | `OkObj.ts`                   |
| `sourceApiFile` | Path to a main file from which code is generated | `../data/singleFileOA2.yaml` |

