FROM haskell:9.2.5-slim-buster
WORKDIR /app

RUN apt-get -y update &&\
    apt-get -y install libgmp-dev curl build-essential netbase tree xxd

RUN curl https://rakubrew.org/install-on-perl.sh | sh
ENV PATH="/root/.rakubrew/bin:/root/.rakubrew/shims:$PATH"
RUN rakubrew mode shim
RUN rakubrew download 2023.06
RUN raku -v
RUN zef install Terminal::ANSI::OO

RUN export PATH=$HOME/.local/bin:$PATH
RUN mkdir -p ~/.local/bin
RUN curl --location https://get.haskellstack.org/stable/linux-x86_64.tar.gz | tar xz --wildcards --strip-components=1 --directory ~/.local/bin '*/stack'
RUN curl -L https://git.io/n-install | bash -s -- -y
RUN /root/n/bin/n 20.9.0
RUN echo "Node.js version (system): $(node -v) at $(which node)"

COPY docker/cache/user_home_stack /root/.stack
RUN mkdir -p /app/.stack-work
COPY docker/cache/project_stack_work /app/.stack-work
RUN du -hs /root/.stack /app/.stack-work

COPY stack.yaml .
COPY package.yaml .
RUN stack setup --install-ghc
RUN stack build --only-dependencies

COPY ts/package.json ts/package.json
RUN cd ts && npm i

COPY . .

RUN stack build

RUN ./test.raku

RUN stack install

RUN stack exec oats -- --version
