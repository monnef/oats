{-# LANGUAGE MultiWayIf #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE TupleSections #-}

module Main where

import BuildInfo (bi, buildDateTimeText, gitDirtyText, gitShortHash, gitTagText, versionText)
import CmdArgsParser
import Configuration (augmentConfigWithCmdArgs, loadConfig, maybeConfigToConfig, validateConfig)
import Control.Monad (unless)
import Control.Monad.IO.Class (liftIO)
import Control.Monad.Except (throwError)
import Control.Monad.State (get)
import Data
import Data.ColorMode (shouldUseColor)
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromMaybe, isJust)
import qualified Data.Text as T
import GHC.IO.Encoding (setLocaleEncoding, utf8)
import IgnoreRules (applyIgnoreRules)
import System.Directory.Extra (doesFileExist, setCurrentDirectory)
import System.Exit (ExitCode (..), exitWith)
import System.FilePath (takeDirectory)
import System.IO (hSetNewlineMode, noNewlineTranslation, stderr, stdout)
import TerminalUtils
import Utils
import Worker (workWithApiFile, workWithApiPath, workWithStdInOut)
import Optics
import Optics.State.Operators

handleWarningsAndErrors :: (AppM () -> AppM ()) -> Config -> AppOperationMode -> AppM ()
handleWarningsAndErrors ifNotQuiet config mode = do
  stateBeforeProcessing <- get
  removeIgnoredWarnings $ config ^. #ignoreWarnings
  removeIgnoredErrors $ config ^. #ignoreErrors
  applyIgnoreRules $ config ^. #ignoreRules
  finalState <- get
  let colorOutput' = config ^. #colorOutput
  let nonCriticalErrors = finalState ^. #appErrors
  let promotedWarnings' = finalState ^. #appWarnings & filter (getAppWarningWithoutContext >>> getAppWarningName >>> flip elem (config ^. #promoteWarningsToErrors))
  if null promotedWarnings'
    then skip
    else
      let promoteWarnToErr = formatAppWarning >>> PromotedWarning
       in throwError $ MultiError $ promotedWarnings' <&> promoteWarnToErr
  ifNotQuiet $ liftIO $ finalState ^. #appWarnings <&> formatAppWarning & mapM_ (printWarn colorOutput')
  unless (null nonCriticalErrors) $ do
    let err = MultiError $ nonCriticalErrors <&> NonCriticalAppError
    liftIO $ mapM_ (printError colorOutput') (formatAppError err)
  unless (config ^. #noSummary) $
    unless (mode == SimpleSchemaMode) $ do
      let ignoredErrorsCount = countErrors stateBeforeProcessing - countErrors finalState
      let ignoredWarningsCount = countWarnings stateBeforeProcessing - countWarnings finalState + ignoredErrorsCount
      let finalWarningsCount = countWarnings finalState
      let finalErrorsCount = countErrors finalState
      let warnPart = formatErrOrWarnCount "warning" "warnings" finalWarningsCount <> formatIgnoreText ignoredWarningsCount
      let errorPart = formatErrOrWarnCount "error" "errors" finalErrorsCount <> formatIgnoreText ignoredErrorsCount
      tm <- use #timing
      let timePart =
            tm <&> (view #elapsedTime >>> fmtFloat2 >>> (" in " <>) >>> (<> "s"))
              & fromMaybe ""
              & bool id (const "") (config ^. #noTiming)
      let appRes =
            if
                | finalErrorsCount > 0 -> ErrResult
                | finalWarningsCount > 0 -> WarnResult
                | otherwise -> OkResult
      let resMsg = [qq|Finished with $warnPart and $errorPart$timePart.$nl|]
      liftIO $ printResult colorOutput' appRes resMsg
  unless (null nonCriticalErrors) $ throwError $ ErrorAlreadyHandled $ MultiError $ nonCriticalErrors <&> NonCriticalAppError
  where
    countWarnings = view #appWarnings >>> length
    countErrors = view #appErrors >>> length
    formatIgnoreText 0 = ""
    formatIgnoreText n = [qq| ($n ignored)|]
    formatErrOrWarnCount :: Text -> Text -> Int -> Text
    formatErrOrWarnCount _ multi 0 = [qq|no $multi|]
    formatErrOrWarnCount single _ 1 = [qq|1 $single|]
    formatErrOrWarnCount _ multi n = [qq|$n $multi|]

surroundWithTagForPrinting :: Text -> Text -> Text
surroundWithTagForPrinting tag msg =
  if T.isInfixOf "\n" msg
    then [qq|[$tag]$nl{indent 4 msg}[/$tag]|]
    else [qq|[$tag] $msg|]

app :: AppM ()
app = do
  cmdArgs <- liftIO parseCmdArgs
  let mkDebugLog ifVerbose (x :: Text) = ifVerbose $ tPutStrLn $ surroundWithTagForPrinting "debug" x
  let tempDebugLog = filterOutColorCodes >>> mkDebugLog (when (cmdArgs ^. #verbose)) >>> liftIO
  tempDebugLog $ pShow cmdArgs
  let configFileName = fromMaybe defaultConfigFileName (cmdArgs ^. #configPath)
  (loadedConfigRaw :: MaybeConfig) <- do
    fileExists <- liftIO $ toS configFileName & doesFileExist
    let explicitlyInCmdArgs = isJust $ cmdArgs ^. #configPath
    if fileExists || explicitlyInCmdArgs
      then loadConfig configFileName
      else return def
  tempDebugLog $ "loadedConfigRaw = " <> pShow loadedConfigRaw
  loadedConfigWithDefaults <- maybeConfigToConfig loadedConfigRaw
  config <- augmentConfigWithCmdArgs loadedConfigWithDefaults cmdArgs & liftEitherToAppM >>= validateConfig
  let handleColorCodesFiltering = bool filterOutColorCodes id (config ^. #colorOutput & shouldUseColor)
  let debugLog :: Text -> IO () = handleColorCodesFiltering >>> mkDebugLog (when (config ^. #verbose) >>> liftIO)
  let ifNotQuiet = unless (config ^. #quiet)
  liftIO $ debugLog $ "_config = " <> pShow config
  let mode =
        if
            | config ^. #apiPath & isJust -> BatchMode
            | config ^. #apiFile & isJust -> SingleFileMode
            | otherwise -> SimpleSchemaMode
  #finalColorOutput .= config ^. #colorOutput
  let mkDryRunPrinter :: Bool -> Text -> IO () -> IO () = \onlyInVerbose msg action ->
        liftIO $ do
          let isDryRun = config ^. #dryRun
          let printFn = bool printIO printDryRun isDryRun
          let shouldPrint = isDryRun || not onlyInVerbose || (onlyInVerbose && config ^. #verbose)
          when shouldPrint $ msg & handleColorCodesFiltering & printFn (config ^. #colorOutput)
          unless isDryRun action
  let skipOnDryRunAndPrintAlways = mkDryRunPrinter False
  let skipOnDryRunAndPrintInVerbose = mkDryRunPrinter True
  let ctx = WorkContext {metas = [], ..}
  case cmdArgs ^. #configPath of
    Nothing -> skip
    Just d -> liftIO $ setCurrentDirectory $ takeDirectory $ toS d
  if
      | cmdArgs ^. #printVersion -> do
        let buildParts = [gitShortHash, gitTagText, gitDirtyText, buildDateTimeText]
        liftIO $ debugLog $ tshow bi
        liftIO $ tPutStrLn $ versionText <> "#" <> T.intercalate " " (filter (/= "") buildParts)
      | cmdArgs ^. #printWarningsList -> liftIO $ tPutStrLn $ appWarningNames & T.intercalate ", "
      | cmdArgs ^. #printErrorsList -> liftIO $ tPutStrLn $ nonCriticalErrorNames & T.intercalate ", "
      | cmdArgs ^. #printDefaultSchemaTranslations ->
        let msg = defaultSchemaTranslationTable & M.toList <&> (\(a, b) -> a <> " -> " <> tshow b) & T.intercalate "\n"
         in liftIO $ tPutStrLn msg
      | otherwise -> do
        measureTiming "Processing" $ do
          case mode of
            BatchMode -> workWithApiPath ctx
            SingleFileMode -> workWithApiFile ctx
            SimpleSchemaMode -> workWithStdInOut ctx
        handleWarningsAndErrors ifNotQuiet config mode

main :: IO ()
main = do
  setLocaleEncoding utf8
  hSetNewlineMode stdout noNewlineTranslation
  hSetNewlineMode stderr noNewlineTranslation
  let startState = def
  (res, finalState) <- runAppM startState app
  case res of
    Right _ -> skip
    Left err -> do
      let printErr = printError $ finalState ^. #finalColorOutput
      unless (wasAppErrorHandled err) $ mapM_ printErr (formatAppError err)
      exitWith $ ExitFailure $ retErrVal $ appErrToReturnCode err
