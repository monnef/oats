{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE TypeApplications #-}

module CmdArgsParser where

import BuildInfo
import Data
import qualified Data.Text as T
import Options.Applicative
import qualified Options.Applicative as OA
import qualified Prettyprinter as D
import Text.Read (readMaybe)
import Utils

textReader :: ReadM Text
textReader = maybeReader $ T.pack >>> Just

cmdArgsParserInfo :: ParserInfo CmdArgs
cmdArgsParserInfo =
  OA.info
    (cmdArgsParser <**> helper)
    (fullDesc <> progDescDoc (Just doc))
  where
    doc =
      D.vsep
        [ D.pretty @String "OpenAPI to io-ts schema convertor created by monnef."
        , D.pretty ("Version: " <> versionText)
        , D.pretty ("Build info: " <> extendedBuildInfo)
        ]

readInt :: String -> Maybe Int
readInt = readMaybe

readText :: String -> Maybe Text
readText = T.pack >>> Just

intMaybeReader :: ReadM (Maybe Int)
intMaybeReader = maybeReader (readInt >>> pure)

textMaybeReader :: ReadM (Maybe Text)
textMaybeReader = maybeReader (readText >>> pure)

cmdArgsParser :: Parser CmdArgs
cmdArgsParser =
  CmdArgs
    <$> switch (long "verbose" <> short 'v' <> verboseHelp)
    <*> switch (long "version" <> versionHelp)
    <*> switch (long "non-required-as-undefined" <> nonRequiredAsUndefinedHelp)
    <*> option textMaybeReader (long "config" <> short 'c' <> value Nothing <> metavar "PATH" <> configHelp)
    <*> switch (long "quiet" <> short 'q' <> quietHelp)
    <*> option textMaybeReader (long "api" <> short 'a' <> value Nothing <> metavar "PATH" <> apiHelp)
    <*> option textMaybeReader (long "api-file" <> short 'f' <> value Nothing <> metavar "PATH" <> apiFileHelp)
    <*> option textMaybeReader (long "ignore" <> value Nothing <> metavar "REGEX" <> ignoreHelp)
    <*> option textMaybeReader (long "title" <> value Nothing <> metavar "TITLE" <> titleHelp)
    <*> option textMaybeReader (long "out" <> short 'o' <> value Nothing <> metavar "PATH" <> outHelp)
    <*> switch (long "print-warnings-list" <> printWarningsListHelp)
    <*> switch (long "print-default-schema-translations" <> printDefaultSchemaTranslationsHelp)
    <*> switch (long "print-errors-list" <> printErrorsListHelp)
    <*> switch (long "no-summary" <> noSummaryHelp)
    <*> switch (long "no-timing" <> noTimingHelp)
    <*> switch (long "no-color" <> noColorHelp)
    <*> switch (long "dry-run" <> dryRunHelp)
  where
    verboseHelp = help "Enable verbose output"
    versionHelp = help "Print version"
    nonRequiredAsUndefinedHelp = help "Model non-required fields as undefined instead of null"
    configHelp = help [qq|Path to configuration file. Default is "$defaultConfigFileName"|]
    quietHelp = help "Suppress printing of result to standard output"
    apiHelp = help "Path to directory with type definitions in Open API YAML format. Enables batch mode"
    apiFileHelp = help "Path to file with type definitions in Open API YAML format. Enables single-file mode"
    ignoreHelp = help "Regular expression of ignored files to skip in batch mode"
    titleHelp = help "Default title. Used in single-file mode when title field is missing"
    outHelp = help "Output directory for generated TypeScript files. Default output is stdout"
    printWarningsListHelp = help "Print list of names of all warnings"
    printDefaultSchemaTranslationsHelp = help "Print default schema translations"
    printErrorsListHelp = help "Print list of names of all errors"
    noSummaryHelp = help "Don't print summary"
    noTimingHelp = help "Don't print timing"
    noColorHelp = help "Don't output color codes"
    dryRunHelp = help "Don't do any dangerous filesystem operations"

parseCmdArgs :: IO CmdArgs
parseCmdArgs = execParser cmdArgsParserInfo
