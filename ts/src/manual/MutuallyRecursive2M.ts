import * as t from 'io-ts';

import {MutuallyRecursive1M, MutuallyRecursive1MSchema} from './MutuallyRecursive1M';

export interface MutuallyRecursive2M {
  tag: '2';
  next: MutuallyRecursive1M | null;
}

export const MutuallyRecursive2MSchema: t.Type<MutuallyRecursive2M> = t.recursion('MutuallyRecursive2M', () => {
  const requiredPart = t.interface({
    tag: t.literal('2'),
    next: t.union([MutuallyRecursive1MSchema, t.null]),
  });

  return t.exact(requiredPart);
});
