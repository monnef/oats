import * as t from 'io-ts';

export const nativeDecimalMMax = 1000;
export const nativeDecimalMMin = -1000;
export const nativeDecimalMMultipleOf = 0.01; // 2 places

const validateMinMax = (num: number): boolean => {
  return num > nativeDecimalMMin
         && num < nativeDecimalMMax
         && Number.isInteger(parseFloat((num / nativeDecimalMMultipleOf).toFixed(10)));
};

export const NativeDecimalMSchema = t.refinement(t.number, validateMinMax);
export type NativeDecimalM = number;
