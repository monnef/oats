import * as t from 'io-ts';
import {StringMatchingRegExpSchema} from './string-matching-regexp';

export const genExactPercentageMRegexGen = () => /^-?\d{1,3}(?:\.\d{1,2})?$/;

export const exactPercentageMMax = 999.99;
export const exactPercentageMMin = -999.99;

const validateMinMax = (x: string): boolean => {
  const num = Number(x);
  return num >= exactPercentageMMin && num <= exactPercentageMMax;
};
export const ExactPercentageMSchema = t.refinement(StringMatchingRegExpSchema<ExactPercentageM>(genExactPercentageMRegexGen()), validateMinMax);
export type ExactPercentageM = string;
