import * as t from 'io-ts';
import {pipe} from 'fp-ts/lib/pipeable';
import * as Either from 'fp-ts/lib/Either';

export const StringMatchingRegExpSchema = <T extends string>(regexp: RegExp): t.Type<T, T, t.mixed> =>
    new t.Type<T>(
        'StringMatchingRegExp',
        (m: unknown): m is T => typeof m === 'string',
        (m, c) =>
            pipe(
                t.string.validate(m, c),
                Either.chain(string => regexp.test(string) ? t.success(string as T) : t.failure(string, c)),
            ),
        a => a,
    );
