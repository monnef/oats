import * as t from 'io-ts';

export interface DirectlyRecursiveM {
  id: number;
  next: DirectlyRecursiveM | null;
}

export const DirectlyRecursiveMSchema: t.Type<DirectlyRecursiveM> = t.recursion('DirectlyRecursiveMSchema', () => {
  const requiredPart = t.interface({
    id: t.number,
    next: t.union([DirectlyRecursiveMSchema, t.null]),
  });

  return t.exact(requiredPart);
});
