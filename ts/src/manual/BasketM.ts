import * as t from 'io-ts';

export const BasketMSchema = t.refinement(t.array(t.number), xs => new Set(xs).size === xs.length, 'BasketM');

export type BasketM = t.TypeOf<typeof BasketMSchema>;
