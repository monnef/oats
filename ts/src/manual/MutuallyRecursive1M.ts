import * as t from 'io-ts';

import {MutuallyRecursive2M, MutuallyRecursive2MSchema} from './MutuallyRecursive2M';

export interface MutuallyRecursive1M {
  tag: '1';
  next: MutuallyRecursive2M | null;
}

export const MutuallyRecursive1MSchema: t.Type<MutuallyRecursive1M> = t.recursion('MutuallyRecursive1M', () => {
  const requiredPart = t.interface({
    tag: t.literal('1'),
    next: t.union([MutuallyRecursive2MSchema, t.null]),
  });

  return t.exact(requiredPart);
});
