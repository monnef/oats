import * as t from 'io-ts';

export const NameMSchema = t.refinement(t.string, x => x.length >= 3 && x.length <= 32);

export type NameM = string;
