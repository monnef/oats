import * as t from 'io-ts';

export const ArrayMinMaxLengthMSchema = t.refinement(t.array(t.number), xs => xs.length >= 1 && xs.length <= 10, 'ArrayMinMaxLengthM');

export type ArrayMinMaxLengthM = t.TypeOf<typeof ArrayMinMaxLengthMSchema>;
