import * as t from 'io-ts';
import {StringMatchingRegExpSchema} from './string-matching-regexp';

export const genEmailRegex = () => /^[\w%+-.]+@[\w-.]+\.[A-Za-z]{2,}$/;

export const EmailMSchema = t.refinement(StringMatchingRegExpSchema<EmailM>(genEmailRegex()), x => x.length <= 100);

export type EmailM = string;
