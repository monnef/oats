import * as chai from 'chai';
import * as spies from 'chai-spies';
import {Either, isLeft, isRight} from 'fp-ts/Either';
import {Errors, Type} from 'io-ts';
import {replicate} from 'fp-ts/Array';

import {EmailMSchema} from 'manual/EmailM';
import {EmailSchema} from 'generated/user/Email';
import {NameMSchema} from 'manual/NameM';
import {NameSchema} from 'generated/user/Name';
import {DateTime, DateTimeSchema} from 'generated/common/DateTime';
import {Date} from 'generated/common/Date';
import {Account, AccountSchema} from 'generated/Account';
import {ObjWithObjArr, ObjWithObjArrSchema} from 'generated/ObjWithObjArr';
import {MultipleSameRefs, MultipleSameRefsSchema} from 'generated/MultipleSameRefs';
import {AgeSchema} from 'generated/user/Age';
import {BasketMSchema} from 'manual/BasketM';
import {BasketSchema} from 'generated/Basket';
import {ExactPercentageMSchema} from 'manual/ExactPercentageM';
import {ExactPercentageSchema} from 'generated/ExactPercentage';
import {Pet, PetSchema} from 'generated/pet/Pet';
import {SingleMemberUnion, SingleMemberUnionSchema} from 'generated/SingleMemberUnion';
import {NativeDecimalMSchema} from 'manual/NativeDecimalM';
import {
  nativeDecimalMaximumInclusive,
  nativeDecimalMinimumInclusive,
  nativeDecimalMinMaxInclusive,
  nativeDecimalRegexGen,
  NativeDecimalSchema
} from 'generated/NativeDecimal';
import {Fleet, FleetSchema, oatsVersion} from 'generated-singlefile/Fleet';
import {ArrayMinMaxLengthMSchema} from 'manual/ArrayMinMaxLengthM';
import {ArrayMinMaxLengthSchema} from 'generated/ArrayMinMaxLength';
import {ArrayMaxLengthUniqueSchema} from 'generated/ArrayMaxLengthUnique';
import {PropertyNameMapTest, PropertyNameMapTestSchema} from 'generated/PropertyNameMapTest';
import {customFlag} from 'generated-singlefile/CustomTest';
import {Api, apiInfo, customEndpointImports} from 'generated-singlefile/Api';
import {BaseApi} from '../BaseApi';
import {startMockServer} from './mockServer';
import {Ship, ShipSchema} from 'generated-singlefile/Ship';
import {FleetsApi} from 'generated-singlefile-endpoints-by-tag/FleetsApi';
import {DirectlyRecursiveM, DirectlyRecursiveMSchema} from 'manual/DirectlyRecursiveM';
import {MutuallyRecursive1M, MutuallyRecursive1MSchema} from 'manual/MutuallyRecursive1M';
import {DirectlyRecursive} from 'generated-singlefile/DirectlyRecursive';
import {MutuallyRecursive1, MutuallyRecursive1Schema} from 'generated-singlefile/MutuallyRecursive1';
import {numberMultipleOfDecimalPlaces} from 'generated-singlefile/NumberMultipleOf';
import {TopLevelDateTimeSchema} from 'generated-singlefile/TopLevelDateTime';
import {TopLevelDateSchema} from 'generated-singlefile/TopLevelDate';
import {TopLevelUuidSchema} from 'generated-singlefile/TopLevelUuid';
import {Uuid} from 'generated/common/Uuid';
import {
  decimal20DecimalPlaces,
  decimal20MaximumInclusive,
  decimal20MinimumInclusive,
  Decimal20Schema
} from 'generated-singlefile/Decimal20';
import {
  ValidationNumCustomLimitTemplateMaximumIncl,
  ValidationNumCustomLimitTemplateMinimumIncl,
  ValidationNumCustomLimitTemplateMaximumExcl,
  ValidationNumCustomLimitTemplateMinimumExcl,
} from 'generated-singlefile-num-limits/NumCustomLimitTemplate';
import {
  ValidationNumCustomLimitTemplateOnlyMaxMaximumIncl,
  ValidationNumCustomLimitTemplateOnlyMaxMaximumExcl,
  numCustomLimitTemplateOnlyMaxMinMaxInclusive
} from 'generated-singlefile-num-limits/NumCustomLimitTemplateOnlyMax';
import {
  ValidationNumCustomLimitTemplateSwappedMaximumExcl,
  ValidationNumCustomLimitTemplateSwappedMaximumIncl,
  ValidationNumCustomLimitTemplateSwappedMinimumExcl,
  ValidationNumCustomLimitTemplateSwappedMinimumIncl
} from "../generated-singlefile-num-limits/NumCustomLimitTemplateSwapped";
import {TrimmedMinLengthSchema, trimmedMinLengthTrimmedMinLength} from "../generated-singlefile/TrimmedMinLength";
import {
  TrimmedMinLengthWithMaxLengthSchema,
  trimmedMinLengthWithMaxLengthTrimmedMinLength
} from "../generated-singlefile/TrimmedMinLengthWithMaxLength";

chai.use(spies);
const {expect} = chai;
chai.should();
// const sandbox = chai.spy.sandbox();

describe('test environment', () => {
  it('works', () => {
    expect(true).to.be.eq(true);
  });
});

const formatError = (x: Either<Errors, unknown>) => isLeft(x) ? JSON.stringify(x.left) : '';

describe('email (regex + max len)', () => {
  const genTests = (schema: Type<string, string>) => () => {
    it('accepts valid', () => {
      const pos = schema.decode('a@a.aa');
      expect(isRight(pos)).to.be.equal(true, formatError(pos));
    });
    describe('rejects invalid', () => {
      it('regex', () => {
        const neg = schema.decode('a@a.a');
        expect(isRight(neg)).to.be.equal(false);
      });
      it('max len', () => {
        const middlePartLength = 500;
        const input = 'a@' + replicate(middlePartLength, 'x').join('') + '.aa';
        input.length.should.be.at.least(middlePartLength);
        const pos = schema.decode(input);
        expect(isRight(pos)).to.be.equal(false, formatError(pos));
      });
    });
  };

  describe('manual', genTests(EmailMSchema));
  describe('generated', genTests(EmailSchema));
});

describe('name (min + max len)', () => {
  const genTests = (schema: Type<string, string>) => () => {
    it('accepts valid', () => {
      const pos = schema.decode('xxx');
      expect(isRight(pos)).to.be.equal(true, formatError(pos));
    });
    describe('rejects invalid', () => {
      it('min len', () => {
        const neg = schema.decode('xx');
        expect(isRight(neg)).to.be.equal(false);
      });
      it('max len', () => {
        const input = replicate(33, 'x').join('');
        input.length.should.be.equal(33);
        const pos = schema.decode(input);
        expect(isRight(pos)).to.be.equal(false, formatError(pos));
      });
    });
  };

  describe('manual', genTests(NameMSchema));
  describe('generated', genTests(NameSchema));
});

describe('x-oats-trimmedMinLength', () => {
  it('pos alone', () => {
    const pos = TrimmedMinLengthSchema.decode('aa');
    expect(isRight(pos)).to.be.equal(true);
  });
  it('pos with max', () => {
    const pos = TrimmedMinLengthWithMaxLengthSchema.decode('aaaa');
    expect(isRight(pos)).to.be.equal(true);
  })
  it('pos with max - space on end', () => {
    const pos = TrimmedMinLengthWithMaxLengthSchema.decode('aaa ');
    expect(isRight(pos)).to.be.equal(true);
  });

  const negTestWithoutMax = (description: string, input: string) => {
    it(description, () => {
      const neg = TrimmedMinLengthSchema.decode(input);
      expect(isRight(neg)).to.be.equal(false, input);
    });
  }

  describe('neg alone', () => {
    negTestWithoutMax('space on left', ' a');
    negTestWithoutMax('space on right', 'a ');
    negTestWithoutMax('spaces around', ' a ');
    negTestWithoutMax('more stuff around', '  a \t ');
  });

  const negTestWithMax = (description: string, input: string) => {
    it(description, () => {
      const neg = TrimmedMinLengthWithMaxLengthSchema.decode(input);
      expect(isRight(neg)).to.be.equal(false, input);
    });
  }

  describe('neg with max', () => {
    negTestWithMax('space on left', '   aa');
    negTestWithMax('space on right', 'a     ');
    negTestWithMax('spaces around', '  a   ');
    negTestWithMax('more stuff around', '  aa \t ');
  });

  describe('emits constants', () => {
    expect(trimmedMinLengthTrimmedMinLength).to.be.equal(2);
    expect(trimmedMinLengthWithMaxLengthTrimmedMinLength).to.be.equal(2);
  });
});

describe('custom schema', () => {
  describe('DateTime', () => {
    it('accepts valid', () => {
      const res = DateTimeSchema.decode('2050-01-02');
      expect(isRight(res)).to.be.equal(true, formatError(res));
    });
    it('rejects invalid', () => {
      const res = DateTimeSchema.decode('1. 2. 2050');
      expect(isRight(res)).to.be.equal(false, formatError(res));
    });
  });
});

describe('referencing custom schema DateTime', () => {
  it('rejects invalid date', () => {
    const acc: Account = {email: 'a@a.aa', createdDate: 'x'};
    const res = AccountSchema.decode(acc);
    expect(isRight(res)).to.be.equal(false);
  });
  it('accepts valid date', () => {
    const acc: Account = {email: 'a@a.aa', createdDate: '2050-01-02'};
    const res = AccountSchema.decode(acc);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
});

// to check if array item schemas are not missing imports
describe('array of objects', () => {
  it('accepts valid', () => {
    const x: ObjWithObjArr = {
      xs: ['a@a.aa', 'a@a.aa'],
    };
    const res = ObjWithObjArrSchema.decode(x);
    expect(isRight(res)).to.be.eq(true, formatError(res));
  });
  it('rejects invalid', () => {
    const x: ObjWithObjArr = {
      xs: ['a@a.a'],
    };
    const res = ObjWithObjArrSchema.decode(x);
    expect(isRight(res)).to.be.eq(false, formatError(res));
  });
});

// to check: TS2300: Duplicate identifier 'EmailSchema'.
describe('multiple references to same schema', () => {
  it('accepts valid', () => {
    const x: MultipleSameRefs = {
      a: 'a@a.aa',
      b: 'a@a.aa',
      c: 'a@a.aa',
    };
    const res = MultipleSameRefsSchema.decode(x);
    expect(isRight(res)).to.be.eq(true, formatError(res));
  });
});

describe('integer minimum and maximum', () => {
  it('accepts valid', () => {
    const res1 = AgeSchema.decode(0);
    expect(isRight(res1)).to.be.eq(true, formatError(res1));
    const res2 = AgeSchema.decode(200);
    expect(isRight(res2)).to.be.eq(true, formatError(res2));
  });
  it('rejects invalid', () => {
    const res1 = AgeSchema.decode(-1);
    expect(isRight(res1)).to.be.eq(false, formatError(res1));
    const res2 = AgeSchema.decode(201);
    expect(isRight(res2)).to.be.eq(false, formatError(res2));
  });
});

describe('uniqueItems', () => {
  const genTests = (schema: Type<number[], number[]>) => () => {
    it('accepts valid', () => {
      const pos = schema.decode([1, 2, 3]);
      expect(isRight(pos)).to.be.equal(true, formatError(pos));
    });
    it('rejects invalid', () => {
      const neg = schema.decode([1, 2, 1]);
      expect(isRight(neg)).to.be.equal(false);
    });
  };

  describe('manual', genTests(BasketMSchema));
  describe('generated', genTests(BasketSchema));
});


describe('decimal', () => {
  const genTests = (schema: Type<string, string>) => () => {
    it('accepts valid', () => {
      const genPos = (input: string) => {
        const pos = schema.decode(input);
        expect(isRight(pos)).to.be.equal(true, `input = "${input}"; ${formatError(pos)}`);
      };
      [
        '0.0',
        '0.1',
        '-0.1',
        '0.11',
        '1.11',
        '11.11',
        '111.11',
        '999.99',
        '-999.99',
        '0',
        '-7',
      ].forEach(genPos);
    });
    it('rejects invalid', () => {
      const genNeg = (input: string) => {
        const neg = schema.decode(input);
        expect(isRight(neg)).to.be.equal(false, `input = "${input}"; ${formatError(neg)}`);
      };
      [
        'x',
        '-1000',
        '-1000.0',
        '-1000.99',
        '-999.100',
        '',
      ].forEach(genNeg);
    });
  };

  describe('manual', genTests(ExactPercentageMSchema));
  describe('generated', genTests(ExactPercentageSchema));
});

describe('tagged union via mapping field in discriminator', () => {
  describe('validation', () => {
    it('pos', () => {
      const pos = PetSchema.decode({petType: 'CAT', name: 'Spot'});
      expect(isRight(pos)).to.be.equal(true, formatError(pos));
    });
    it('neg', () => {
      const neg = PetSchema.decode({petType: 'DOG', name: 'Bad Wolf'});
      expect(isRight(neg)).to.be.equal(false);
    });
  });

  describe('types', () => {
    it('impossible to construct illegal member', () => {
      // @ts-expect-error
      const wrongDog: Pet = {petType: 'DOG'};
      // @ts-expect-error
      const wrongCat: Pet = {petType: 'CAT', bark: 'meow'};
    });
    it('type narrowing', () => {
      const pet: Pet = {petType: 'DOG', bark: 'yes!'} as any;
      if (pet.petType === 'DOG') {
        expect(pet.bark).to.not.be.undefined;
      }
    });
  });
});

describe('single member union', () => {
  it('compiles and validates', () => {
    const x: SingleMemberUnion = '';
    const res = SingleMemberUnionSchema.decode(x);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
});

describe('native decimal', () => {
  const genTests = (schema: Type<number, number>) => () => {
    it('accepts valid', () => {
      const genPos = (input: number) => {
        const pos = schema.decode(input);
        expect(isRight(pos)).to.be.equal(true, `input = "${input}"; ${formatError(pos)}`);
      };
      [
        0.0,
        0.1,
        -0.1,
        0.11,
        1.11,
        11.11,
        111.11,
        2.22,
        999.99,
        -999.99,
        0,
        -7,
      ].forEach(genPos);
    });
    it('rejects invalid', () => {
      const genNeg = (input: number) => {
        const neg = schema.decode(input);
        expect(isRight(neg)).to.be.equal(false, `input = "${input}"; ${formatError(neg)}`);
      };
      [
        -1000,
        -1000.0,
        -1000.99,
        -999.999,
        -999.9999,
        -999.99999,
        999.999,
        999.9999,
        999.99999,
        999.999999,
        999.9999999,
        2.221,
        2.22001,
        2.2200001,
        0.0000001,
        0.00000001,
        0.000000001,
        0.0000000001,
        0.00000000001,
        0.000000000001,
      ].forEach(genNeg);
    });
  };

  describe('manual', genTests(NativeDecimalMSchema));
  describe('generated', genTests(NativeDecimalSchema));

  describe('generated', () => {
    describe('"decimal" with zero decimal places', () => {
      it('generates correct decimal places', () => {
        expect(decimal20DecimalPlaces).to.be.equal(0);
      });
      it('pos', () => {
        const res1 = Decimal20Schema.decode(0);
        expect(isRight(res1)).to.be.equal(true, formatError(res1));
        const res2 = Decimal20Schema.decode(-99);
        expect(isRight(res2)).to.be.equal(true, formatError(res2));
        const res3 = Decimal20Schema.decode(99);
        expect(isRight(res3)).to.be.equal(true, formatError(res3));
        expect(decimal20MinimumInclusive).to.be.eq(-99);
        expect(decimal20MaximumInclusive).to.be.eq(99);
      });
      it('neg', () => {
        const res1 = Decimal20Schema.decode(0.1);
        expect(isLeft(res1)).to.be.true;
        const res2 = Decimal20Schema.decode(100);
        expect(isLeft(res2)).to.be.true;
        const res3 = Decimal20Schema.decode(-100);
        expect(isLeft(res3)).to.be.true;
      });
    });

    describe('min and max inclusive consts', () => {
      const eps = 0.0001;
      it('default template', () => {
        expect(nativeDecimalMinimumInclusive).to.be.approximately(-999.99, eps);
        expect(nativeDecimalMaximumInclusive).to.be.approximately(999.99, eps);
      });
      it('custom template', () => {
        expect(ValidationNumCustomLimitTemplateMinimumIncl).to.be.approximately(0.1, eps);
        expect(ValidationNumCustomLimitTemplateMaximumIncl).to.be.approximately(10.0, eps);
        expect(ValidationNumCustomLimitTemplateMinimumExcl).to.be.approximately(0.0, eps);
        expect(ValidationNumCustomLimitTemplateMaximumExcl).to.be.approximately(9.9, eps);

        expect(ValidationNumCustomLimitTemplateSwappedMinimumIncl).to.be.approximately(0.0, eps);
        expect(ValidationNumCustomLimitTemplateSwappedMaximumIncl).to.be.approximately(9.9, eps);
        expect(ValidationNumCustomLimitTemplateSwappedMinimumExcl).to.be.approximately(0.1, eps);
        expect(ValidationNumCustomLimitTemplateSwappedMaximumExcl).to.be.approximately(10.0, eps);
      });
      it('custom template - only max', () => {
        expect(ValidationNumCustomLimitTemplateOnlyMaxMaximumIncl).to.be.approximately(9.99, eps);
        expect(ValidationNumCustomLimitTemplateOnlyMaxMaximumExcl).to.be.approximately(10.0, eps);
      });
    });

    describe('min and max combined consts', () => {
      it('both', () => {
        const eps = 0.0001;
        expect(nativeDecimalMinMaxInclusive[0]).to.be.approximately(-999.99, eps);
        expect(nativeDecimalMinMaxInclusive[1]).to.be.approximately(999.99, eps);
      });

      it('only max', () => {
        const eps = 0.0001;
        expect(numCustomLimitTemplateOnlyMaxMinMaxInclusive[0]).to.be.null;
        expect(numCustomLimitTemplateOnlyMaxMinMaxInclusive[1]).to.be.approximately(9.99, eps);
      });
    })
  });

  it('regex', () => {
    expect(nativeDecimalRegexGen.test('')).to.be.false;
    expect(nativeDecimalRegexGen.test('-')).to.be.false;
    expect(nativeDecimalRegexGen.test('-1')).to.be.false;
    expect(nativeDecimalRegexGen.test('1')).to.be.false;
    expect(nativeDecimalRegexGen.test('1.00')).to.be.true;
    expect(nativeDecimalRegexGen.test('0.00')).to.be.true;
    expect(nativeDecimalRegexGen.test('-0.00')).to.be.true;
    expect(nativeDecimalRegexGen.test('999.99')).to.be.true;
    expect(nativeDecimalRegexGen.test('1000.00')).to.be.false;
    expect(nativeDecimalRegexGen.test('-999.99')).to.be.true;
    expect(nativeDecimalRegexGen.test('-1000.00')).to.be.false;
  });
});

describe('datetime', () => {
  const genShip = (constructedAt: any) => ({
    id: 0,
    code: '',
    name: '',
    constructedAt
  });
  it('pos', () => {
    const res = ShipSchema.decode(genShip('2020-04-11T12:00:40.500'));
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
  it('rejects empty date', () => {
    const res = ShipSchema.decode(genShip(''));
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
  it('rejects invalid date', () => {
    const res = ShipSchema.decode(genShip('Liz'));
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
});

describe('top-level datetime', () => {
  it('pos', () => {
    const pos: DateTime = '2020-04-11T12:00:40.500';
    const res = TopLevelDateTimeSchema.decode(pos);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
  it('rejects empty date', () => {
    const res = TopLevelDateTimeSchema.decode('');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
  it('rejects invalid date', () => {
    const res = TopLevelDateTimeSchema.decode('Zel');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
});

describe('top-level date', () => {
  it('pos', () => {
    const date: Date = '2020-04-11';
    const res = TopLevelDateSchema.decode(date);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
  it('rejects empty date', () => {
    const res = TopLevelDateSchema.decode('');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
  it('rejects invalid date', () => {
    const res = TopLevelDateSchema.decode('Mael');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
});

describe('top-level uuid', () => {
  it('pos', () => {
    const uuid: Uuid = '123E4567-e89b-12d3-a456-426614174000';
    const res = TopLevelUuidSchema.decode(uuid);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
  it('rejects empty uuid', () => {
    const res = TopLevelUuidSchema.decode('');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
  it('rejects invalid uuid', () => {
    const res = TopLevelUuidSchema.decode('123E4567-e89b-12d3-x456-426614174000');
    expect(isRight(res)).to.be.equal(false, formatError(res));
  });
});

describe('singlefile', () => {
  it('dependency works', () => {
    const data: Fleet = {
      ships: [
        {name: 'USS Ajax', code: 'NCC-11574', id: 69, constructedAt: '2050-01-02'},
      ],
    };
    const res = FleetSchema.decode(data);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });

  it('custom schema overrides existing one', () => {
    expect(customFlag).to.be.true;
  });
});

describe('header', () => {
  it('renders version', () => {
    expect(oatsVersion).to.be.not.empty;
  });
});

describe('array with length limit', () => {
  const genTests = (schema: Type<number[], number[]>) => () => {
    it('accepts valid', () => {
      const pos = schema.decode([1, 2]);
      expect(isRight(pos)).to.be.equal(true, formatError(pos));
    });
    describe('rejects invalid', () => {
      it('min len', () => {
        const neg = schema.decode([]);
        expect(isRight(neg)).to.be.equal(false);
      });
      it('max len', () => {
        const input = replicate(33, 0);
        input.length.should.be.equal(33);
        const pos = schema.decode(input);
        expect(isRight(pos)).to.be.equal(false, formatError(pos));
      });
    });
  };

  describe('manual', genTests(ArrayMinMaxLengthMSchema));
  describe('generated', genTests(ArrayMinMaxLengthSchema));
});

describe('array max items + unique', () => {
  it('accepts valid', () => {
    const genPos = (input: number[]) => {
      const pos = ArrayMaxLengthUniqueSchema.decode(input);
      expect(isRight(pos)).to.be.equal(true, `input = "${input}"; ${formatError(pos)}`);
    };
    [
      [],
      [1],
      [1, 2],
      [1, 2, 3],
    ].forEach(genPos);
  });
  it('rejects invalid', () => {
    const genNeg = (input: number[]) => {
      const neg = ArrayMaxLengthUniqueSchema.decode(input);
      expect(isRight(neg)).to.be.equal(false, `input = "${input}"; ${formatError(neg)}`);
    };
    [
      [1, 2, 3, 4],
      [1, 1],
      [1, 2, 3, 4, 4],
    ].forEach(genNeg);
  });
});

describe('property name map', () => {
  it('translates: testName1 -> newName1', () => {
    const a: PropertyNameMapTest = {newName1: 'x'};
    const res = PropertyNameMapTestSchema.decode(a);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
});

describe('overrideNonRequiredRepresentation', () => {
  it('overrides', () => {
    const a: Fleet = {};
    const res = FleetSchema.decode(a);
    expect(isRight(res)).to.be.equal(true, formatError(res));
  });
});

describe('recursive types', () => {
  describe('direct', () => {
    it('manual', () => {
      const x: DirectlyRecursiveM = {
        id: 0,
        next: {
          id: 1,
          next: null,
        },
      };
      const res = DirectlyRecursiveMSchema.decode(x);
      expect(isRight(res)).to.be.equal(true, formatError(res));
    });

    it('generated', () => {
      const x: DirectlyRecursive = {
        id: 0,
        next: {
          id: 1,
          next: null,
        },
      };
      const res = DirectlyRecursiveMSchema.decode(x);
      expect(isRight(res)).to.be.equal(true, formatError(res));
    });
  });

  describe('indirect (mutually recursive)', () => {
    it('manual', () => {
      const x: MutuallyRecursive1M = {
        tag: '1',
        next: {
          tag: '2',
          next: {
            tag: '1',
            next: {
              tag: '2',
              next: null,
            },
          },
        },
      };
      const res = MutuallyRecursive1MSchema.decode(x);
      expect(isRight(res)).to.be.equal(true, formatError(res));
    });

    it('generated', () => {
      const x: MutuallyRecursive1 = {
        tag: '1',
        next: {
          tag: '2',
          next: {
            tag: '1',
            next: {
              tag: '2',
              next: null,
            },
          },
        },
      };
      const res = MutuallyRecursive1Schema.decode(x);
      expect(isRight(res)).to.be.equal(true, formatError(res));
    });
  });
});

describe('number', () => {
  it('multipleOf', () => {
    expect(numberMultipleOfDecimalPlaces).to.be.eq(2);
  });
});

describe('api info', () => {
  it('is filled', () => {
    expect(apiInfo.gitHash).to.be.a('string');
    expect(apiInfo.gitHashShort).to.be.a('string');
    expect(apiInfo.gitBranch).to.be.a('string');
  });
});

describe('endpoints', () => {
  it('custom endpoint code present', () => {
    expect(Api.customEndpoint).to.be.true;
    expect(customEndpointImports).to.be.true;
  });

  describe('requests', () => {
    it('get', async () => {
      const debugLogsEnabled = false;
      const debugLog = (...xs: unknown[]) => {
        if (debugLogsEnabled) {
          console.log('[get test]', ...xs);
        }
      };
      debugLog('starting server');
      const server = await startMockServer(BaseApi.apiPort);
      debugLog('server is running');
      try {
        const r = await Api.getShips();
        debugLog('got response', r);
        expect(r.status).to.be.eq(200);
        expect(r.data).to.be.eql([
          {name: "USS Ajax", code: "NCC-11574", id: 69, constructedAt: '2050-01-02'},
        ]);
      } catch (e) {
        console.error(e);
        throw e;
      } finally {
        server.close();
      }
    });

    it('put', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      const genNewShip = (): Ship => ({name: "USS Ajax 2", code: "NCC-11574", id: 69, constructedAt: '2035-12-05'});
      try {
        const r = await Api.updateShip(genNewShip(), {shipId: genNewShip().id});
        expect(r.status).to.be.eq(200);
        expect(r.data).to.be.eql(genNewShip());
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('delete', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const rBefore = await Api.getShips();
        expect(rBefore.data).is.not.empty;
        const r = await Api.deleteShip({shipId: 69});
        expect(r.status).to.be.eq(200);
        const rAfter = await Api.getShips();
        expect(rAfter.data).is.empty;
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('get filtered', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const r = await Api.getShipsFiltered({name: 'y'});
        expect(r.status).to.be.eq(200);
        expect(r.data).to.be.empty;
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('get fleets uses x-meta', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const r = await Api.getFleets();
        expect(r.headers.fromBaseApi2).to.be.true;
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('get fleets (files by tags)', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const r = await FleetsApi.getFleets();
        expect(r.status).to.be.eq(200);
        expect(r.data).to.not.be.empty;
        expect(r.headers.fromBaseApi2).to.be.undefined;
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('get seconds (merged in from other yaml file)', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const r = await Api.getSeconds();
        expect(r.status).to.be.eq(200);
        expect(r.data).to.be.eql([{id: 4}]);
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });

    it('get thirds (merged in from other yaml file)', async () => {
      const server = await startMockServer(BaseApi.apiPort);
      try {
        const r = await Api.getThirds();
        expect(r.status).to.be.eq(200);
        expect(r.data).to.be.eql([{x: 5}]);
      } catch (e) {
        throw e;
      } finally {
        server.close();
      }
    });
  });

  it('permissions are generated', () => {
    expect(Api.permissionsDeleteShip).to.be.eql(['FleetAdmiral', 'Admiral']);
  });
});
