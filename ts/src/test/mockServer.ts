import * as http from 'http';
import * as jsonServer from 'json-server';

const db = Object.freeze({
  "ships": [
    {
      "name": "USS Ajax",
      "code": "NCC-11574",
      "id": 69,
      "constructedAt": "2050-01-02",
    },
  ],
  "ships-filtered": [],
  "fleets": [
    {ships: []},
  ],
  "seconds": [
    {id: 4}
  ],
  "thirds": [
    {x: 5}
  ],
});

export const startMockServer = (port: number, verbose = false): Promise<http.Server> => {
  const server = jsonServer.create();
  const router = jsonServer.router(db);
  const middlewares = jsonServer.defaults({logger: verbose});
  server.use(middlewares);
  server.use(router);
  return new Promise((resolve, _reject) => {
    const s = server.listen(port, () => {
      if (verbose) {
        console.log(`JSON Server is running on port ${port}`);
      }
      resolve(s);
    });
  });
};
