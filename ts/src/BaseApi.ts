import * as t from 'io-ts';
import {Type, Errors} from 'io-ts';
import {failure} from 'io-ts/lib/PathReporter';
import {getOrElse} from 'fp-ts/lib/Either';
import axios, {AxiosResponse, AxiosRequestConfig, AxiosError} from 'axios';

const debugLogsEnabled = false;

const debugLog = (...xs: unknown[]) => {
  if (debugLogsEnabled) {
    console.log('[BaseApi]', ...xs);
  }
};

export const validateSchema = <A, O>(type: t.Type<A, O>) => (value: unknown): A =>
  getOrElse((errors: Errors) => {
    console.error('validateSchema error: ', value, errors); // eslint-disable-line no-console
    const errorString = failure(errors).join('\n');
    throw new Error(errorString);
    return value as A;
  })(type.decode(value));

export type Response<T> = AxiosResponse<T>;

type RequestArgs<TResponse, TRequest> = AxiosRequestConfig & {
  requestSchema: Type<TRequest> | null;
  responseSchema: Type<TResponse> | null;
  data?: TRequest;
  expectedStatusCode?: number;
}

/**
 * Example implementation of base API / requester.
 *
 * It is very basic, e.g. it doesn't handle well errors, doesn't support authentication and so on.
 * You will most likely want to expand it a lot.
 */
export class BaseApi {
  static apiIp = 'http://127.0.0.1';
  static apiPort = 8094;
  static apiUrl = `${BaseApi.apiIp}:${BaseApi.apiPort}`;

  public static async makeRequest<TResponse, TRequest>({
    url,
    baseURL,
    method,
    headers,
    requestSchema,
    responseSchema,
    data,
    responseType,
    expectedStatusCode,
  }: RequestArgs<TResponse, TRequest>): Promise<Response<TResponse>> {
    const config: AxiosRequestConfig = {
      url,
      baseURL: baseURL ?? BaseApi.apiUrl,
      method,
      headers,
      responseType,
      data,
      timeout: 1000,
    };
    if (requestSchema) {
      validateSchema(requestSchema)(data);
    } else {
      console.warn('Missing request schema: ', requestSchema); // eslint-disable-line no-console
    }

    try {
      debugLog('doing request', config);
      const response = await axios(config);
      if (responseSchema) {
        validateSchema(responseSchema)(response.data);
      } else {
        console.warn('[BaseApi] Missing response schema: ', responseSchema); // eslint-disable-line no-console
      }
      if (expectedStatusCode) {
        if (response.status !== expectedStatusCode) {
          throw new Error(`Expected status code was ${expectedStatusCode}, but got ${response.status}.`);
        }
      }
      return response;
    } catch (e) {
      const error: AxiosError = e as any;
      console.error('[BaseApi] ERROR:', error, '\n', JSON.stringify(error, null, 2));
      throw e;
    }
  }
}

export class BaseApi2 {
  public static async makeRequest<TResponse, TRequest>(args: RequestArgs<TResponse, TRequest>): Promise<Response<TResponse>> {
    const res = await BaseApi.makeRequest(args);
    res.headers['fromBaseApi2'] = true;
    return res;
  }
}
