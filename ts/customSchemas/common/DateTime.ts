import * as t from 'io-ts';
import {pipe} from 'fp-ts/lib/pipeable';
import * as Either from 'fp-ts/lib/Either';
import * as moment from 'moment';

export const DateTimeSchema = new t.Type<string, string>(
  'DateTimeString',
  (m: unknown): m is string => typeof m === 'string',
  (input, c) =>
    pipe(
      t.string.validate(input, c),
      Either.chain(input2 => {
        const d = moment(input2, moment.ISO_8601);

        return d.isValid() ? t.success<DateTime>(input2 as string) : t.failure(input2, c);
      }),
    ),
  (a: string) => a,
);

export type DateTime = string;
