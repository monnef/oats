import * as t from 'io-ts';
import * as moment from 'moment';
import {either} from 'fp-ts';
import {pipe} from 'fp-ts/function';

export class DateType extends t.Type<string> {
    public readonly _tag: 'DateStringType' = 'DateStringType';

    constructor() {
        super(
            'DateString',
            (m: unknown): m is string => typeof m === 'string',
            (m, c) =>
                pipe(
                    t.string.validate(m, c),
                    either.chain(s => {
                        const d = moment(s, 'YYYY-MM-DD', true);

                        return d.isValid() ? t.success(s) : t.failure(s, c);
                    }),
                ),
            t.identity,
        );
    }
}

export const DateSchema = new DateType();

export type Date = string;
