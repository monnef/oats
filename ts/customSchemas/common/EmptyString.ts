import * as t from 'io-ts';

export const EmptyStringSchema = t.literal('');

export type EmptyString = t.TypeOf<typeof EmptyStringSchema>;
