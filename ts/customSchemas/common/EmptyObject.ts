import * as t from 'io-ts';

export interface EmptyObject {}

export const EmptyObjectSchema = t.exact(t.interface({}));
