import {StringMatchingRegExpSchema} from 'manual/string-matching-regexp';

export const genUuidRegex = () => /^[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}$/i;

export const UuidSchema = StringMatchingRegExpSchema<Uuid>(genUuidRegex());

export type Uuid = string;
