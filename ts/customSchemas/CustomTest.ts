// CUSTOM

import * as t from 'io-ts';

const requiredPart = t.interface({
  a: t.union([t.string, t.null]),
});

export const CustomTestSchema = t.exact(requiredPart);

export interface CustomTest extends t.TypeOf<typeof CustomTestSchema> {}

export const customFlag = true;
