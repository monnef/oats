{% script %}
set nl = "\n";
{% endscript %}
// Generated from {{apiInfo.gitHashShort}} ({{apiInfo.gitBranch}})

import * as t from 'io-ts';
import { BaseApi, Response, validateSchema, BaseApi2 } from '../BaseApi';
// @ts-ignore: Workaround for "is declared but its value is never read."
import { EmptyString, EmptyStringSchema } from './EmptyString';
// @ts-ignore: Workaround for "is declared but its value is never read."
import { EmptyObject, EmptyObjectSchema } from './EmptyObject';

{{ imports }}

{{ customEndpoints.imports }}

export const apiInfo = {
  gitHash: '{{apiInfo.gitHash}}',
  gitHashShort: '{{apiInfo.gitHashShort}}',
  gitBranch: '{{apiInfo.gitBranch}}',
};

{% for it in endpoints %}
{% if it.queryParamsTypeDefinition %}
{{ it.queryParamsTypeDefinition }}

{% endif %}
{% if it.pathParamsTypeDefinition %}
{{ it.pathParamsTypeDefinition }}

{% endif %}
{% endfor %}

export class {{ groupName }} {
{% for it in endpoints %}
{% if it.permissions %}  static permissions{{ capitalize(it.operationId) }} = {{ json(it.permissions, false) }};{{ nl }}{% endif %}
{% if it.summary %}  // {{ it.summary }}
{% endif %}
{% if it.description %}  // {{ it.description }}
{% endif %}
  public static async {{ it.operationId }}({{ it.preparedFunctionArguments }}): Promise<Response<{{ it.responseTsType }}>> {
{% if it.queryParamsSchemaName %}    validateSchema({{ it.queryParamsSchemaName }})({{ it.queryParamsArgumentName }});{{ nl }}{% endif %}
{% if it.pathParamsSchemaName %}    validateSchema({{ it.pathParamsSchemaName }})({{ it.pathParamsArgumentName }});{{ nl }}{% endif %}
    return BaseApi{{ it.meta.apiNumber }}.makeRequest({
      url: `{{ it.urlWithParams }}`,
      method: '{{ it.requestType }}',
{% if it.queryParamsTypeName %}      params: {{ it.queryParamsArgumentName }},{{ nl }}{% endif %}
{% if it.requestTsType %}      data,{{ nl }}{% endif %}
      requestSchema: {{ it.requestSchema }},
{% if it.isResponseFileDownload %}      responseType: 'arraybuffer',{{ nl }}{% endif %}
      responseSchema: {{ it.responseSchema }},
      expectedStatusCode: {{ it.expectedStatusCode }},
    });
  }

{% endfor %}
  {{ customEndpoints.code }}
}
