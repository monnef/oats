{% script %}
set nl = "\n";
{% endscript %}
import * as t from 'io-ts';
import { BaseApi, Response } from '../BaseApi';
// @ts-ignore: Workaround for "is declared but its value is never read."
import { EmptyString, EmptyStringSchema } from './EmptyString';
// @ts-ignore: Workaround for "is declared but its value is never read."
import { EmptyObject, EmptyObjectSchema } from './EmptyObject';

{{ imports }}

{% for it in endpoints %}
{% if it.queryParamsTypeDefinition %}
{{ it.queryParamsTypeDefinition }}

{% endif %}
{% if it.pathParamsTypeDefinition %}
{{ it.pathParamsTypeDefinition }}

{% endif %}
{% endfor %}

export class {{ groupName }} {
{% for it in endpoints %}
{% if it.summary %}  // {{ it.summary }}
{% endif %}
  public static async {{ it.operationId }}({{ it.preparedFunctionArguments }}): Promise<Response<{{ it.responseTsType }}>> {
    return BaseApi.makeRequest({
      url: `{{ it.urlWithParams }}`,
      method: '{{ it.requestType }}',
{% if it.queryParamsTypeName %}      params,{{ nl }}{% endif %}
{% if it.requestTsType %}      data,{{ nl }}{% endif %}
      requestSchema: {{ it.requestSchema }},
{% if it.isResponseFileDownload %}      responseType: 'arraybuffer',{{ nl }}{% endif %}
      responseSchema: {{ it.responseSchema }},
      expectedStatusCode: {{ it.expectedStatusCode }},
    });
  }

{% endfor %}
{{ customEndpoints }}
}
