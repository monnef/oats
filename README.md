![Oats banner](doc/banner.svg)

Oats takes an API description in a subset of [OpenAPI](https://www.openapis.org) format and generates schemas, types and endpoint methods or functions. It relies on [io-ts](https://github.com/gcanti/io-ts) library for runtime validation, sparing front-end developers from tedious debugging caused for example by a bug on a back-end or a mismatching API version.

Oats focuses on type safety and long term use. This resulted in several limitations which oats imposes on a structure of an input OpenAPI document.

For example anonymous object types are on many places forbidden, because it would lead to duplicities in schemas and would be hard to work with (it is also error-prone when API of these types changes, for example two same anonymous object schemas start to diverge).

Another example would be using `null` for non-required types (empty values). Changes in API types (e.g. addition of an optional field to a query parameter) are immediately obvious, because TypeScript will report them after API regeneration. While not strictly enforced, it is the default and not all features are supported with `undefined` in non-required types.

# Simple example

```ts
$ oats < test/data/trivial_object.yaml
import * as t from 'io-ts';

const requiredPart = t.interface({
  email: t.union([t.string, t.null]),
  username: t.string,
  id: t.Integer,
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
```

# Why?

* Automatic generation of types from OpenAPI schemas and request parameters  
  Those types can be used anywhere on the front-end. And when eventually API changes, TypeScript compiler will tell you where you need to update your code.
* Automatic (runtime) validation of request and response bodies  
  This means catching errors early. For example bugs on a front-end, back-end or mismatching API versions (using `null` as empty value is recommended for maximum effectiveness).
* OpenAPI validations  
  They are designed to check for bugs in API or even poorly designed schemas and endpoints, or parts which are unsuitable as an input for a code generation (thus badly usable in front-end code, e.g. anonymous enum and object schemas)

# [Installation](doc/installation.md)

# [Input](doc/input.md)

# [Output](doc/output.md)

# [Development](doc/development.md)

# License
* Code: GPLv3
* Graphics: Proprietary
