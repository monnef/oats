{-# OPTIONS_GHC -F -pgmF htfpp #-}

module TestUtils where

import qualified Data.Text as T
import Data.Text (Text)
import Test.Framework
import Test.Framework.TestInterface (Assertion)

assertInfixOf :: Text -> Text -> Assertion
assertInfixOf needle haystack = assertBool $ T.isInfixOf needle haystack
