{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module DataProcessorSpec
  ( htf_thisModulesTests,
  )
where

import Data
import Data.Aeson.Types as AT
import Data.Default (def)
import Data.Either.Extra (fromLeft', mapLeft)
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromJust)
import qualified Data.Text as T
import DataProcessor
import Optics hiding (allOf)
import Test.Framework
import Utils

defROA :: RawOA
defROA = def

defCfg :: Config
defCfg = def

defOAM :: OAMeta
defOAM =
  OAMeta
    { name = "Name",
      path = Nothing,
      dependencies = [],
      taggedUnionMapping = Nothing,
      example = Nothing,
      description = Nothing,
      comment = Nothing,
      isRecursive = Nothing
    }

defOAClass :: OA
defOAClass =
  OAClass
    { extends = Nothing,
      fields = [],
      tag = Nothing,
      overrideNonRequiredRepresentation = Nothing
    }

defEC :: ErrorContext
defEC = def

test_pathToSchemaToSchemaName :: IO ()
test_pathToSchemaToSchemaName = do
  assertEqual "" $ pathToSchemaToSchemaName ""
  assertEqual "Some" $ pathToSchemaToSchemaName "Some"
  assertEqual "Email" $ pathToSchemaToSchemaName "../../openapi.yaml#/components/schemas/Email"
  assertEqual "Email" $ pathToSchemaToSchemaName "../../openapi.yaml#/components/schemas/Email.yaml"

test_typeNameToTypeOA :: IO ()
test_typeNameToTypeOA = do
  let f = typeNameToTypeOA
  assertEqual (Just $ PrimitiveTOA StringPOA) $ f "string"
  assertEqual (Just $ PrimitiveTOA BooleanPOA) $ f "boolean"
  assertEqual (Just $ PrimitiveTOA IntegerPOA) $ f "integer"
  assertEqual (Just $ PrimitiveTOA NumberPOA) $ f "number"
  assertEqual Nothing $ f ""
  assertEqual Nothing $ f "xxx"

test_refToTypeOA :: IO ()
test_refToTypeOA = do
  let f = refToTypeOA defaultSchemaTranslationTable
  assertEqual (Just $ SchemaTOA "X") $ f "../../../openapi.yaml#/components/schemas/X"
  assertEqual (Just $ PrimitiveTOA BooleanPOA) $ f "../../../openapi.yaml#/components/schemas/Boolean.yaml"

test_arrayPropToTypeOA :: IO ()
test_arrayPropToTypeOA = do
  let f = arrayPropToTypeOA def defaultSchemaTranslationTable >>> evalAppM def
  r1 <- f $ defROA & #typ ?~ "array" & #items ?~ (defROA & #typ ?~ "string")
  assertEqual (Right $ Just $ ArrayTOA $ PrimitiveTOA StringPOA) r1
  r2 <- f $ defROA & #typ ?~ "array" & #items ?~ (defROA & #ref ?~ "../Obj.yaml")
  assertEqual (Right $ Just $ ArrayTOA $ SchemaTOA "Obj") r2

assertNonCriticalErrors :: AppState -> [NonCriticalError] -> IO ()
assertNonCriticalErrors s xs = assertEqual xs $ s ^. #appErrors

test_rawOAToTypeOA :: IO ()
test_rawOAToTypeOA = do
  let errCtx = def & #subUnitName ?~ "A"
  let f = rawOAToTypeOA errCtx defaultSchemaTranslationTable >>> runAppM def
  (r1, s1) <- f $ defROA & #typ ?~ "string"
  assertEqual (Right $ Just $ PrimitiveTOA StringPOA) r1
  assertNonCriticalErrors s1 []

  (r2, s2) <- f $ defROA & #ref ?~ "Obj"
  assertEqual (Right $ Just $ SchemaTOA "Obj") r2
  assertNonCriticalErrors s2 []

  (r3, s3) <- f $ defROA & #typ ?~ "array" & #items ?~ (defROA & #typ ?~ "string")
  assertEqual (Right $ Just $ ArrayTOA $ PrimitiveTOA StringPOA) r3
  assertNonCriticalErrors s3 []

  (r4, s4) <- f (defROA & #typ ?~ "array" & #items ?~ (defROA & #typ ?~ "string" & #uniqueItems ?~ True))
  assertEqual (Right $ Just $ ArrayTOA $ PrimitiveTOA StringPOA) r4
  assertNonCriticalErrors s4 [NonCriticalError errCtx ForbiddenValidationError]

test_oneValueEnumToTypeOA :: IO ()
test_oneValueEnumToTypeOA = do
  let f = oneValueEnumToTypeOA >>> evalAppM def
  r1 <- f $ defROA & #enum ?~ ["X"] & #typ ?~ "string"
  assertEqual (Right $ Just $ OneValueEnumTOA "X") r1
  r2 <- f $ defROA & #enum ?~ [] & #typ ?~ "string"
  assertEqual (Right Nothing) r2
  r3 <- f $ defROA & #enum ?~ ["X", "Y"] & #typ ?~ "string"
  assertEqual (Right Nothing) r3
  r4 <- f $ defROA & #enum ?~ ["X", "Y", "Z"] & #typ ?~ "string"
  assertEqual (Right Nothing) r4

test_isSimpleObject :: IO ()
test_isSimpleObject = do
  let f = isSimpleObject
  assertEqual True $ f $ defROA & #properties ?~ M.fromList []
  assertEqual True $ f $ defROA & #properties ?~ M.fromList [] & #typ ?~ "object"
  assertEqual False $ f $ defROA & #typ ?~ "object"
  assertEqual False $ f defROA

test_isInheritingObject :: IO ()
test_isInheritingObject = do
  let f = isInheritingObject
  assertEqual True $ f $ defROA & #allOf ?~ [defROA]
  assertEqual False $ f defROA

test_isUnion :: IO ()
test_isUnion = do
  let f = isUnion
  assertEqual True $ f $ defROA & #oneOf ?~ [defROA]
  assertEqual False $ f defROA

test_isEnum :: IO ()
test_isEnum = do
  let f = isEnum
  assertEqual True $ f $ defROA & #enum ?~ ["A", "B"] & #typ ?~ "string"
  assertEqual False $ f defROA

test_isArray :: IO ()
test_isArray = do
  let f = isArray
  assertEqual True $ f $ defROA & #typ ?~ "array"
  assertEqual False $ f defROA

test_isString :: IO ()
test_isString = do
  let f = isString
  assertEqual True $ f $ defROA & #typ ?~ "string"
  assertEqual False $ f defROA

test_isInteger :: IO ()
test_isInteger = do
  let f = isInteger
  assertEqual True $ f $ defROA & #typ ?~ "integer"
  assertEqual False $ f defROA

test_isNumber :: IO ()
test_isNumber = do
  let f = isNumber
  assertEqual True $ f $ defROA & #typ ?~ "number"
  assertEqual False $ f defROA

test_propToField :: IO ()
test_propToField = do
  let f requiredProps schemaName x = x & propToField defaultSchemaTranslationTable Nothing requiredProps schemaName & evalAppM def
  let e1 = Right $ FieldOA {name = "A", typ = PrimitiveTOA StringPOA, required = RequiredOA, description = Nothing}
  r1 <- f ["A", "B"] Nothing ("A", defROA & #typ ?~ "string")
  assertEqual e1 r1
  --
  r2 <- f [] Nothing ("A", defROA & #typ ?~ "array" & #items ?~ (defROA & #typ ?~ "object"))
  assertLeft r2
  let e2Text = "Failed to convert a property to a field: 'A'.\nThis means the property has invalid or unsupported format. A common mistake is forgetting to use $ref."
  Left r2Text <- return (r2 & mapLeft ((\case ValidationError _ t -> t; x -> tshow x) >>> T.take (T.length e2Text)))
  assertBoolVerbose (show r2) (r2Text == e2Text)

test_validateRequiredReferencesExist :: IO ()
test_validateRequiredReferencesExist = do
  let f = validateRequiredReferencesExist
  let fileName = Just "test.yaml"
  let schemaName = Just "TestSchema"
  assertRight $ f defCfg fileName schemaName (M.fromList []) []
  assertLeft $ f defCfg fileName schemaName (M.fromList []) ["Quark"]
  assertRight $ f defCfg fileName schemaName (M.fromList [("Quark", defROA)]) ["Quark"]
  assertRight $ f defCfg fileName schemaName (M.fromList [("Quark", defROA), ("Proton", defROA)]) ["Quark"]
  assertRight $ f defCfg fileName schemaName (M.fromList [("Quark", defROA), ("Proton", defROA)]) []
  assertLeft $ f defCfg fileName schemaName (M.fromList [("Quark", defROA), ("Proton", defROA)]) ["Electron"]
  assertEqual (Left $ ValidationError (mkErrCtxFromFileAndSchemaNameOrDef fileName schemaName) "`required` is referencing one or more non-existing properties: 'Electron'.") $
    f defCfg fileName schemaName (M.fromList [("Quark", defROA), ("Proton", defROA)]) ["Electron"]

test_processData_simpleObject :: IO ()
test_processData_simpleObject = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let fieldA = FieldOA {name = "A", typ = PrimitiveTOA StringPOA, required = RequiredOA, description = Just "d"}
  let e1 = Right (OAClass {extends = Nothing, fields = [fieldA], tag = Nothing, overrideNonRequiredRepresentation = Nothing}, defOAM & #name .~ "C" & #comment ?~ "c")
  let props1 = M.fromList [("A", defROA & #typ ?~ "string" & #description ?~ "d")]
  let i1 = defROA & #properties ?~ props1 & #typ ?~ "object" & #title ?~ "C" & #required ?~ ["A"] & #comment ?~ "c"
  r1 <- f i1
  assertEqual e1 r1

test_processData_extends :: IO ()
test_processData_extends = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let fieldA = FieldOA {name = "A", typ = PrimitiveTOA StringPOA, required = RequiredOA, description = Nothing}
  let e1 = Right (OAClass {extends = Just "B", fields = [fieldA], tag = Nothing, overrideNonRequiredRepresentation = Nothing}, defOAM & #name .~ "C" & #dependencies .~ ["B"])
  let props1 = M.fromList [("A", defROA & #typ ?~ "string")]
  let inner = defROA & #properties ?~ props1 & #typ ?~ "object" & #title ?~ "C" & #required ?~ ["A"]
  let i1 =
        defROA & #allOf
          ?~ [ defROA & #ref ?~ "B",
               inner
             ]
  r1 <- f i1
  assertEqual e1 r1
  let i2 =
        defROA & #allOf
          ?~ [ defROA & #ref ?~ "B",
               inner & #required ?~ ["XXX"]
             ]
  r2 <- f i2
  assertLeft r2
  skip

roaRef :: Text -> RawOA
roaRef x = defROA & #ref ?~ x

test_processData_union :: IO ()
test_processData_union = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let e1 = Right (OAUnion {members = [SchemaTOA "A", SchemaTOA "B"], tag = Nothing}, defOAM & #name .~ "U" & #dependencies .~ ["A", "B"] & #isRecursive ?~ False)
  let i1 = defROA & #oneOf ?~ [roaRef "A", roaRef "B"] & #title ?~ "U"
  r1 <- f i1
  assertEqual e1 r1

test_processData_taggedUnion :: IO ()
test_processData_taggedUnion = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let e1 = Right (OAUnion {members = [SchemaTOA "A", SchemaTOA "B"], tag = Just "kind"}, defOAM & #name .~ "U" & #dependencies .~ ["A", "B"] & #isRecursive ?~ False)
  let i1 =
        defROA
          & #oneOf ?~ [roaRef "A", roaRef "B"]
          & #title ?~ "U"
          & #discriminator ?~ DiscriminatorROA {propertyName = "kind", mapping = Just $ M.fromList []}
  r1 <- f i1
  assertEqual e1 r1

test_processData_taggedUnionWithMapping :: IO ()
test_processData_taggedUnionWithMapping = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let e1 =
        Right
          ( OAClass {fields = [], extends = Nothing, tag = Just "kind", overrideNonRequiredRepresentation = Nothing},
            defOAM & #name .~ "U" & #taggedUnionMapping ?~ M.fromList [("KEK", "Lol")]
          )
  let i1 =
        defROA
          & #title ?~ "U"
          & #typ ?~ "object"
          & #properties ?~ M.fromList []
          & #discriminator ?~ DiscriminatorROA {propertyName = "kind", mapping = Just $ M.fromList [("KEK", "#/components/schemas/Lol")]}
  r1 <- f i1
  assertEqual e1 r1

test_processData_oneValueEnum :: IO ()
test_processData_oneValueEnum = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  let fieldA = FieldOA {name = "A", typ = OneValueEnumTOA "X", required = RequiredOA, description = Nothing}
  let e1 = Right (OAClass {extends = Nothing, fields = [fieldA], tag = Nothing, overrideNonRequiredRepresentation = Nothing}, defOAM & #name .~ "C")
  let props1 = M.fromList [("A", defROA & #typ ?~ "string" & #enum ?~ ["X"])]
  let i1 = defROA & #properties ?~ props1 & #typ ?~ "object" & #title ?~ "C" & #required ?~ ["A"]
  r1 <- f i1
  assertEqual e1 r1

test_processData_enum :: IO ()
test_processData_enum = do
  let f = processSchemaData def def Nothing Nothing >>> evalAppM def
  r1 <- f $ defROA & #typ ?~ "string" & #enum ?~ ["A"] & #title ?~ "E"
  assertEqual (Right (OAEnum {enumItems = ["A"]}, defOAM & #name .~ "E" & #example ?~ "A" & #isRecursive ?~ False)) r1
  r2 <- f $ defROA & #typ ?~ "string" & #enum ?~ ["A", "B"] & #title ?~ "E"
  assertEqual (Right (OAEnum {enumItems = ["A", "B"]}, defOAM & #name .~ "E" & #example ?~ "A" & #isRecursive ?~ False)) r2

test_processData_titleWarn :: IO ()
test_processData_titleWarn = do
  let f = processSchemaData def def (Just "f.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #typ ?~ "string" & #enum ?~ ["A"]
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "f.yaml") MissingTitleWarning
  assertBool $ e1_warn `elem` r1_warns

test_processData_nnrDeprecationWarn :: IO ()
test_processData_nnrDeprecationWarn = do
  let f = processSchemaData def def (Just "T.yaml") Nothing >>> runAppM def
  r1 <-
    f $
      defROA
        & #typ ?~ "object"
        & #properties ?~ M.fromList [("field", defROA & #typ ?~ "string" & #example ?~ "e")]
        & #xOverrideNonRequiredRepresentation ?~ UndefinedAsNonRequired
        & #title ?~ "T"
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let warnCtx = defEC & #fileName ?~ "T.yaml" & #unitName ?~ "T" & #schemaName ?~ "T"
  let msg = "x-overrideNonRequiredRepresentation is deprecated, please use x-oats-overrideNonRequiredRepresentation. It will be removed in some future version."
  let e1_warn = AppWarning warnCtx $ DeprecationWarning msg
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_processData_inheritingObjectWithoutOwnObjectWarn :: IO ()
test_processData_inheritingObjectWithoutOwnObjectWarn = do
  let f = processSchemaData def def (Just "f.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #allOf ?~ [defROA & #ref ?~ ""]
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "f.yaml" & #unitName ?~ "f" & #schemaName ?~ "f") InheritingObjectWithoutOwnObjectWarning
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_processData_inheritingObjectWithMisplacedRequired :: IO ()
test_processData_inheritingObjectWithMisplacedRequired = do
  let f = processSchemaData def def (Just "f.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #allOf ?~ [defROA & #ref ?~ "X"] & #required ?~ ["Y"]
  let e1 = Left $ ValidationError (defEC & #fileName ?~ "f.yaml") "required is not allowed alongside of allOf (move it to the object schema inside allOf)"
  assertEqual e1 (fst r1)

test_processData_inheritingObjectDescriptionPassing :: IO ()
test_processData_inheritingObjectDescriptionPassing = do
  let f = processSchemaData def def Nothing (Just "E") >>> evalAppM def
  r1e <- f $ defROA & #title ?~ "E" & #allOf ?~ [defROA & #ref ?~ "X", defROA & #typ ?~ "object" & #description ?~ "inner" & #properties ?~ mempty]
  assertRightVerbose (show r1e) r1e
  Right r1 <- pure r1e
  assertEqual (defOAClass & #extends ?~ "X", defOAM & #name .~ "E" & #description ?~ "inner" & #dependencies .~ ["X"]) r1

test_processData_inheritingObjectDescriptionOverwriting :: IO ()
test_processData_inheritingObjectDescriptionOverwriting = do
  let f = processSchemaData def def Nothing (Just "E") >>> evalAppM def
  r1e <- f $ defROA & #title ?~ "E" & #allOf ?~ [defROA & #ref ?~ "X", defROA & #typ ?~ "object" & #description ?~ "inner" & #properties ?~ mempty] & #description ?~ "outer"
  assertRightVerbose (show r1e) r1e
  Right r1 <- pure r1e
  assertEqual (defOAClass & #extends ?~ "X", defOAM & #name .~ "E" & #description ?~ "outer" & #dependencies .~ ["X"]) r1

test_missingExampleWarn_topEnum :: IO ()
test_missingExampleWarn_topEnum = do
  let f = processSchemaData def def (Just "F.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #enum ?~ ["a", "b"] & #typ ?~ "string"
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  assertBoolVerbose (show r1_warns) $ none (\(AppWarning _ x) -> x == MissingExampleWarning) r1_warns

test_missingExampleWarn_topArray :: IO ()
test_missingExampleWarn_topArray = do
  let f = processSchemaData def def (Just "Arr.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #typ ?~ "array" & #title ?~ "Arr" & #items ?~ (defROA & #typ ?~ "string")
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "Arr.yaml" & #unitName ?~ "Arr") MissingExampleWarning
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns
  --
  r2 <- f $ defROA & #typ ?~ "array" & #title ?~ "Arr" & #items ?~ (defROA & #typ ?~ "string" & #example ?~ "\"Holla!\"")
  _ <- assertRight $ fst r2
  let r2_warns = snd r2 ^. #appWarnings
  let e2_warn = AppWarning (defEC & #fileName ?~ "Arr.yaml" & #unitName ?~ "Arr") MissingExampleWarning
  assertBoolVerbose (pShow r2_warns & toS) $ e2_warn `notElem` r2_warns

test_missingExampleWarn_topDecimal :: IO ()
test_missingExampleWarn_topDecimal = do
  let f = processSchemaData def def (Just "Dec.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #typ ?~ "string" & #title ?~ "Dec" & #description ?~ "format: decimal(1,2)"
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "Dec.yaml" & #unitName ?~ "Dec") MissingExampleWarning
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_missingExampleWarn_topString :: IO ()
test_missingExampleWarn_topString = do
  let f = processSchemaData def def (Just "Str.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #typ ?~ "string" & #title ?~ "Str"
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "Str.yaml" & #unitName ?~ "Str") MissingExampleWarning
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_missingExampleWarn_topInteger :: IO ()
test_missingExampleWarn_topInteger = do
  let f = processSchemaData def def (Just "Int.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #typ ?~ "integer" & #title ?~ "Int"
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let e1_warn = AppWarning (defEC & #fileName ?~ "Int.yaml" & #unitName ?~ "Int") MissingExampleWarning
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_unknownType :: IO ()
test_unknownType = do
  let f = processSchemaData def def (Just "A.yaml") Nothing >>> runAppM def
  r1 <- f $ defROA & #title ?~ "A"
  _ <- assertLeft $ fst r1
  let err = fst r1 & fromLeft'
  let eEC = defEC & #fileName ?~ "A.yaml"
  assertEqual (Just eEC) (err ^? _ValidationError % _1)
  let e1Prefix = "Failed to identify input type (typ = Nothing). title = A, fileName = Just \"A.yaml\"."
  let r1ErrorMessage = err ^? _ValidationError % _2 & fromJust & T.take (T.length e1Prefix)
  assertEqual e1Prefix r1ErrorMessage

test_extractTitle :: IO ()
test_extractTitle = do
  let f c m fn schN x = extractTitle c m fn schN x & evalAppM def
  let roaWithoutTitle = defROA & #properties ?~ M.fromList [] & #typ ?~ "object"
  let roaWithTitle = roaWithoutTitle & #title ?~ "obj"
  let fileName = Just "fileName" :: Maybe Text
  let schemaName = Just "schemaName" :: Maybe Text
  let cfgWithoutTitle = defCfg
  let cfgWithTitle = defCfg & #titleFromCmdArgs ?~ "cfg"
  assertEqual (Right "obj") =<< f cfgWithoutTitle SimpleSchemaMode Nothing Nothing roaWithTitle
  assertLeft =<< f cfgWithTitle SimpleSchemaMode Nothing Nothing roaWithTitle
  assertLeft =<< f cfgWithoutTitle SimpleSchemaMode fileName Nothing roaWithTitle
  assertLeft =<< f cfgWithTitle SimpleSchemaMode fileName Nothing roaWithoutTitle
  assertEqual (Right "fileName") =<< f cfgWithoutTitle SimpleSchemaMode fileName Nothing roaWithoutTitle
  assertLeft =<< f cfgWithoutTitle SingleFileMode fileName Nothing roaWithoutTitle
  assertEqual (Right "schemaName") =<< f cfgWithoutTitle SimpleSchemaMode Nothing schemaName roaWithoutTitle
  assertEqual (Right "cfg") =<< f cfgWithTitle SimpleSchemaMode Nothing Nothing roaWithoutTitle
  assertLeft =<< f cfgWithoutTitle SimpleSchemaMode Nothing Nothing roaWithoutTitle
  skip

test_extractDecimalParams :: IO ()
test_extractDecimalParams = do
  let f = extractDecimalParams
  assertEqual Nothing $ f ""
  assertEqual Nothing $ f "x"
  assertEqual (Just (1, 2)) $ f "format: decimal(1, 2)"
  assertEqual (Just (1, 2)) $ f "xxx format: decimal(1, 2) xxx"
  assertEqual (Just (1, 2)) $ f "format:decimal(1,2)"
  assertEqual (Just (1, 2)) $ f "  format:  decimal(  1  ,  2  )  "
  assertEqual (Just (111, 222)) $ f "format: decimal(111, 222)"

test_extractDecimalPlacesFromMultipleOf :: IO ()
test_extractDecimalPlacesFromMultipleOf = do
  let f = extractDecimalPlacesFromMultipleOf
  assertEqual 0 $ f 0
  assertEqual 1 $ f 0.1
  assertEqual 2 $ f 0.01
  assertEqual 3 $ f 0.001
  assertEqual 4 $ f 0.0001
  assertEqual 2 $ f (-0.01)
  assertEqual 1 $ f (-0.1)

rawNumberParam :: RawOAParameter
rawNumberParam =
  RawOAParameter
    { inn = QueryParameterInType,
      schema = defROA & #typ ?~ "number",
      name = "x",
      required = Just True,
      description = Nothing
    }

numberParam :: OAParameter
numberParam =
  OAParameter
    { inn = QueryParameterInType,
      name = "x",
      schema = PrimitiveTOA NumberPOA,
      required = True,
      description = Nothing
    }

test_rawParamToParam :: IO ()
test_rawParamToParam = do
  r1 <- rawParamToParam def Nothing "op" "url" GetRequest >>> runAppMgetResUnsafe $ rawNumberParam
  let exp1 = numberParam
  assertEqual exp1 r1
  ---
  r2 <-
    rawParamToParam def Nothing "op" "url" GetRequest >>> evalAppM def $
      RawOAParameter {inn = QueryParameterInType, schema = defROA & #typ ?~ "object", name = "x", required = Just True, description = Nothing}
  assertEqual (Left $ ValidationError (def & #unitName ?~ "op" & #endpointOperationId ?~ "op" & #endpointUrl ?~ "url" & #endpointRequestType ?~ GetRequest) "Failed to get type of schema in parameter \"x\" (possibly unsupported schema type)") r2

rawRefResponse :: RawOARequestOrResponse
rawRefResponse =
  def
    & #content ?~ M.fromList [(mimeAppJson, RawOAContentItem {schema = defROA & #ref ?~ "REF"})]
    & #description ?~ "desc"

rawNumberResponse :: RawOARequestOrResponse
rawNumberResponse =
  def
    & #content ?~ M.fromList [(mimeAppJson, RawOAContentItem {schema = defROA & #typ ?~ "number"})]
    & #description ?~ "desc"

refResponse :: OARequestOrResponse
refResponse =
  OARequestOrResponse
    { mime = Just mimeAppJson,
      schema = Just $ SchemaTOA "REF",
      description = Just "desc"
    }

test_rawRequestOrResponseToResponse :: IO ()
test_rawRequestOrResponseToResponse = do
  let d1 = rawRefResponse
  r1E <- rawRequestOrResponseToResponse def Nothing RequestDirection "a/b" GetRequest def d1 & evalAppM def
  _ <- assertRight r1E
  Right r1 <- pure r1E
  let exp1 = refResponse
  assertEqual exp1 r1
  ---
  r2E <- rawRequestOrResponseToResponse def Nothing RequestDirection "a/b" GetRequest def rawNumberResponse & runAppM def
  _ <- assertRightVerbose (show r2E) $ fst r2E
  let st = snd r2E
  let exp2 = [NonCriticalError (def & #endpointOperationId ?~ "?" & #unitName ?~ "?" & #endpointUrl ?~ "a/b" & #endpointCommunicationDirection ?~ RequestDirection & #endpointRequestType ?~ GetRequest) (PrimitiveTypeInRequestOrResponseBodyError RequestDirection rawNumberResponse)]
  assertEqual exp2 (st ^. #appErrors)
  return ()

test_rawPathToEndpoint :: IO ()
test_rawPathToEndpoint = do
  let path1 =
        def
          & #tags .~ ["Tag"]
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam]
  r1E <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path1) & evalAppM def
  _ <- assertRight r1E
  Right r1 <- pure r1E
  let exp1 = def & #tag .~ "Tag" & #responses .~ M.fromList [(200, refResponse)] & #parameters .~ [numberParam]
  assertEqual (GetRequest, exp1) r1
  ---
  let path2 =
        def
          & #tags .~ []
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam]
  r2E <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path2) & evalAppM def
  assertEqual (Left $ ValidationError (def & #unitName ?~ "?" & #endpointOperationId ?~ "?" & #endpointUrl ?~ "a/b" & #endpointRequestType ?~ GetRequest) "Path is missing a tag.") r2E
  ---
  let path3 =
        def
          & #tags .~ ["Tag"]
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam & #inn .~ PathParameterInType & #required .~ Nothing]
  r3E <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path3) & evalAppM def
  assertEqual (Left $ ValidationError (def & #unitName ?~ "?" & #endpointOperationId ?~ "?" & #endpointUrl ?~ "a/b" & #endpointRequestType ?~ GetRequest) "Parameter \"x\" has invalid/missing value in \"required\" field.") r3E
  ---
  let path4 =
        def
          & #tags .~ ["Tag"]
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam, rawNumberParam]
  r4E <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path4) & evalAppM def
  assertEqual (Left $ ValidationError (def & #unitName ?~ "?" & #endpointOperationId ?~ "?" & #endpointUrl ?~ "a/b" & #endpointRequestType ?~ GetRequest) "Duplicate parameters: x.") r4E

test_rawPathToEndpoint_metaDeprecationWarn :: IO ()
test_rawPathToEndpoint_metaDeprecationWarn = do
  let path1 =
        def
          & #tags .~ ["Tag"]
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam]
          & #xMeta ?~ AT.String "Haru"
  r1 <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path1) & runAppM def
  _ <- assertRight $ fst r1
  let r1_warns = snd r1 ^. #appWarnings
  let msg = "x-meta is deprecated, please use x-oats-meta. It will be removed in some future version."
  let warnCtx = defEC & #unitName ?~ "?" & #endpointOperationId ?~ "?" & #endpointUrl ?~ "a/b" & #endpointRequestType ?~ GetRequest
  let e1_warn = AppWarning warnCtx $ DeprecationWarning msg
  assertBoolVerbose (pShow r1_warns & toS) $ e1_warn `elem` r1_warns

test_rawPathToEndpoint_cookieParamWarn :: IO ()
test_rawPathToEndpoint_cookieParamWarn = do
  let path =
        def
          & #tags .~ ["Tag"]
          & #responses .~ M.fromList [(200, rawRefResponse)]
          & #parameters ?~ [rawNumberParam & #inn .~ CookieParameterInType]
  r <- rawPathToEndpoint def Nothing "a/b" (GetRequest, path) & runAppM def
  _ <- assertRight $ fst r
  let r_warns = snd r ^. #appWarnings
  let warnCtx = defEC & #unitName ?~ "?" & #endpointOperationId ?~ "?" & #endpointUrl ?~ "a/b" & #endpointRequestType ?~ GetRequest
  let e_warn = AppWarning warnCtx CookieParameterWarning
  assertBoolVerbose (pShow r_warns & toS) $ e_warn `elem` r_warns

test_rawPathsToRoute :: IO ()
test_rawPathsToRoute = do
  let path1 = def & #tags .~ ["Tag"] & #responses .~ M.fromList [(200, rawRefResponse)] & #parameters ?~ [rawNumberParam]
  let rawPaths :: RawOAPaths = M.fromList [(GetRequest, path1)]
  r1E <- rawPathsToRoute def Nothing ("a/b", rawPaths) & evalAppM def
  _ <- assertRight r1E
  Right r1 <- pure r1E
  let expEndpoint1 = def & #tag .~ "Tag" & #responses .~ M.fromList [(200, refResponse)] & #parameters .~ [numberParam]
  let exp1 = ("a/b", M.fromList [(GetRequest, expEndpoint1)])
  assertEqual exp1 r1

test_processOperationIdTagPairUniqueness :: IO ()
test_processOperationIdTagPairUniqueness = do
  let f = processOperationIdTagPairUniqueness
  let ep1 = def & #operationId .~ "EP1" & #tag .~ "TAG1"
  let ep1' = def & #operationId .~ "EP1" & #tag .~ "TAG2"
  -- let ep2 = def & #operationId .~ "EP2" & #tag .~ "TAG1"
  let ep2' = def & #operationId .~ "EP2" & #tag .~ "TAG2"
  let ep3 = def & #operationId .~ "EP3" & #tag .~ "TAG1"
  -- let ep3'' = def & #operationId .~ "EP3" & #tag .~ "TAG3"
  let route1 = M.fromList [(GetRequest, ep1)]
  let route2 = M.fromList [(PostRequest, ep2'), (PatchRequest, ep1')]
  let route3 = M.fromList [(PutRequest, ep3)]
  let db1 = M.fromList [("url1", route1), ("url2", route2), ("url3", route3)]
  let ctx1 = def & #config % #endpointsGenerationMode ?~ AllEndpointsInOneFile
  r1E <- f ctx1 [] db1 "url1" route1 & evalAppM def
  _ <- assertLeft r1E
  Left r1 <- pure r1E
  let errMsg1 = r1 & formatAppError & T.intercalate "\n"
  assertBool $ T.isInfixOf "EP1" errMsg1
  assertBool $ T.isInfixOf "TAG1" errMsg1
  assertBoolVerbose (toS errMsg1) $ not $ T.isInfixOf "EP3" errMsg1
  assertBool $ not $ T.isInfixOf "TAG3" errMsg1
  assertBool $ T.isInfixOf "url1" errMsg1
  assertBool $ T.isInfixOf "url2" errMsg1
  assertBool $ not $ T.isInfixOf "url3" errMsg1
  let ctx2 = def & #config % #endpointsGenerationMode ?~ EndpointsWithSameTagInOneFile
  r2E <- f ctx2 [] db1 "url1" route1 & evalAppM def
  _ <- assertRight r2E
  return ()

test_processTagValidity :: IO ()
test_processTagValidity = do
  let f = processTagValidity
  let tag1 = "TAG 1"
  let ep1tag1 = def & #operationId .~ "EP1" & #tag .~ tag1
  let route1 = M.fromList [(GetRequest, ep1tag1)]
  let db1 = M.fromList [("url1", route1)]
  let ctx1 = def & #config % #endpointsGenerationMode ?~ AllEndpointsInOneFile & #mode .~ SingleFileMode
  r1E <- f ctx1 [] db1 "url1" route1 & evalAppM def
  _ <- assertLeft r1E
  Left r1 <- pure r1E
  let errMsg1 = r1 & formatAppError & T.intercalate "\n"
  assertBool $ T.isInfixOf "EP1" errMsg1
  assertBool $ T.isInfixOf tag1 errMsg1
  --
  r2E <- f ctx1 [OATag {name = tag1, description = Nothing}] db1 "url1" route1 & evalAppM def
  _ <- assertRight r2E
  return ()

test_getDependentSchemas_simple :: IO ()
test_getDependentSchemas_simple = do
  let f = getDependentSchemas
  let schemaA = defOAClass
  let metaA = defOAM & #name .~ "A"
  let schemaC = defOAClass
  let metaC = defOAM & #name .~ "C" & #dependencies .~ ["D"]
  let schemaD = defOAClass
  let metaD = defOAM & #name .~ "D" & #dependencies .~ ["E"]
  let schemaE = defOAClass
  let metaE = defOAM & #name .~ "E" & #dependencies .~ ["C"]
  let db = [(schemaA, metaA), (schemaC, metaC), (schemaD, metaD), (schemaE, metaE)]
  assertEqual [(schemaE, metaE)] $ f db (schemaC, metaC)
  assertEqual [(schemaC, metaC)] $ f db (schemaD, metaD)
  assertEqual [(schemaD, metaD)] $ f db (schemaE, metaE)
  assertEqual [] $ f db (schemaA, metaA)

test_calcRecursiveSchemas_simple :: IO ()
test_calcRecursiveSchemas_simple = do
  let f = calcRecursiveSchemas
  let schemaA = defOAClass
  let metaA = defOAM & #name .~ "A" & #dependencies .~ ["B"]
  let schemaB = defOAClass
  let metaB = defOAM & #name .~ "B" & #dependencies .~ ["C"]
  let schemaC = defOAClass
  let metaC = defOAM & #name .~ "C"
  assertEqual [] $ f []
  assertEqual [] $ f [(schemaA, metaA)]
  assertEqual [] $ f [(schemaA, metaA), (schemaB, metaB), (schemaC, metaC)]
  assertEqual [] $ f [(schemaB, metaB), (schemaC, metaC), (schemaA, metaA)]

test_calcRecursiveSchemas_direct :: IO ()
test_calcRecursiveSchemas_direct = do
  let f = calcRecursiveSchemas
  let schemaA = defOAClass
  let metaA = defOAM & #name .~ "A"
  -- B -> B
  let schemaB = defOAClass
  let metaB = defOAM & #name .~ "B" & #dependencies .~ ["B"]
  assertEqual ["B"] $ f [(schemaB, metaB)]
  assertEqual ["B"] $ f [(schemaA, metaA), (schemaB, metaB)]

test_calcRecursiveSchemas_indirectSimple :: IO ()
test_calcRecursiveSchemas_indirectSimple = do
  let f = calcRecursiveSchemas
  -- C -> D -> C
  let schemaC = defOAClass
  let metaC = defOAM & #name .~ "C" & #dependencies .~ ["D"]
  let schemaD = defOAClass
  let metaD = defOAM & #name .~ "D" & #dependencies .~ ["C"]
  assertEqual ["C", "D"] $ f [(schemaC, metaC), (schemaD, metaD)]

test_calcRecursiveSchemas_indirectComplex :: IO ()
test_calcRecursiveSchemas_indirectComplex = do
  -- A -> B -> C -> A, A -> A, C -> D -> I, E, F -> G -> F, G -> H -> B
  let f = calcRecursiveSchemas
  let schemaA = defOAClass
  let metaA = defOAM & #name .~ "A" & #dependencies .~ ["B", "A"]
  let schemaB = defOAClass
  let metaB = defOAM & #name .~ "B" & #dependencies .~ ["C"]
  let schemaC = defOAClass
  let metaC = defOAM & #name .~ "C" & #dependencies .~ ["A", "D"]
  let schemaD = defOAClass
  let metaD = defOAM & #name .~ "D" & #dependencies .~ ["I"]
  let schemaE = defOAClass
  let metaE = defOAM & #name .~ "E"
  let schemaF = defOAClass
  let metaF = defOAM & #name .~ "F" & #dependencies .~ ["G"]
  let schemaG = defOAClass
  let metaG = defOAM & #name .~ "G" & #dependencies .~ ["F"]
  let schemaH = defOAClass
  let metaH = defOAM & #name .~ "H" & #dependencies .~ ["B"]
  let schemaK = defOAClass
  let metaK = defOAM & #name .~ "K" & #dependencies .~ ["H"]
  let schemaI = defOAClass
  let metaI = defOAM & #name .~ "I" & #dependencies .~ []
  let db =
        [ (schemaA, metaA),
          (schemaB, metaB),
          (schemaC, metaC),
          (schemaD, metaD),
          (schemaE, metaE),
          (schemaF, metaF),
          (schemaG, metaG),
          (schemaH, metaH),
          (schemaK, metaK),
          (schemaI, metaI)
        ]
  assertEqual ["A", "B", "C", "F", "G"] $ f db
