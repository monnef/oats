{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module RangeUtilsSpec
  ( htf_thisModulesTests,
  )
where

import Test.Framework
import Prelude hiding (exp)
import RangeUtils

test_calcLimitVal_noop :: IO ()
test_calcLimitVal_noop = do
  let f = calcLimitVal

  assertEqual 1.0 $ f Min 0.1 (Inclusive, Inclusive) 1.0
  assertEqual 1.0 $ f Max 0.1 (Inclusive, Inclusive) 1.0
  assertEqual 1.0 $ f Min 0.1 (Exclusive, Exclusive) 1.0
  assertEqual 1.0 $ f Max 0.1 (Exclusive, Exclusive) 1.0

test_calcLimitVal_accuracy :: IO ()
test_calcLimitVal_accuracy = do
  let f = calcLimitVal

  assertEqual (-0.99) $ f Min 0.01 (Inclusive, Exclusive) (-1.00)
  assertEqual 1.01 $ f Min 0.01 (Inclusive, Exclusive) 1.00

  assertEqual (-0.999) $ f Min 0.001 (Inclusive, Exclusive) (-1.000)
  assertEqual 1.001 $ f Min 0.001 (Inclusive, Exclusive) 1.000

  assertEqual 1.000 $ f Min 0.001 (Exclusive, Exclusive) 1.000

  assertEqual (-0.9999) $ f Min 0.0001 (Inclusive, Exclusive) (-1.0000)
  assertEqual 1.0001 $ f Min 0.0001 (Inclusive, Exclusive) 1.0000

  assertEqual (-0.99999) $ f Min 0.00001 (Inclusive, Exclusive) (-1.00000)
  assertEqual 1.00001 $ f Min 0.00001 (Inclusive, Exclusive) 1.00000

  assertEqual (-0.999999) $ f Min 0.000001 (Inclusive, Exclusive) (-1.000000)
  assertEqual 1.000001 $ f Min 0.000001 (Inclusive, Exclusive) 1.000000

  assertEqual (-0.9999999) $ f Min 0.0000001 (Inclusive, Exclusive) (-1.0000000)
  assertEqual 1.0000001 $ f Min 0.0000001 (Inclusive, Exclusive) 1.0000000

  assertEqual (-0.99999999) $ f Min 0.00000001 (Inclusive, Exclusive) (-1.00000000)
  assertEqual 1.00000001 $ f Min 0.00000001 (Inclusive, Exclusive) 1.00000000

test_calcLimitVal_min_ex2in :: IO ()
test_calcLimitVal_min_ex2in = do
  let f = calcLimitVal
  assertEqual 1.0 $ f Min 0.1 (Exclusive, Inclusive) 0.9

test_calcLimitVal_min_in2ex :: IO ()
test_calcLimitVal_min_in2ex = do
  let f = calcLimitVal
  assertEqual 1.1 $ f Min 0.1 (Inclusive, Exclusive) 1.0

test_calcLimitVal_min_ex2ex :: IO ()
test_calcLimitVal_min_ex2ex = do
  let f = calcLimitVal
  assertEqual 1.0 $ f Min 0.1 (Exclusive, Exclusive) 1.0

test_calcLimitVal_min_in2in :: IO ()
test_calcLimitVal_min_in2in = do
  let f = calcLimitVal
  assertEqual 1.0 $ f Min 0.1 (Inclusive, Inclusive) 1.0

test_calcLimitVal_max_ex2in :: IO ()
test_calcLimitVal_max_ex2in = do
  let f = calcLimitVal
  assertEqual 0.9 $ f Max 0.1 (Exclusive, Inclusive) 1.0

test_calcLimitVal_max_in2ex :: IO ()
test_calcLimitVal_max_in2ex = do
  let f = calcLimitVal
  assertEqual 0.9 $ f Max 0.1 (Inclusive, Exclusive) 1.0

test_calcLimitVal_max_in2in :: IO ()
test_calcLimitVal_max_in2in = do
  let f = calcLimitVal
  assertEqual 1.0 $ f Max 0.1 (Inclusive, Inclusive) 1.0

test_calcLimitVal_max_ex2ex :: IO ()
test_calcLimitVal_max_ex2ex = do
  let f = calcLimitVal
  assertEqual 1.0 $ f Max 0.1 (Exclusive, Exclusive) 1.0

test_calcLimitVal_max_numCustomLimitTemplateOnlyMax :: IO ()
test_calcLimitVal_max_numCustomLimitTemplateOnlyMax = do
  let f = calcLimitVal
  assertEqual 10.00 $ f Max 0.01 (Exclusive, Exclusive) 10.00
