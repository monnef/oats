{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module CrawlerSpec
  ( htf_thisModulesTests,
  )
where

import Optics
import Crawler
import Data
import Data.Default (def)
import Test.Framework
import Text.RE.PCRE.Text (compileRegex)
import Utils

testDir :: FilePath
testDir = "test/data"

testDirCrawlerApi :: FilePath
testDirCrawlerApi = testDir <> "/crawler/api"

testDirCrawlerCustomSchemas :: FilePath
testDirCrawlerCustomSchemas = testDir <> "/crawler/custom"

test_findFilesWithExtension :: IO ()
test_findFilesWithExtension = do
  let f = findFilesWithExtension ".yaml"
  r1 <- f testDirCrawlerApi
  assertEqual ["yaml.yaml"] r1
  r2 <- f testDir
  let r2str = pShow r2 & toS
  assertBoolVerbose r2str $ "crawler/api/yaml.yaml" `elem` r2
  assertBoolVerbose r2str $ "union.yaml" `elem` r2

runCrawl :: WorkContext -> IO (Either AppError CrawlResult)
runCrawl ctx = crawl ctx & evalAppM def

test_crawl_trivial :: IO ()
test_crawl_trivial = do
  let ctx = def & #config % #apiPath ?~ toS testDirCrawlerApi & #config % #customSchemasPath ?~ toS testDirCrawlerCustomSchemas
  r <- runCrawl ctx
  assertEqual (Right $ CrawlResult {sourceFiles = ["yaml.yaml"], customSchemaFiles = ["CustomSchema.ts"]}) r

test_crawl_ignoreRegex :: IO ()
test_crawl_ignoreRegex = do
  let ctx = def & #config % #apiPath ?~ toS testDir & #config % #ignoreFilesRegex .~ compileRegex "[aec].*\\."
  r <- runCrawl ctx
  assertEqual (Right $ CrawlResult {sourceFiles = ["union.yaml"], customSchemaFiles = []}) r
