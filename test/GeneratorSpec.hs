{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module GeneratorSpec
  ( htf_thisModulesTests,
  )
where

import Data
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import qualified Data.Text as T
import Generator (FieldLocation (..), extractSuccessResponse, fieldToIoTs, fieldToTs, genMultilineSingleLineComment, genNumberRegex, genTsInterface, isSimpleFieldName, rawTagToTagId)
import Optics
import Test.Framework
import UnliftIO (liftIO)
import Prelude hiding (exp)

test_extractSuccessResponse :: IO ()
test_extractSuccessResponse = do
  let f = extractSuccessResponse
  let r1 = def & #description ?~ "r1"
  let r2 = def & #description ?~ "r2"
  let r3 = def & #description ?~ "r3"
  assertEqual Nothing $ f $ M.fromList []
  assertEqual Nothing $ f $ M.fromList [(301, r1)]
  assertEqual (Just (200, r1)) $ f $ M.fromList [(200, r1)]
  assertEqual (Just (201, r1)) $ f $ M.fromList [(301, r2), (201, r1)]
  assertEqual (Just (201, r1)) $ f $ M.fromList [(301, r2), (201, r1), (100, r2)]
  assertEqual (Just (200, r3)) $ f $ M.fromList [(301, r2), (201, r1), (100, r2), (200, r3)]

test_rawTagToTagId :: IO ()
test_rawTagToTagId = do
  let f = rawTagToTagId
  assertEqual "Car" $ f "Car"
  assertEqual "CarLists" $ f "Car lists"
  assertEqual "OperationalAndEconomicsIndicators" $ f "Operational and economics indicators"
  assertEqual "QuestionnaireSubjectsBreeders" $ f "Questionnaire subjects, breeders"

test_genMultilineSingleLineComment :: IO ()
test_genMultilineSingleLineComment = do
  let f = genMultilineSingleLineComment
  assertEqual [] $ f ""
  assertEqual ["// Liz"] $ f "Liz"
  assertEqual ["// 良い"] $ f "良い"
  assertEqual ["// 尸ヨ尸ヨ冊　句回し回尺　己工卞　丹冊ヨ卞，　亡回几己ヨ亡卞ヨ卞凵尺　丹句工尸工己亡工几呂　ヨし工卞．", "// 尸ヨ尸ヨ冊　句回し回尺　己工卞　丹冊ヨ卞，　亡回几己ヨ亡卞ヨ卞凵尺　丹句工尸工己亡工几呂　ヨし工卞．"] $ f "尸ヨ尸ヨ冊　句回し回尺　己工卞　丹冊ヨ卞，　亡回几己ヨ亡卞ヨ卞凵尺　丹句工尸工己亡工几呂　ヨし工卞．\n尸ヨ尸ヨ冊　句回し回尺　己工卞　丹冊ヨ卞，　亡回几己ヨ亡卞ヨ卞凵尺　丹句工尸工己亡工几呂　ヨし工卞．"
  assertEqual ["// a", "// b"] $ f "a\nb"
  assertEqual ["// a", "// b"] $ f "a\nb\n"

defOAClass :: OA
defOAClass = OAClass {extends = Nothing, fields = [], tag = Nothing, overrideNonRequiredRepresentation = Nothing}

test_isSimpleFieldName :: IO ()
test_isSimpleFieldName = do
  let f = isSimpleFieldName
  assertEqual True $ f "x"
  assertEqual True $ f "Inu"
  assertEqual True $ f "arr64"
  assertEqual False $ f "4"
  assertEqual False $ f "4a"
  assertEqual True $ f "_"
  assertEqual True $ f "_4_"
  assertEqual False $ f "spa ce"
  assertEqual False $ f "TA\tB"
  assertEqual False $ f "z.y.x"

test_fieldToIoTs_fieldNameHandling :: IO ()
test_fieldToIoTs_fieldNameHandling = do
  let f = fieldToIoTs def FieldLocationRequired defOAClass
  assertEqual (Right ["a: t.number,"]) $ f (FieldOA {name = "a", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"ぁ\": t.number,"]) $ f (FieldOA {name = "ぁ", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"name.first\": t.number,"]) $ f (FieldOA {name = "name.first", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"what a bad\\tfield name!\": t.number,"]) $ f (FieldOA {name = "what a bad\tfield name!", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["a1: t.number,"]) $ f (FieldOA {name = "a1", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"1\": t.number,"]) $ f (FieldOA {name = "1", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"žluťoučkýKůň\": t.number,"]) $ f (FieldOA {name = "žluťoučkýKůň", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual (Right ["\"\\\"\": t.number,"]) $ f (FieldOA {name = "\"", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})

test_fieldToIoTs_comments :: IO ()
test_fieldToIoTs_comments = do
  let f = fieldToIoTs def FieldLocationRequired defOAClass
  assertEqual (Right ["a: t.number, // d"]) $ f (FieldOA {name = "a", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Just "d"})
  assertEqual (Right ["// d", "// e", "a: t.number,"]) $ f (FieldOA {name = "a", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Just "d\ne"})
  let longStr = T.replicate 100 "x"
  assertEqual (Right ["// " <> longStr, "a: t.number,"]) $ f (FieldOA {name = "a", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Just longStr})

test_fieldToTs_simple :: IO ()
test_fieldToTs_simple = do
  let f = fieldToTs NullAsNonRequired
  assertEqual ["age: number;"] $ f (FieldOA {name = "age", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Nothing})
  assertEqual ["\"1\": ThisShouldBeAnArrayItem;"] $ f (FieldOA {name = "1", typ = SchemaTOA "ThisShouldBeAnArrayItem", required = RequiredOA, description = Nothing})
  assertEqual ["isMale: boolean;"] $ f (FieldOA {name = "isMale", typ = PrimitiveTOA BooleanPOA, required = RequiredOA, description = Nothing})
  assertEqual ["ratings: number[];"] $ f (FieldOA {name = "ratings", typ = ArrayTOA (PrimitiveTOA NumberPOA), required = RequiredOA, description = Nothing})
  assertEqual ["age: number; // Age"] $ f (FieldOA {name = "age", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Just "Age"})
  assertEqual ["// 1", "// 2", "age: number;"] $ f (FieldOA {name = "age", typ = PrimitiveTOA NumberPOA, required = RequiredOA, description = Just "1\n2"})

test_fieldToTs_nrrNull :: IO ()
test_fieldToTs_nrrNull = do
  let f = fieldToTs NullAsNonRequired
  assertEqual ["age: number | null;"] $ f (FieldOA {name = "age", typ = PrimitiveTOA NumberPOA, required = OptionalOA, description = Nothing})
  assertEqual ["ratings: number[] | null;"] $ f (FieldOA {name = "ratings", typ = ArrayTOA (PrimitiveTOA NumberPOA), required = OptionalOA, description = Nothing})

test_fieldToTs_nrrUndefined :: IO ()
test_fieldToTs_nrrUndefined = do
  let f = fieldToTs UndefinedAsNonRequired
  assertEqual ["age?: number;"] $ f (FieldOA {name = "age", typ = PrimitiveTOA NumberPOA, required = OptionalOA, description = Nothing})
  assertEqual ["ratings?: number[];"] $ f (FieldOA {name = "ratings", typ = ArrayTOA (PrimitiveTOA NumberPOA), required = OptionalOA, description = Nothing})

test_genTsInterface_nrrNull :: IO ()
test_genTsInterface_nrrNull = do
  let f = \name xs -> genTsInterface def name xs & evalAppM def
  let e1 = "export interface User {\n  name: string; // NAME\n  // Children?\n  // Hmm...\n  hasChildren: boolean | null;\n  weight: number | null;\n  ratings: number[];\n}"
  let fields1 =
        [ FieldOA {name = "name", typ = PrimitiveTOA StringPOA, required = RequiredOA, description = Just "NAME"},
          FieldOA {name = "hasChildren", typ = PrimitiveTOA BooleanPOA, required = OptionalOA, description = Just "Children?\nHmm..."},
          FieldOA {name = "weight", typ = PrimitiveTOA NumberPOA, required = OptionalOA, description = Nothing},
          FieldOA {name = "ratings", typ = ArrayTOA (PrimitiveTOA NumberPOA), required = RequiredOA, description = Nothing}
        ]
  res1 <- liftIO $ f "User" fields1
  assertEqual (Right e1) res1

test_genTsInterface_nrrUndefined :: IO ()
test_genTsInterface_nrrUndefined = do
  let ctx = def & #config % #nonRequiredRepresentation .~ UndefinedAsNonRequired
  let f = \name xs -> genTsInterface ctx name xs & evalAppM def
  let e1 = "export interface User {\n  name: string;\n  hasChildren?: boolean;\n  weight?: number;\n  ratings: number[];\n}"
  let fields1 =
        [ FieldOA {name = "name", typ = PrimitiveTOA StringPOA, required = RequiredOA, description = Nothing},
          FieldOA {name = "hasChildren", typ = PrimitiveTOA BooleanPOA, required = OptionalOA, description = Nothing},
          FieldOA {name = "weight", typ = PrimitiveTOA NumberPOA, required = OptionalOA, description = Nothing},
          FieldOA {name = "ratings", typ = ArrayTOA (PrimitiveTOA NumberPOA), required = RequiredOA, description = Nothing}
        ]
  res1 <- liftIO $ f "User" fields1
  assertEqual (Right e1) res1

test_genNumberRegex :: IO ()
test_genNumberRegex = do
  let f = genNumberRegex

  assertEqual "^\\d{1}$" $ f 0 9 False False 1 0
  assertEqual "^\\d{1,4}$" $ f 0 9999 False False 1 0
  assertEqual "^\\d{1}$" $ f 0 10 False True 1 0
  assertEqual "^\\d{1,2}$" $ f 0 10 False False 1 0

  assertEqual "^\\d{1}[,.]\\d{1}$" $ f 0.1 9.9 False False 0.1 1
  assertEqual "^\\d{1,3}[,.]\\d{2}$" $ f 0.01 999.99 False False 0.01 2

  assertEqual "^-?\\d{1,3}[,.]\\d{2}$" $ f (-999.99) 999.99 False False 0.01 2
  assertEqual "^-?\\d{1,3}[,.]\\d{2}$" $ f (-1000.00) 1000.00 True True 0.01 2
