{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module IgnoreRulesSpec
  ( htf_thisModulesTests,
  )
where

import Optics hiding (re)
import Data
import Data.Default (def)
import IgnoreRules
import Test.Framework
import Text.RE.PCRE.Text (re)

test_doesIgnoreRuleSelectorMatch :: IO ()
test_doesIgnoreRuleSelectorMatch = do
  let f = doesIgnoreRuleSelectorMatch
  let irSchemaA = IgnoreRuleSchemaByName {schemaNameRegex = [re|a+|]}
  assertEqual False $ f irSchemaA def
  assertEqual False $ f irSchemaA (def & #schemaName ?~ "bb")
  assertEqual True $ f irSchemaA (def & #schemaName ?~ "aa")
  let irUrlA = IgnoreRuleEndpointByUrl {urlRegex = [re|a+|]}
  assertEqual False $ f irUrlA def
  assertEqual False $ f irUrlA (def & #endpointUrl ?~ "bb")
  assertEqual True $ f irUrlA (def & #endpointUrl ?~ "aa")
  let irOpIdA = IgnoreRuleEndpointByOperationId {operationIdRegex = [re|a+|]}
  assertEqual False $ f irOpIdA def
  assertEqual False $ f irOpIdA (def & #endpointOperationId ?~ "bb")
  assertEqual True $ f irOpIdA (def & #endpointOperationId ?~ "aa")

test_doesIgnoreRuleTargetMatch :: IO ()
test_doesIgnoreRuleTargetMatch = do
  let f = doesIgnoreRuleTargetMatch
  ---
  let primInBodyErr = IRError $ NonCriticalError def $ PrimitiveTypeInRequestOrResponseBodyError RequestDirection def
  assertEqual False $ f def primInBodyErr
  assertEqual False $ f (def & #errors .~ ["X"] & #warnings .~ ["X"]) primInBodyErr
  assertEqual True $ f (def & #errors .~ ["PrimitiveTypeInRequestOrResponseBody"] & #warnings .~ ["X"]) primInBodyErr
  ---
  let missTitleWarn = IRWarning $ AppWarning def MissingTitleWarning
  assertEqual False $ f def missTitleWarn
  assertEqual False $ f (def & #errors .~ ["X"] & #warnings .~ ["X"]) missTitleWarn
  assertEqual True $ f (def & #errors .~ ["X"] & #warnings .~ ["MissingTitle"]) missTitleWarn
