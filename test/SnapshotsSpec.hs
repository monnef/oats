{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module SnapshotsSpec
  ( htf_thisModulesTests,
  )
where

import Control.Monad.Extra (whenM)
import Data
import Data.Algorithm.Diff (Diff, getDiff)
import Data.Algorithm.DiffOutput (ppDiff)
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import Data.Maybe (fromMaybe)
import qualified Data.Text as T
import Optics
import System.Console.ANSI
import System.Directory.Extra (doesFileExist)
import System.Environment (getArgs, lookupEnv)
import Test.Framework
import Utils
import Worker (singleSchemaYamlToTypeScript)

genSnapshotFileName :: Text -> Text -> Text
genSnapshotFileName testName subtitle = "test/snapshots/" <> testName <> (if T.null subtitle then "" else "_" <> subtitle) <> ".snapshot.ts"

genInputFileName :: Text -> Text
genInputFileName name = "test/data/" <> name <> ".yaml"

assertSnapshotMatches :: Text -> Text -> Maybe Config -> IO ()
assertSnapshotMatches testName subtitle configMay = do
  inputData <- tReadFile $ genInputFileName testName
  let ctx = WorkContext {debugLog = const skip, config = fromMaybe def configMay, metas = [], mode = SimpleSchemaMode, skipOnDryRunAndPrintAlways = const $ const skip, skipOnDryRunAndPrintInVerbose = const $ const skip}
  outputMay <- singleSchemaYamlToTypeScript SimpleSchemaMode ctx Nothing (toS inputData) & evalAppM def
  let fullTestName = testName <> (if subtitle == "" then "" else "_" <> subtitle)
  case outputMay of
    Left e -> do
      printHeader
      tPutStrLn fullTestName
      mapM_ tPutStrLn $ formatAppError e
      assertRight outputMay
      return ()
    Right _ -> skip
  let output = orCrash outputMay
  let snapPath = genSnapshotFileName testName subtitle
  snapExists <- doesFileExist $ toS snapPath
  whenM ciRun $ assertBoolVerbose [qq|Snapshot is missing: $snapPath|] snapExists
  if snapExists
    then do
      snapData <- tReadFile snapPath
      when (snapData /= output) $ do
        let dif :: [Diff [String]] = getDiff (toS snapData & lines <&> (: [])) (toS output & lines <&> (: []))
        let str = ppDiff dif
        putStrLn str
        putStrLn "--- ---"
        tPutStrLn output
        putStrLn "--- ---"
      assertEqualVerbose (toS snapPath) snapData output
      whenM verboseRun $ do
        printHeader
        tPutStrLn fullTestName
        printOkLn
    else do
      tWriteFile snapPath output
      printHeader
      tPutStrLn fullTestName
      printOk
      printOkNote [qq| Created new snapshot $snapPath:$nl$nl$output$nl$nl|]
  where
    verboseRun = getArgs <&> elem "-q" <&> not
    ciRun = lookupEnv "OATS_FORBID_SNAPSHOT_CREATION" <&> (== Just "1")
    printHeader = do
      setSGR [SetColor Foreground Vivid White]
      tPutStr "[Snapshot TEST] "
      setSGR [Reset]
    printOk = do
      tPutStr "^- "
      setSGR [SetColor Foreground Dull Green]
      tPutStr "+++ OK"
      setSGR [Reset]
    printOkLn = printOk >> tPutStrLn ""
    printOkNote msg = do
      setSGR [SetColor Foreground Vivid Black]
      putStr msg
      setSGR [Reset]

defCfg :: Config
defCfg = def

test_objects :: IO ()
test_objects = do
  assertSnapshotMatches "trivial_object" "" Nothing
  assertSnapshotMatches "simple_inheritance" "" Nothing
  assertSnapshotMatches "trivial_object" "nonReqAsUndef" (Just $ (def :: Config) & #nonRequiredRepresentation .~ UndefinedAsNonRequired)
  assertSnapshotMatches "trivial_object" "propNameMap" (Just $ (def :: Config) & #propertyNameMap .~ M.fromList [("username", "foo"), ("email", "bar"), ("id", "baz")])
  assertSnapshotMatches "trivial_object" "excess" (Just $ (def :: Config) & #useExcess .~ True & #excessImport ?~ "import {excess} from 'ts-io-excess';")
  assertSnapshotMatches "alias_via_inheritance" "" Nothing
  assertSnapshotMatches "object_with_comment" "" Nothing
  assertSnapshotMatches "object_with_field_comments" "" Nothing
  assertSnapshotMatches "schema_description_multiline" "" Nothing

test_all_field_types :: IO ()
test_all_field_types = assertSnapshotMatches "all_field_types" "" Nothing

test_top_level_array :: IO ()
test_top_level_array = assertSnapshotMatches "top_level_array" "" Nothing

cfgForPatterns :: Config
cfgForPatterns =
  (def :: Config)
    & #stringPatternImport ?~ "import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';"
    & #stringPatternSchemaGenerator ?~ "StringMatchingRegularExpressionSchema"

test_top_level_string_with_pattern :: IO ()
test_top_level_string_with_pattern = do
  assertSnapshotMatches "top_level_string_with_pattern" "" (Just cfgForPatterns)

test_top_level_string_with_maxLength :: IO ()
test_top_level_string_with_maxLength = assertSnapshotMatches "top_level_string_with_maxLength" "" Nothing

test_top_level_string_with_minLength_and_maxLength :: IO ()
test_top_level_string_with_minLength_and_maxLength =
  assertSnapshotMatches "top_level_string_with_minLength_and_maxLength" "" Nothing

test_top_level_string_with_pattern_and_minLength_and_maxLength :: IO ()
test_top_level_string_with_pattern_and_minLength_and_maxLength =
  assertSnapshotMatches "top_level_string_with_pattern_and_minLength_and_maxLength" "" (Just cfgForPatterns)

test_top_level_string_with_format :: IO ()
test_top_level_string_with_format =
  assertSnapshotMatches "top_level_string_with_format" "" Nothing

test_top_level_integer_with_minimum :: IO ()
test_top_level_integer_with_minimum =
  assertSnapshotMatches "top_level_integer_with_minimum" "" Nothing

test_unions :: IO ()
test_unions = do
  assertSnapshotMatches "union" "" Nothing
  assertSnapshotMatches "tagged_union" "" Nothing

test_enums :: IO ()
test_enums = do
  assertSnapshotMatches "one_value_enum" "" Nothing
  assertSnapshotMatches "enum" "" Nothing

test_schemaTranslation :: IO ()
test_schemaTranslation = do
  assertSnapshotMatches "schema_translation" "" Nothing
  assertSnapshotMatches "schema_translation" "disabled" ((def :: Config) & #schemaTranslation .~ False & Just)

test_examples :: IO ()
test_examples = do
  assertSnapshotMatches "examples" "" Nothing

test_union_single_member :: IO ()
test_union_single_member = do
  assertSnapshotMatches "union_single_member" "" Nothing

test_union_primitives :: IO ()
test_union_primitives = do
  assertSnapshotMatches "union_primitives" "" Nothing

test_topLevelNativeDecimal :: IO ()
test_topLevelNativeDecimal = do
  assertSnapshotMatches "top_level_native_decimal" "" Nothing
  assertSnapshotMatches "top_level_native_decimal" "both" (Just $ defCfg & #numberMinMaxLimitsCombinedMode .~ InclusiveAndExclusive)

test_arrayLengthMinMax :: IO ()
test_arrayLengthMinMax = do
  assertSnapshotMatches "array_length_min_max" "" Nothing
