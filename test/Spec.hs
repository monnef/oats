{-# OPTIONS_GHC -F -pgmF htfpp -Wno-unused-top-binds #-}

import {-@ HTF_TESTS @-} UtilsSpec
import {-@ HTF_TESTS @-} TemplateUtilsSpec
import {-@ HTF_TESTS @-} DataSpec
import {-@ HTF_TESTS @-} ParserSpec
import {-@ HTF_TESTS @-} DataProcessorSpec
import {-@ HTF_TESTS @-} SnapshotsSpec
import {-@ HTF_TESTS @-} ConfigurationSpec
import {-@ HTF_TESTS @-} CrawlerSpec
import {-@ HTF_TESTS @-} WorkerSpec
import {-@ HTF_TESTS @-} ErrorsSpec
import {-@ HTF_TESTS @-} GeneratorSpec
import {-@ HTF_TESTS @-} IgnoreRulesSpec
import {-@ HTF_TESTS @-} PatternCheckerSpec
import {-@ HTF_TESTS @-} RangeUtilsSpec

import Test.Framework

main :: IO ()
main = htfMain htf_importedTests
