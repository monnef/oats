{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module WorkerSpec
  ( htf_thisModulesTests,
  )
where

import Configuration (prepareOptionalRegexFromConfigFile)
import Data
import Data.Default (def)
import Data.Either.Extra (fromRight')
import Data.List (sort)
import Data.Maybe (fromMaybe)
import Optics hiding (allOf)
import Test.Framework
import Utils
import Worker

test_singleFileApiYamlToTypeScripts_ignoreComponentsRegex :: IO ()
test_singleFileApiYamlToTypeScripts_ignoreComponentsRegex = do
  let regex = prepareOptionalRegexFromConfigFile "" "" (Just "[iI]gnore") & fromRight'
  let ctx = (def :: WorkContext) & #config % #ignoreComponentsRegex .~ regex
  let validSchemaYamlInput =
        [qq|
components:
  schemas:
    MyString:
      type: string
    MyObject:
      type: object
      properties:
        name:
          type: string
|]
  let invalidSchemaYamlInput =
        validSchemaYamlInput
          <> [qq|
    MyFooToIgnore:
      type: FOOOO
|]
  -- doesn't drop anything when regex is missing
  resNeg <- singleFileApiYamlToTypeScripts def def Nothing validSchemaYamlInput [] Nothing [] & evalAppM def
  assertEqualVerbose (pShow resNeg & toS) (Right 2) (resNeg <&> length)
  -- fails to parse invalid type
  resInvalid <- singleFileApiYamlToTypeScripts def def Nothing invalidSchemaYamlInput [] Nothing [] & evalAppM def
  _ <- assertLeftVerbose (resInvalid & pShow & toS) resInvalid
  -- filters correctly invalid schema and doesn't try to process it (doesn't return an error)
  resPos <- singleFileApiYamlToTypeScripts def ctx Nothing invalidSchemaYamlInput [] Nothing [] & evalAppM def
  let pResPos = resPos & pShow & toS
  assertEqualVerbose pResPos (Right 2) (resPos <&> length)
  let negNames :: [Text] = resPos ^? _Right & fromMaybe [] <&> ((^. _2) >>> toS) & sort
  let expNegNames = ["MyString", "MyObject"] & sort
  assertEqualVerbose (negNames & pShow & toS) expNegNames negNames
