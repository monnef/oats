import * as t from 'io-ts';
import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';

// Example: "27733772"

export const icoRegexGen = (): RegExp => new RegExp("^[0-9]{8}$");

export const IcoSchema = StringMatchingRegularExpressionSchema<Ico>(icoRegexGen());

export type Ico = string;
