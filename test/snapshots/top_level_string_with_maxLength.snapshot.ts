import * as t from 'io-ts';

// Example: "xxx"

export const businessNameMaxLength = 256;

export const BusinessNameSchema = t.refinement(t.string, n => n.length <= businessNameMaxLength, 'BusinessName');

export type BusinessName = string;
