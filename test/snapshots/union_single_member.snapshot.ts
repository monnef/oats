import * as t from 'io-ts';

export const SingleMemberUnionSchema = t.string;

export type SingleMemberUnion = t.TypeOf<typeof SingleMemberUnionSchema>;
