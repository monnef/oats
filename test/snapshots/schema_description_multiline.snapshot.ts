import * as t from 'io-ts';

// Ordinary customer,
// looking to waste some money.

const requiredPart = t.interface({
  name: t.union([t.string, t.null]),
});

export const CustomerSchema = t.exact(requiredPart);

export interface Customer extends t.TypeOf<typeof CustomerSchema> {}
