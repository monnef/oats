import * as t from 'io-ts';

const requiredPart = t.interface({
  // A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. A very loong comment. 
  age: t.union([t.number, t.null]),
  id: t.Integer, // A simple comment
  // Not so
  // simple
  // comment
  name: t.union([t.string, t.null]),
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
