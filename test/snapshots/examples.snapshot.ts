import * as t from 'io-ts';

// A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. A lot of examples. #end

const requiredPart = t.interface({
  array: t.union([t.array(t.number), t.null]),
  float: t.union([t.number, t.null]),
  num: t.union([t.number, t.null]),
  str: t.union([t.string, t.null]),
  strQuoted: t.union([t.string, t.null]),
});

export const ExamplesSchema = t.exact(requiredPart);

export interface Examples extends t.TypeOf<typeof ExamplesSchema> {}
