import * as t from 'io-ts';

const requiredPart = t.interface({
  code: t.Integer,
  name: t.string,
  note: t.union([t.string, t.null]),
});

export const CreateItemSchema = t.exact(t.intersection([UpdateItemSchema.type, requiredPart]));

export interface CreateItem extends t.TypeOf<typeof CreateItemSchema> {}
