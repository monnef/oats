import * as t from 'io-ts';

export const nativeDecimalMinimum = -1000.0; // exclusive
export const nativeDecimalMaximum = 1000.0; // exclusive
export const nativeDecimalMinimumInclusive = -999.99;
export const nativeDecimalMaximumInclusive = 999.99;
export const nativeDecimalMinMaxInclusive: [number | null, number | null] = [-999.99, 999.99];
export const nativeDecimalMinMaxExclusive: [number | null, number | null] = [-1000.0, 1000.0];
export const nativeDecimalDecimalPlaces = 2;
export const nativeDecimalRegexGen = new RegExp("^-?\\d{1,3}[,.]\\d{2}$");

export const NativeDecimalSchema = t.refinement(t.number, x => x > nativeDecimalMinimum && x < nativeDecimalMaximum && Number.isInteger(parseFloat((x / 0.01).toFixed(10))), 'NativeDecimal');

export type NativeDecimal = t.TypeOf<typeof NativeDecimalSchema>;
