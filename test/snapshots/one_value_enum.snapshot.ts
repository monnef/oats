import * as t from 'io-ts';

const requiredPart = t.interface({
  tag: t.literal('CUSTOMER'),
  username: t.union([t.string, t.null]),
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
