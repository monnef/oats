import * as t from 'io-ts';

const requiredPart = t.interface({
  bool: t.union([t.boolean, t.null]),
  double: t.union([t.number, t.null]),
  int32: t.union([t.Integer, t.null]),
  int32arr: t.union([t.array(t.Integer), t.null]),
  int64: t.union([t.Integer, t.null]),
  int64arr: t.union([t.array(t.Integer), t.null]),
});

export const SchemaTranslationSchema = t.exact(requiredPart);

export interface SchemaTranslation extends t.TypeOf<typeof SchemaTranslationSchema> {}
