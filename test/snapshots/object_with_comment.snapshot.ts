import * as t from 'io-ts';

// COMMENT

const requiredPart = t.interface({
  id: t.Integer,
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
