import * as t from 'io-ts';

// tag is field 'kind'
export const TaggedUnionSchema = t.union([DetailsSchema, AdminDetailsSchema]);

export type TaggedUnion = t.TypeOf<typeof TaggedUnionSchema>;
