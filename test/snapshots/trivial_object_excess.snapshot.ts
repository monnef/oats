import * as t from 'io-ts';
import {excess} from 'ts-io-excess';

const requiredPart = t.interface({
  email: t.union([t.string, t.null]),
  id: t.Integer,
  username: t.string,
});

export const UserSchema = excess(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
