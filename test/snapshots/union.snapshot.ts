import * as t from 'io-ts';

export const UnionSchema = t.union([DetailsSchema, AdminDetailsSchema]);

export type Union = t.TypeOf<typeof UnionSchema>;
