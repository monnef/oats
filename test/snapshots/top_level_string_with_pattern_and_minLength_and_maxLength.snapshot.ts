import * as t from 'io-ts';
import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';

// Example: "xxx"

export const businessNameRegexGen = (): RegExp => new RegExp("^[a-zA-Z]{2,10}$");

export const businessNameMinLength = 2;
export const businessNameMaxLength = 256;

export const BusinessNameSchema = t.refinement(StringMatchingRegularExpressionSchema<BusinessName>(businessNameRegexGen()), n => n.length >= businessNameMinLength && n.length <= businessNameMaxLength, 'BusinessName');

export type BusinessName = string;
