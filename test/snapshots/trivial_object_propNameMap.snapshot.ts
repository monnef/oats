import * as t from 'io-ts';

const requiredPart = t.interface({
  bar: t.union([t.string, t.null]),
  baz: t.Integer,
  foo: t.string,
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
