import * as t from 'io-ts';

const requiredPart = t.interface({
  email: t.union([t.string, t.null]),
  id: t.Integer,
  username: t.string,
});

export const UserSchema = t.exact(requiredPart);

export interface User extends t.TypeOf<typeof UserSchema> {}
