import * as t from 'io-ts';

const requiredPart = t.interface({
});

export const AliasViaInheritanceSchema = t.exact(t.intersection([OriginalSchema.type, requiredPart]));

export interface AliasViaInheritance extends t.TypeOf<typeof AliasViaInheritanceSchema> {}
