import * as t from 'io-ts';

export const ErrorsSchema = t.refinement(t.array(ErrorSchema), xs => xs.length >= 1 && xs.length <= 10, 'Errors');

export type Errors = t.TypeOf<typeof ErrorsSchema>;
