import * as t from 'io-ts';

export const UnionPrimitivesSchema = t.union([t.string, t.number, t.boolean]);

export type UnionPrimitives = t.TypeOf<typeof UnionPrimitivesSchema>;
