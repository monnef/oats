import * as t from 'io-ts';

// Example: 4

export const intGT2Minimum = 2.0; // inclusive

export const IntGT2Schema = t.refinement(t.Integer, x => x >= intGT2Minimum, 'IntGT2');

export type IntGT2 = t.TypeOf<typeof IntGT2Schema>;
