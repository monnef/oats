import * as t from 'io-ts';

const requiredPart = t.interface({
  arrayObjectF: t.union([t.array(ArrayObjectSchema), t.null]),
  arrayPrimitiveF: t.union([t.array(t.string), t.null]),
  dateF: t.union([DateSchema, t.null]),
  dateTimeF: t.union([DateTimeSchema, t.null]),
  integerF: t.union([t.Integer, t.null]),
  numberF: t.union([t.number, t.null]),
  schemaF: t.union([ObjectSchema, t.null]),
  stringF: t.union([t.string, t.null]),
});

export const ObjectSchema = t.exact(requiredPart);

export interface Object extends t.TypeOf<typeof ObjectSchema> {}
