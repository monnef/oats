import * as t from 'io-ts';

const optionalPart = t.partial({
  email: t.string,
});

const requiredPart = t.interface({
  id: t.Integer,
  username: t.string,
});

export const UserSchema = t.exact(t.intersection([optionalPart, requiredPart]));

export interface User extends t.TypeOf<typeof UserSchema> {}
