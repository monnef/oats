import * as t from 'io-ts';

const requiredPart = t.interface({
  bool: t.union([BooleanSchema, t.null]),
  double: t.union([DoubleSchema, t.null]),
  int32: t.union([Int32Schema, t.null]),
  int32arr: t.union([Int32ArraySchema, t.null]),
  int64: t.union([Int64Schema, t.null]),
  int64arr: t.union([Int64ArraySchema, t.null]),
});

export const SchemaTranslationSchema = t.exact(requiredPart);

export interface SchemaTranslation extends t.TypeOf<typeof SchemaTranslationSchema> {}
