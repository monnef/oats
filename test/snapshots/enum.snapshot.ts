import * as t from 'io-ts';

// Example: "USER"

export const RoleSchema = t.keyof({
  USER: null,
  ADMIN: null,
});

export type Role = t.TypeOf<typeof RoleSchema>;
