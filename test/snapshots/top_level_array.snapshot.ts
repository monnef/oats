import * as t from 'io-ts';

export const ErrorsSchema = t.array(ErrorSchema);

export type Errors = t.TypeOf<typeof ErrorsSchema>;
