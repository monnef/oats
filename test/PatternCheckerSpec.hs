{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module PatternCheckerSpec
  ( htf_thisModulesTests,
  )
where

import PatternChecker
import Test.Framework
import Utils
import qualified Data.Text as T

test_genPatternCheckJsCode :: IO ()
test_genPatternCheckJsCode = do
  let f = genPatternCheckJsCode
  let t e r = assertEqualVerbose [qq|{nl}result = {T.unpack r}{nl}expected = $e{nl}|] e r
  t "console.log(new RegExp(\"x\").test(\"y\"))" $ f "x" "y"
  t "console.log(new RegExp(\"^A$\").test(\"A1\"))" $ f "^A$" "A1"
  t "console.log(new RegExp(\"^A$\").test(\"A1\\\"\"))" $ f "^A$" "A1\""
  t "console.log(new RegExp(\"^A\\\\$\").test(\"A\\\\\"))" $ f "^A\\$" "A\\"

test_validateStringPatternAndExample :: IO ()
test_validateStringPatternAndExample = do
  let f = validateStringPatternAndExample
  f "^Alice" "Alice S30" >>= assertRight
  f "^Alice$" "Alice S30" >>= assertEqual (Left "Example \"Alice S30\" doesn't match given pattern \"^Alice$\".")
