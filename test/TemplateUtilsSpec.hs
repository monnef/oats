{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module TemplateUtilsSpec
  ( htf_thisModulesTests,
  )
where

import Data.Aeson (object, (.=))
import Test.Framework
import Utils
import qualified Data.Text as T

test_renderTemplate :: IO ()
test_renderTemplate = do
  let f = renderTemplate "// generated via oats {{ version }}"
  assertEqual (Right ("// generated via oats 269" :: Text)) $ f $ object ["version" .= (269 :: Integer)]
  let ff = renderTemplate "{{"
  let r2 = ff $ object []
  r2ErrMsg <- assertLeft r2
  assertBoolVerbose (toS r2ErrMsg) $ "ParserError" `T.isInfixOf` r2ErrMsg

test_renderTemplate_toPascal :: IO ()
test_renderTemplate_toPascal = do
  let f = renderTemplate "{{ 'my-schema'|toPascal() }}"
  assertEqual (Right ("MySchema" :: Text)) $ f $ object []

test_renderTemplate_toCamel :: IO ()
test_renderTemplate_toCamel = do
  let f = renderTemplate "{{ 'my-schema'|toCamel() }}"
  assertEqual (Right ("mySchema" :: Text)) $ f $ object []

test_renderTemplate_toScreamingSnake :: IO ()
test_renderTemplate_toScreamingSnake = do
  let f = renderTemplate "{{ 'my-schema'|toScreamingSnake() }}"
  assertEqual (Right ("MY_SCHEMA" :: Text)) $ f $ object []

test_renderTemplate_toQuietSnake :: IO ()
test_renderTemplate_toQuietSnake = do
  let f = renderTemplate "{{ 'my-schema'|toQuietSnake() }}"
  assertEqual (Right ("my_schema" :: Text)) $ f $ object []

test_renderTemplate_toKebab :: IO ()
test_renderTemplate_toKebab = do
  let f = renderTemplate "{{ 'MySchema'|toKebab() }}"
  assertEqual (Right ("my-schema" :: Text)) $ f $ object []

test_renderTemplate_toPascal_error :: IO ()
test_renderTemplate_toPascal_error = do
  let f = renderTemplate "{{ toPascal() }}"
  assertEqual (Right ("<Error: wrong number of arguments given to template function toPascal.>" :: Text)) $ f $ object []
