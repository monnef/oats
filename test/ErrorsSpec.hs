{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module ErrorsSpec
  ( htf_thisModulesTests,
  )
where

import Control.Arrow ((>>>))
import Data.AppM
import Data.Default (def)
import Data.Errors
import Data.ReturnCodes
import Optics
import TerminalUtils (filterOutColorCodes)
import Test.Framework
import Prelude hiding (exp)

defCtx :: ErrorContext
defCtx = def

test_formatErrorContext :: IO ()
test_formatErrorContext = do
  let f = formatErrorContext >>> fmap filterOutColorCodes
  assertEqual Nothing $ f defCtx
  assertEqual (Just "f.yaml") $ f $ defCtx & #fileName ?~ "f.yaml"
  assertEqual (Just "<unknown>:7") $ f $ defCtx & #lineNumber ?~ 7
  assertEqual (Just "f.yaml:7") $ f $ defCtx & #lineNumber ?~ 7 & #fileName ?~ "f.yaml"
  assertEqual (Just "f.yaml:7#Ship") $ f $ defCtx & #lineNumber ?~ 7 & #fileName ?~ "f.yaml" & #unitName ?~ "Ship"
  assertEqual (Just "f.yaml:7#Ship.id") $ f $ defCtx & #lineNumber ?~ 7 & #fileName ?~ "f.yaml" & #unitName ?~ "Ship" & #subUnitName ?~ "id"
  assertEqual (Just "f.yaml#Ship.id") $ f $ defCtx & #fileName ?~ "f.yaml" & #unitName ?~ "Ship" & #subUnitName ?~ "id"

test_formatAppError :: IO ()
test_formatAppError = do
  let f = formatAppError >>> fmap filterOutColorCodes
  assertEqual ["Parser : err"] $ f (ParseError def "err")

test_formatAppWarning :: IO ()
test_formatAppWarning = do
  let f = formatAppWarning >>> filterOutColorCodes
  assertEqual "Missing title" $ f (AppWarning defCtx MissingTitleWarning)
  assertEqual "f.yaml > Missing title" $ f (AppWarning (defCtx & #fileName ?~ "f.yaml") MissingTitleWarning)
  assertEqual "f.yaml:2 > Missing title" $ f (AppWarning (defCtx & #fileName ?~ "f.yaml" & #lineNumber ?~ 2) MissingTitleWarning)
  assertEqual "f.yaml:2#U > Missing example" $ f (AppWarning (defCtx & #fileName ?~ "f.yaml" & #lineNumber ?~ 2 & #unitName ?~ "U") MissingExampleWarning)
  assertEqual "f.yaml:2#U.s > Missing example" $ f (AppWarning (defCtx & #fileName ?~ "f.yaml" & #lineNumber ?~ 2 & #unitName ?~ "U" & #subUnitName ?~ "s") MissingExampleWarning)

test_appWarningNames :: IO ()
test_appWarningNames = do
  let xs = appWarningNames
  assertBool $ "MissingTitle" `elem` xs

test_isAppWarningName :: IO ()
test_isAppWarningName = do
  let f = isAppWarningName
  assertBool $ f "MissingTitle"

test_getAppWarningName :: IO ()
test_getAppWarningName = do
  let f = getAppWarningName
  assertEqual "MissingTitle" $ f MissingTitleWarning

test_removeIgnoredWarnings :: IO ()
test_removeIgnoredWarnings = do
  let warn = AppWarning defCtx MissingTitleWarning
  let warn' = AppWarning defCtx MissingExampleWarning
  res <- runAppM (def & #appWarnings .~ [warn, warn']) $ removeIgnoredWarnings ["MissingTitle"]
  assertEqual [warn'] $ res ^. _2 % #appWarnings

test_appErrToReturnCode :: IO ()
test_appErrToReturnCode = do
  let f = appErrToReturnCode
  assertEqual RetErrUnknown $ f $ MultiError []
  assertEqual RetErrCrawler $ f $ MultiError [CrawlerError ""]
  assertEqual RetErrProcessing $ f $ MultiError [GenerationError def "", CrawlerError ""]
