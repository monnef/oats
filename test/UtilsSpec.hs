{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module UtilsSpec
  ( htf_thisModulesTests,
  )
where

import Data (TestRecord(..), a, b)
import qualified Data.Aeson as AES
import qualified Data.HashMap.Lazy as M
import qualified Data.Text as T
import Optics
import Test.Framework
import Utils

test_htf :: IO ()
test_htf = assertBool True

test_qq_newline :: IO ()
test_qq_newline = do
  assertEqual
    ("\n" :: String)
    [qq|
|]
  assertEqual ("\n" :: String) [qq|$nl|]

--test_qq_label :: IO ()
--test_qq_label = do
--  let x = TestRecord {a = "A", b = Nothing}
--  let exp = x ^. #a
--  assertEqual exp [qq|{x ^. #a}|]

test_i :: IO ()
test_i = do
  let x = TestRecord {a = "A", b = Nothing}
  let exp = x ^. #a & surround "(" ")"
  assertEqual exp [i|(#{x ^. #a})|]

type ETI = Either Text Int

type ETLI = Either Text [Int]

test_firstErrorFromEithers :: IO ()
test_firstErrorFromEithers = do
  let f = firstErrorFromEithers
  assertEqual (Right [] :: ETLI) $ f ([] :: [ETI])
  assertEqual (Left "e" :: ETLI) $ f [Left "e"]
  assertEqual (Left "e") $ f ([Right 1, Left "e"] :: [ETI])
  assertEqual (Left "e" :: ETLI) $ f [Left "e", Right 1]
  assertEqual (Right [1] :: ETLI) $ f [Right 1]
  assertEqual (Right [1, 2] :: ETLI) $ f [Right 1, Right 2]
  assertEqual (Right [1, 2] :: ETLI) $ f [Right 1, Right 2]
  assertEqual (Right [1, 2, 3] :: ETLI) $ f [Right 1, Right 2, Right 3]

test_capitalize :: IO ()
test_capitalize = do
  let f = capitalize
  assertEqual "" $ f ""
  assertEqual "Pike" $ f "pike"

test_deCapitalize :: IO ()
test_deCapitalize = do
  let f = deCapitalize
  assertEqual "" $ f ""
  assertEqual "bortus" $ f "Bortus"

test_indent :: IO ()
test_indent = do
  let f = indent
  assertEqual "" $ f 0 ""
  assertEqual "" $ f 2 ""
  assertEqual "a\nb\n" $ f 0 "a\nb"
  assertEqual "    a\n    b\n" $ f 4 "a\nb"

test_backSlashToSlash :: IO ()
test_backSlashToSlash = do
  let f = backSlashToSlash
  assertEqual "" $ f ""
  assertEqual "a" $ f "a"
  assertEqual "/" $ f "/"
  assertEqual "/" $ f "\\"
  assertEqual "x/y/z" $ f "x\\y\\z"

type HTI = M.HashMap Text Int

test_hashMapLookupValue :: IO ()
test_hashMapLookupValue = do
  let f = hashMapLookupValue
  assertEqual Nothing $ f 1 (M.fromList [] :: HTI)
  assertEqual Nothing $ f 1 (M.fromList [("x", 2)] :: HTI)
  assertEqual (Just "x") $ f 2 (M.fromList [("x" :: Text, 2)] :: HTI)
  assertEqual (Just "x") $ f 2 (M.fromList [("y", 1), ("x", 2), ("z", 3)] :: HTI)

test_leftOnTrue :: IO ()
test_leftOnTrue = do
  let f = leftOnTrue
  let z = 0 :: Int
  assertEqual (Right ()) $ f z False
  assertEqual (Left z) $ f z True

test_emptyTextAsNothing :: IO ()
test_emptyTextAsNothing = do
  let f = emptyTextAsNothing
  assertEqual Nothing $ f ""
  assertEqual (Just "x") $ f "x"

test_fmtFloat :: IO ()
test_fmtFloat = do
  let f = fmtFloat
  assertEqual "0.0" $ f 0
  assertEqual "1.0" $ f 1
  assertEqual "-1.0" $ f (-1)
  assertEqual "1.01" $ f 1.01
  assertEqual "4.00000001" $ f (4.00000001 :: Double)

test_textToMaybe :: IO ()
test_textToMaybe = do
  let f = textToMaybe
  assertEqual Nothing $ f ""
  assertEqual (Just "a") $ f "a"

test_justFalseToNothing :: IO ()
test_justFalseToNothing = do
  let f = justFalseToNothing
  assertEqual Nothing $ f $ Nothing
  assertEqual Nothing $ f $ Just False
  assertEqual (Just True) $ f $ Just True

test_lookupPair :: IO ()
test_lookupPair = do
  let f = lookupPair
  assertEqual Nothing $ f 1 (M.fromList ([] :: [(Int, Text)]))
  assertEqual (Just (4, "a")) $ f 4 (M.fromList ([(4, "a"), (5, "b")] :: [(Int, Text)]))
  assertEqual Nothing $ f 9 (M.fromList ([(4, "a"), (5, "b")] :: [(Int, Text)]))

type MaybeIntTextPair = Maybe (Int, Text)

test_unzipMaybe :: IO ()
test_unzipMaybe = do
  let f = unzipMaybe
  assertEqual (Nothing, Nothing) $ f (Nothing :: MaybeIntTextPair)
  let exp2 = (Just 1, Just "a")
  let res2 = f (Just (1, "a") :: MaybeIntTextPair)
  assertEqual exp2 res2

test_eq :: IO ()
test_eq = do
  assertEqual True $ eq (0 :: Int) 0
  assertEqual False $ eq ("a" :: Text) "b"

test_surround :: IO ()
test_surround = do
  let f = surround
  assertEqual "{a}" $ f "{" "}" "a"

test_isHttpSuccessCode :: IO ()
test_isHttpSuccessCode = do
  let f = isHttpSuccessCode
  assertEqual True $ f 200
  assertEqual True $ f 201
  assertEqual True $ f 204
  assertEqual False $ f 300
  assertEqual False $ f 399
  assertEqual False $ f 404

test_tReadFileEither :: IO ()
test_tReadFileEither = do
  r1 <- tReadFileEither "__non_existing_file__"
  _ <- assertLeft r1
  r2 <- tReadFileEither "README.md"
  _ <- assertRight r2
  Right r2' <- pure r2
  assertBool $ T.isInfixOf "oats" r2'

test_isAesonValueString :: IO ()
test_isAesonValueString = do
  let f = isAesonValueString
  assertBool $ not $ f (AES.Number 4)
  assertBool $ f (AES.String "4")

test_isAesonValueNumber :: IO ()
test_isAesonValueNumber = do
  let f = isAesonValueNumber
  assertBool $ f (AES.Number 4)
  assertBool $ not $ f (AES.String "4")

test_filterOut :: IO ()
test_filterOut = do
  let f :: (Integer -> Bool) -> [Integer] -> [Integer] = filterOut
  assertEqual [] $ f (== 0) []
  assertEqual [] $ f (== 0) [0]
  assertEqual [1] $ f (== 0) [0, 1, 0]
  assertEqual [1, 2, 3] $ f (== 0) [1, 0, 2, 0, 3]

test_singleListToJust :: IO ()
test_singleListToJust = do
  let f :: [Int] -> Maybe Int = singleListToJust
  assertEqual Nothing $ f []
  assertEqual Nothing $ f [1, 2]
  assertEqual Nothing $ f [1 .. 10]
  assertEqual (Just 1) $ f [1]

test_numberOfDigits :: IO ()
test_numberOfDigits = do
  let f = numberOfDigits
  assertEqual 1 $ f 0
  assertEqual 1 $ f 9
  assertEqual 2 $ f 10
  assertEqual 2 $ f 50
  assertEqual 2 $ f 99
  assertEqual 3 $ f 100
  assertEqual 3 $ f 999
  assertEqual 3 $ f (-100)
  assertEqual 3 $ f (-999)

test_genJsNumberLiteral :: IO ()
test_genJsNumberLiteral = do
  let f = genJsNumberLiteral
  assertEqual "1.0" $ f 1.0
  assertEqual "0.1" $ f 0.1
  assertEqual "0.01" $ f 0.01
  assertEqual "0.001" $ f 0.001
  assertEqual "0.0001" $ f 0.0001
  assertEqual "1000.0001" $ f 1000.0001

test_isMultipleOf :: IO ()
test_isMultipleOf = do
  let f = isMultipleOf
  assertEqual True $ f 0.1 0.01
  assertEqual True $ f 5.0 1.0
  assertEqual True $ f 500.0 1.0
  assertEqual True $ f 500.0 10.0
  assertEqual True $ f 10.5 0.5
  assertEqual True $ f 3.0 1.0
  assertEqual True $ f 0.3 0.1
  assertEqual True $ f 0.03 0.01
  assertEqual True $ f 0.003 0.001
  assertEqual True $ f 0.0003 0.0001
  assertEqual True $ f 0.00003 0.00001
  assertEqual True $ f 0.000003 0.000001
  assertEqual True $ f 0.0000003 0.0000001
  assertEqual True $ f 0.00001 0.000001
  assertEqual True $ f 0.09 0.01
  assertEqual True $ f 10000000.0 1.0

  assertEqual False $ f 0.03 0.02
  assertEqual False $ f 0.3 0.2
  assertEqual False $ f 0.03 0.2
  assertEqual False $ f 0.003 0.002
  assertEqual False $ f 1.0 2.0
  assertEqual False $ f 100.0 99.0
  assertEqual False $ f 10000.0 9999.0
