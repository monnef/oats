{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module ParserSpec
  ( htf_thisModulesTests,
  )
where

import Optics
import Data
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import qualified Data.Yaml as Y
import Parser
import Test.Framework
import Utils
import Prelude hiding (exp)

defROA :: RawOA
defROA = def @RawOA

loadFile :: Text -> IO Text
loadFile = (\x -> "test/data/" <> x <> ".yaml") >>> toS >>> readFile >>> (<&> toS)

test_parse_empty :: IO ()
test_parse_empty = do
  res <- parseOneSchema def "" & evalAppM def
  assertRight res
  assertEqual (Right defROA) res

test_parse_trivial_object :: IO ()
test_parse_trivial_object = do
  res <- loadFile "trivial_object" >>= (parseOneSchema def >>> evalAppM def)
  let props =
        M.fromList
          [ ("username", defROA & #typ ?~ "string"),
            ("email", defROA & #typ ?~ "string"),
            ("id", defROA & #typ ?~ "integer")
          ]
  let exp = Right $ defROA {title = Just "User", required = Just ["username", "id"], properties = Just props}
  assertEqual exp res

test_parse_object_with_override_non_required_representation :: IO ()
test_parse_object_with_override_non_required_representation = do
  res <- loadFile "object_with_override_non_required_representation" >>= (parseOneSchema def >>> evalAppM def)
  let props =
        M.fromList
          [("id", defROA & #typ ?~ "integer")]
  let exp = Right $ defROA {title = Just "User", required = Nothing, properties = Just props, xOatsOverrideNonRequiredRepresentation = Just UndefinedAsNonRequired}
  assertEqual exp res

mkTag :: Text -> Maybe Text -> RawOATag
mkTag name description = RawOATag {..}

test_parse_RequestTypeRaw :: IO ()
test_parse_RequestTypeRaw = do
  resRaw :: RequestType <- Y.decodeThrow "get"
  assertEqual GetRequest resRaw

test_parse_RequestTypeKey :: IO ()
test_parse_RequestTypeKey = do
  let resEnc = Y.encode (M.fromList [(GetRequest, 0)] :: M.HashMap RequestType Int)
  assertEqual "get: 0\n" resEnc
  let yaml =
        [qq|
get: 1
post: 2
patch: 7
  |]
  res :: M.HashMap RequestType Int <- yaml & Y.decodeThrow
  assertEqual (M.fromList [(GetRequest, 1), (PostRequest, 2), (PatchRequest, 7)]) res

test_parse_RawOAPaths :: IO ()
test_parse_RawOAPaths = do
  let yaml =
        [qq|
get:
  operationId: getRoles
  parameters:
    - in: query
      name: name
      schema:
        type: string
  responses:
    "200":
      content:
        application/json:
          schema:
            \$ref: '#/components/schemas/Role'
      description: OK
  summary: Returns a page of all user roles.
  tags:
    - Roles
  |]
  res :: RawOAPaths <- yaml & Y.decodeThrow
  assertBool $ M.member GetRequest res

test_parse_OAParameterIn :: IO ()
test_parse_OAParameterIn = do
  let resEnc = Y.encode QueryParameterInType
  assertEqual "query\n" resEnc
  res :: OAParameterIn <- "query" & Y.decodeThrow
  assertEqual QueryParameterInType res

test_parse_endpoints :: IO ()
test_parse_endpoints = do
  resEither <- loadFile "endpoints" >>= (parseSingleFileApi def >>> evalAppM def)
  _ <- assertRight resEither
  Right res <- return resEither
  let expProps =
        M.fromList
          [ ("name", def & #typ ?~ "string"),
            ("id", def & #typ ?~ "string" & #format ?~ "uuid"),
            ("permissionIds", def & #typ ?~ "array" & #items ?~ (def & #typ ?~ "number"))
          ]
  let expRole = defROA & #typ ?~ "object" & #properties ?~ expProps & #required ?~ ["id", "name", "permissionIds"]
  let expIco = defROA & #typ ?~ "string" & #pattern ?~ "^[0-9]{8}$"
  let setContent (x :: RawOARequestOrResponse) = x & #content ?~ M.fromList [("application/json", def & #schema .~ (def & #ref ?~ "#/components/schemas/Role"))]
  let setTags = #tags .~ ["Roles"]
  let setTestTags = #tags .~ ["Test"]
  let expGetRoles =
        def
          & #operationId .~ "getRoles"
          & #summary ?~ "Returns a page of all user roles."
          & setTags
          & #parameters ?~ [def & #inn .~ QueryParameterInType & #name .~ "name" & #schema .~ (defROA & #typ ?~ "string")]
          & #responses .~ M.fromList [(200, def & #description ?~ "OK" & setContent)]
  let expCreateRole =
        def
          & #operationId .~ "createRole"
          & #summary ?~ "Creates a role."
          & setTags
          & #responses .~ M.fromList [(201, def & #description ?~ "Created")]
          & #requestBody ?~ (def & setContent)
  let expRolesEndpoints = M.fromList [(GetRequest, expGetRoles), (PostRequest, expCreateRole)]
  let roleIdParam =
        def
          & #inn .~ PathParameterInType
          & #name .~ "roleId"
          & #schema .~ (defROA & #typ ?~ "string" & #format ?~ "uuid")
          & #required ?~ True
  let icoParam =
        def
          & #inn .~ PathParameterInType
          & #name .~ "ico"
          & #schema .~ (defROA & #ref ?~ "#/components/schemas/Ico")
          & #required ?~ True
  let nameParam =
        def
          & #inn .~ QueryParameterInType
          & #name .~ "name"
          & #schema .~ (defROA & #typ ?~ "string")
          & #required ?~ True
  let setParams = #parameters ?~ [roleIdParam]
  let expGetRole =
        def
          & #operationId .~ "getRole"
          & #summary ?~ "Finds details about the role."
          & setParams
          & #responses .~ M.fromList [(200, def & #description ?~ "OK" & setContent)]
          & setTags
  let expDeleteRole =
        def
          & #operationId .~ "deleteRole"
          & #summary ?~ "Deletes the role."
          & setParams
          & #responses .~ M.fromList [(204, def & #description ?~ "No Content")]
          & setTags
  let expUpdateRole =
        def
          & #operationId .~ "updateRole"
          & #summary ?~ "Updates the role."
          & setParams
          & #responses .~ M.fromList [(204, def & #description ?~ "No Content")]
          & setTags
          & #requestBody ?~ (def & setContent)
  let maxEndpoint =
        def
          & #operationId .~ "max"
          & #summary ?~ "Max"
          & setParams
          & #responses .~ M.fromList [(200, def & #description ?~ "OK" & setContent)]
          & setTestTags
          & #requestBody ?~ (def & setContent)
          & #parameters ?~ [roleIdParam, icoParam, nameParam]
  let minEndpoint =
        def
          & #operationId .~ "min"
          & #responses .~ M.fromList [(204, def & #description ?~ "No Content")]
          & setTestTags
  let expRoleEndpoints = M.fromList [(GetRequest, expGetRole), (DeleteRequest, expDeleteRole), (PutRequest, expUpdateRole)]
  let maxEndpoints = M.fromList [(GetRequest, maxEndpoint)]
  let minEndpoints = M.fromList [(GetRequest, minEndpoint)]
  let refreshTokenParam =
        def
          & #inn .~ CookieParameterInType
          & #name .~ "refresh-token"
          & #schema .~ (defROA & #typ ?~ "string")
          & #required ?~ True
  let refreshTokenEndpoint =
        def
          & #operationId .~ "refreshToken"
          & #responses .~ M.fromList [(204, def & #description ?~ "No Content")]
          & #parameters ?~ [refreshTokenParam]
          & setTestTags
  let expRefreshTokenEndpoints = M.fromList [(PostRequest, refreshTokenEndpoint)]
  let expPaths :: M.HashMap Text RawOAPaths = M.fromList
        [ ("/refresh-token", expRefreshTokenEndpoints),
          ("/roles", expRolesEndpoints),
          ("/roles/{roleId}", expRoleEndpoints),
          ("/max/{roleId}/2/{ico}", maxEndpoints),
          ("/min", minEndpoints)
        ]
  let exp =
        RawOAFile
          { components = RawOAComponents {schemas = M.fromList [("Role", expRole), ("Ico", expIco)]},
            tags = Just [mkTag "Roles" (Just "The management of roles.")],
            paths = Just expPaths
          }
  assertEqual (exp ^. #tags) (res ^. #tags)
  assertEqual (exp ^. #components % #schemas) (res ^. #components % #schemas)
  assertEqual (exp ^. #paths) (res ^. #paths)
  assertEqual exp res

test_parseCustomEndpoints :: IO ()
test_parseCustomEndpoints = do
  resEither <- loadFile "custom_endpoints" >>= (parseCustomEndpoints def >>> evalAppM def)
  _ <- assertRight resEither
  Right res <- pure resEither
  let exp =
        CustomEndpointsMainFile
          { files =
              M.fromList
                [ ("Api", CustomEndpointsOneFile {code = "a", imports = Just "b"}),
                  ("Api2", CustomEndpointsOneFile {code = "2", imports = Nothing})
                ]
          }
  assertEqual exp res
