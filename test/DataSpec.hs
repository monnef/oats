{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TypeApplications #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module DataSpec
  ( htf_thisModulesTests,
  )
where

import Data
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import qualified Data.Yaml as Y
import Test.Framework
import TestUtils
import Utils
import Prelude hiding (exp)
import Optics

defROA :: RawOA
defROA = def

test_fieldRenaming_decode :: IO ()
test_fieldRenaming_decode = do
  let exp = defROA & #typ ?~ "TYPE" & #ref ?~ "REF" & #title ?~ "TITLE"
  act <- Y.decodeThrow "type: TYPE\n$ref: REF\ntitle: TITLE"
  assertEqual exp act

test_fieldRenaming_encode :: IO ()
test_fieldRenaming_encode = do
  let input = defROA & #typ ?~ "TYPE" & #ref ?~ "REF" & #title ?~ "TITLE"
  let act = toS $ Y.encode input
  subAssert $ assertInfixOf "$ref: REF" act
  subAssert $ assertInfixOf "type: TYPE" act
  subAssert $ assertInfixOf "title: TITLE" act

test_properties_decode :: IO ()
test_properties_decode = do
  let props =
        M.fromList
          [ ("username", defROA & #typ ?~ "string"),
            ("email", defROA & #ref ?~ ".../Email")
          ]
  let exp = defROA {properties = Just props}
  let input =
        [qq|
properties:
  username:
    type: string
  email:
    \$ref: .../Email
  |]
  act <- Y.decodeThrow input
  assertEqual exp act
