{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -F -pgmF htfpp #-}

module ConfigurationSpec
  ( htf_thisModulesTests,
  )
where

import Configuration
import Optics hiding (re)
import Data
import Data.Default (def)
import qualified Data.HashMap.Lazy as M
import qualified Data.Text as T
import Test.Framework
import Text.RE.PCRE.Text (re)
import Utils
import Data.Yaml (decodeEither')
import Data.Either.Extra (mapLeft)

mkCfgPath :: Text -> Text
mkCfgPath n = [qq|test/data/config_$n.yaml|]

test_loadConfig_empty :: IO ()
test_loadConfig_empty = do
  c <- evalAppM def $ loadConfig $ mkCfgPath "empty"
  assertEqual (Right def) c

ignoreRulesData :: Maybe [IgnoreRuleRaw]
ignoreRulesData =
  Just
    [ IgnoreRuleRaw
        { errors = Just ["PrimitiveTypeInRequestOrResponseBody"],
          warnings = Nothing,
          operationId = Just "^(getUsers|deleteProduct)$",
          url = Nothing,
          schemaName = Nothing
        },
      IgnoreRuleRaw
        { errors = Just ["PrimitiveTypeInRequestOrResponseBody"],
          warnings = Nothing,
          operationId = Nothing,
          url = Just "^/some/things/",
          schemaName = Nothing
        },
      IgnoreRuleRaw
        { warnings = Just ["MissingTitle", "MissingExample"],
          errors = Nothing,
          operationId = Nothing,
          url = Nothing,
          schemaName = Just "Test$"
        }
    ]

test_loadConfig_full :: IO ()
test_loadConfig_full = do
  c <- evalAppM def $ loadConfig $ mkCfgPath "full"
  let e =
        Right $
          MaybeConfig
            { nonRequiredRepresentation = Just NullAsNonRequired,
              schemaTranslation = Just True,
              propertyNameMap = Just $ M.fromList [("a", "b")],
              useExcess = Just True,
              excessImport = Just "import {excess} from 'utils/excess';",
              apiPath = Just "../project-api/schemas",
              apiFilesToMerge = Just [ApiFileToMerge {path = "a/b"}, ApiFileToMerge {path = "x"}],
              apiFile = Just "./openapi.yaml",
              ignoreFilesRegex = Just "._.",
              stringPatternImport = Just "import {StringMatchingRegularExpressionSchema} from 'utils/StringMatchingRegularExpressionSchema';",
              stringPatternSchemaGenerator = Just "StringMatchingRegularExpressionSchema",
              header = Just "/* eslint-disable @typescript-eslint/no-unused-vars,unicorn/regex-shorthand */\n\n// Generated file, do NOT modify.",
              outPath = Just "output/path",
              apiModule = Just "generatedApi",
              customSchemasPath = Just "custom-schemas",
              ignoreComponentsRegex = Just "^_^",
              ignoreWarnings = Just ["MissingTitle", "MissingExample", "InheritingObjectWithoutOwnObject"],
              promoteWarningsToErrors = Just ["MissingTitle"],
              endpointsGenerationMode = Just AllEndpointsInOneFile,
              endpointsTemplate = Just "This is a long\ntemplate...\n",
              endpointsTemplateFile = Just "etf",
              emptyRequestSchema = Just "EmptyRequestSchema",
              emptyResponseType = Just "unknown",
              emptyResponseSchema = Just "EmptyResponseSchema",
              downloadFileResponseSchema = Just "aBS",
              downloadFileResponseType = Just "AB",
              uploadFileRequestSchema = Just "FDS",
              uploadFileRequestType = Just "FD",
              pathParamsParameterName = Just "PPPN",
              queryParamsParameterName = Just "QPPN",
              ignoreEndpointsByPathRegex = Just "@_@",
              customEndpointsFile = Just "custom/endpoints/file.ts",
              quiet = Just True,
              verbose = Just False,
              ignoreErrors = Just ["PrimitiveTypeInRequestOrResponseBody"],
              ignoreRules = ignoreRulesData,
              noTiming = Just True,
              noPatternValidationWithExample = Just True,
              color = Just True,
              cleanOutputDirectory = Just True,
              afterFileWriteCommand = Just "echo {{fileName}}",
              apiInfoEnabled = Just True,
              numberMinMaxLimitsEnabled = Just True,
              numberMinMaxLimitsTemplate = Just "const {{schemaName|toCamel()}}{{limitName|toPascal()}} = {{limitValue}};",
              numberMinMaxLimitsMode = Just InclusiveAndExclusive,
              numberMinMaxLimitsCombinedEnabled = Just True,
              numberMinMaxLimitsCombinedTemplate = Just "const {{schemaName|toCamel()}}{{isInclusive ? \"Incl\" : \"Excl\"}} = {{limitValue}};",
              numberMinMaxLimitsCombinedMode = Just InclusiveAndExclusive
            }
  assertEqual e c

test_loadConfig_unknown_field :: IO ()
test_loadConfig_unknown_field = do
  c <- evalAppM def $ loadConfig $ mkCfgPath "unknown_field"
  let str = show c
  _ <- assertLeftVerbose str c
  assertBoolVerbose str $ T.isInfixOf "unknown_field" (toS str)
  return ()

test_augmentConfigWithCmdArgs_def :: IO ()
test_augmentConfigWithCmdArgs_def = do
  let f = augmentConfigWithCmdArgs
  _ <- assertRight $ f def def
  return ()

test_augmentConfigWithCmdArgs_quiet_from_cfg :: IO ()
test_augmentConfigWithCmdArgs_quiet_from_cfg = do
  let f = augmentConfigWithCmdArgs
  let cfg = (def :: Config) & #quiet .~ True
  let args = (def :: CmdArgs) & #quiet .~ False
  let res = f cfg args
  assertEqual (Right True) (res <&> (^. #quiet))

test_validateAndConvertIgnoreRules :: IO ()
test_validateAndConvertIgnoreRules = do
  let f = validateAndConvertIgnoreRules
  r1 <- evalAppM def $ f ignoreRulesData
  let e1 =
        Right
          [ IgnoreRule
              { selector =
                  IgnoreRuleEndpointByOperationId
                    { operationIdRegex = [re|^(getUsers|deleteProduct)$|]
                    },
                targets =
                  IgnoreRuleTargets
                    { errors = ["PrimitiveTypeInRequestOrResponseBody"],
                      warnings = []
                    }
              },
            IgnoreRule
              { selector =
                  IgnoreRuleEndpointByUrl
                    { urlRegex = [re|^/some/things/|]
                    },
                targets =
                  IgnoreRuleTargets
                    { errors = ["PrimitiveTypeInRequestOrResponseBody"],
                      warnings = []
                    }
              },
            IgnoreRule
              { selector =
                  IgnoreRuleSchemaByName
                    { schemaNameRegex = [re|Test$|]
                    },
                targets =
                  IgnoreRuleTargets
                    { errors = [],
                      warnings = ["MissingTitle", "MissingExample"]
                    }
              }
          ]
  assertEqual e1 r1
  r2 <- evalAppM def $ f (ignoreRulesData & _Just % ix 0 % #errors % _Just % ix 0 .~ "NonexistantError")
  assertEqual (Left $ ConfigError "Unknown error name \"NonexistantError\"") r2
  r3 <- evalAppM def $ f (ignoreRulesData & _Just % ix 2 % #warnings % _Just % ix 0 .~ "NonexistantWarning")
  assertEqual (Left $ ConfigError "Unknown warning name \"NonexistantWarning\"") r3
  r4 <- evalAppM def $ f (ignoreRulesData & _Just % ix 0 % #operationId % _Just .~ "?!*")
  assertEqual (Left $ ConfigError "Failed to compile regex: ?!*") r4

test_decodingOf_NumberMinMaxLimitsMode :: IO ()
test_decodingOf_NumberMinMaxLimitsMode = do
  let f x = decodeEither' x & mapLeft show
  assertEqual (Right InclusiveOnly) $ f "inclusive"
  assertEqual (Right ExclusiveOnly) $ f "exclusive"
  assertEqual (Right InclusiveAndExclusive) $ f "both"
