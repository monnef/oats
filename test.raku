#!/usr/bin/env raku

use Terminal::ANSI::OO :t;

constant $ts-dir = 'ts'.IO;
constant $e2e-dir = 'e2e'.IO;

enum TestType <stack ts e2e>;

sub stack-test() {
    shell 'stack test' or die "unit tests failed";
}

sub ts-test() {
    indir $ts-dir, {
        shell 'npm test' or die "TS test failed";
    }
}

sub e2e-test() {
    indir $e2e-dir, {
        shell 'npm test' or die "E2E test failed";
    }
}

my @tests-to-run = [TestType::stack, TestType::ts, TestType::e2e];

if @*ARGS ~~ (['-h'] || ['--help'])  {
    say "test.raku [TEST-TYPE...]";
    say "  test types: " ~ TestType.keys.join(', ');
    exit 0;
}

if @*ARGS {
    @tests-to-run = @*ARGS.map({ TestType(TestType.enums{$_}) // die "invalid test type: $_" });
}

say "Running tests: {@tests-to-run.join(', ')}";
for @tests-to-run -> $test {
    given $test {
        when TestType::stack { stack-test(); }
        when TestType::ts { ts-test(); }
        when TestType::e2e { e2e-test(); }
    }
}

say '';
say t.cyan ~ "[test.raku]" ~ t.bright-green ~ " ✅ " ~ t.text-reset ~ "All tests were successful.";
